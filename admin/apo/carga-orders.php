<?php
	//header('Content-type: application/json');
	include_once('../func/functions.php');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$fileList = glob('../assets/*order*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach ($fileList as $filename) {

		$destinyfile = $filename;

	  //SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as &$content) {

				if ($content['estatus'] == 1 && $content['serviceinfo'] == 0) {

					$para['IMEI'] = $content['imei'];
					$para['ID'] = $content['serviceid'];

					$request = enviaImeiOrder($para);

					if (isset($request['SUCCESS'])) { //SI ES SUCCESS
            $content['serviceanswer'] = "SUCCESS";
						$content['serviceinfo'] = $request['SUCCESS'];
						$msg = "CORRECTO";
						$msg_2 = "Solo resta esperar por la respuesta de con tu código de desbloqueo.";
					} elseif ($request['ERROR']) { //SI ES ERROR
            $content['serviceanswer'] = "ERROR";
						$content['serviceinfo'] = $request['ERROR'];
						$msg = "ERROR";
						$msg_2 = "Hubo en error al procesar tu orden, por favor contactanos para ayudarte a solucionarlo.";
					}

					//ENVIAMOS EL MAIL DE AVISO//
					//////////////////////////////////
					$asunto = "Mensaje de tu orden " .$order. ;
					$mensaje = "<p><h2>Hola, " . $content['nombre'] . ", este es un reporte de seguimiento de tu orden.</p>
								<h3>Estatus: " . $msg . "</h3>
								<div>
								<i class='style=display: block; padding: 3px; border-bottom: 1px dotted grey;'>Tu orden: " . $order . "</i>
								<i class='style=display: block; padding: 3px; border-bottom: 1px dotted grey;'>Equipo" .$content['marca'].  ", " . $content['equipo'] . "</i>
								<i class='style=display: block; padding: 3px; border-bottom: 1px dotted grey;'>IMEI: " . $content['imei'] . "</i>
								</div>
								<p>" . $msg_2 . "</p>
								<div style='display: block; padding: 5px 0;'></div>
								<p>Nos pondremos en contacto contigo con los pasos siguientes a seguir.</p>
								<div style='display: block; padding: 5px 0;'></div>
								<p>Revisa tu orden en nuestra p&aacute;gina o para poder asistirte en caso de requerir apoyo: <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
								<div style='display: block; padding: 10px 0;'></div>
								<p>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de Lizipar para poder auxiliarte: <a href='mailto:contact@movilsim.com' style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent; color: #4b4b4b; font-weight: bolder;'><b>AQUI</b></a></p>
					";
					include_once('../../func/mailFunctions.php');
					enviaCorreo($asunto,$mensaje,$content['email']);

				}

				//LO VOLVEMOS A GUARDAR EDITADO CON EL MENSAJE DEL SERVIDOR
				$filejson = fopen($destinyfile, 'w') or die ('No file found, contact support');
				if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
				} else {
					echo "Error, contact support";
				}
				fclose($filejson);

			}

		}

	}

	function enviaImeiOrder($link) {
		include __DIR__ . ('/api.php');
		$request = $api->action('placeimeiorder', $link);
		return $request;
	}

?>
