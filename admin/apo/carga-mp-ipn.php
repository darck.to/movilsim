<?php
    // For 4.3.0 <= PHP <= 5.4.0
    if (!function_exists('http_response_code'))
    {
        function http_response_code($newcode = NULL)
        {
            static $code = 200;
            if($newcode !== NULL)
            {
                header('X-PHP-Response-Code: '.$newcode, true, $newcode);
                if(!headers_sent())
                    $code = $newcode;
            }
            return $code;
        }
    }

    header('Content-type: application/json');

    // Requerimos el archivo autoload dentro de la carpeta 'vendor' para poder usar el SDK
    require '../../vendor/autoload.php';

    // Agrega credenciales
    MercadoPago\SDK::setAccessToken('APP_USR-2084779251716512-082715-c191cac961cc369255952085dacabe72-456348368');

    $merchant_order = null;

    switch($_GET["topic"]) {
        case "payment":
            $payment = MercadoPago\Payment::find_by_id($_GET["id"]);
            // Get the payment and the corresponding merchant_order reported by the IPN.
            $merchant_order = MercadoPago\MerchantOrder::find_by_id($payment->order_id);
        case "merchant_order":
            $merchant_order = MercadoPago\MerchantOrder::find_by_id($_GET["id"]);
    }

    //COMPRABAMOS EL STATUS DEL PAGO
    if ($payment->status == "approved") {
        functionModificaOrder($_GET['id']);
    }

    function functionModificaOrder($link) {
        //NOMBRE DE ARCHIVO
        $fileList = glob('../assets/*order*.json');

        //RECORREMOS LOS ARCHIVOS
        foreach ($fileList as $filename) {

            $destinyfile = $filename;

            //SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
            if (file_exists($filename)) {

                $filename = file_get_contents($filename);
                $json = json_decode($filename, true);

                foreach ($json as &$content) {

                    if ($content['mporder'] == $link) {

                        $content['estatus'] = 1;

                    }

                    //LO VOLVEMOS A GUARDAR EDITADO CON EL MENSAJE DEL SERVIDOR
                    $filejson = fopen($destinyfile, 'w') or die ('No file found, contact support');
                    if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
                    } else {
                        echo "Error, contact support";
                    }
                    fclose($filejson);

                    //ENVIAMOS EL MAIL DE PROCESAMIENTO//
            				//////////////////////////////////
            				$asunto = "Pago procesado por MercadoPago";
            				$mensaje = "<p><h2>Hola, " . $content['nombre'] . ", este es un reporte de seguimiento de tu orden: "  .$order. "</p>
            							<h3>Estatus: PAGADO</h3>
            							<div>
            							<i class='style=display: block; padding: 3px; border-bottom: 1px dotted grey;'>Tu orden: " . $order . "</i>
            							<i class='style=display: block; padding: 3px; border-bottom: 1px dotted grey;'>Equipo" .$content['marca'].  ", " . $content['equipo'] . "</i>
            							<i class='style=display: block; padding: 3px; border-bottom: 1px dotted grey;'>IMEI: " . $content['imei'] . "</i>
            							</div>
            							<p>Ahora que hemos registrado tu pago solo resta esperar a que te enviemos el tu codigo de desbloqueo.</p>
            							<div style='display: block; padding: 5px 0;'></div>
            							<p>Nos pondremos en contacto contigo con los pasos siguientes a seguir.</p>
            							<div style='display: block; padding: 5px 0;'></div>
            							<p>Revisa tu orden en nuestra p&aacute;gina o para poder asistirte en caso de requerir apoyo: <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
            							<div style='display: block; padding: 10px 0;'></div>
            							<p>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de Lizipar para poder auxiliarte: <a href='mailto:contact@movilsim.com' style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent; color: #4b4b4b; font-weight: bolder;'><b>AQUI</b></a></p>
            				";
            				include_once('../../func/mailFunctions.php');
            				enviaCorreo($asunto,$mensaje,$content['email']);

                }

            }

        }

    }

    //LOG DE MP, COMPROBACION DE CONEXION CON MP IPN
    if (file_exists('log_mp')) {
        $filename = file_get_contents('log_mp');
        $json = json_decode($filename, true);
        $cabecera[] = array('fecha'=> date('d-m-Y H:i:s'), 'mporder' => $_GET['id'], 'status' => $payment->status);
        $json[] = $cabecera;
        if (file_exists('log_mp')) {
            $filelog = fopen('log_mp', 'w') or die ('No file found, contact support');
            if (fwrite($filelog, json_encode($json, JSON_PRETTY_PRINT))) {
            } else {
                echo "Error, contact support";
            }
            fclose($filelog);
        } else {
        }
    }

?>
