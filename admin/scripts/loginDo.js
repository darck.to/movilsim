//Muestra la lista de interes del cliente
$('form#formLogin').submit(function(event) {
  event.preventDefault();

  var formNombre = $(this).attr('name');
  var formData = $(this).serialize();
  var formMethod = $(this).attr('method');
  var rutaScrtip = $(this).attr('action');

  var request = $.ajax({
    url: rutaScrtip,
    method: formMethod,
    data: formData,
    dataType: "html"
  });

  // handle the responses
  request.done(function(data, req, err) {
    window.location.href = (".");
  })
  request.fail(function(jqXHR, textStatus) {
    console.log(textStatus);
  })
  request.always(function(data) {
    // clear the form
    $('form[name="' + formNombre + '"]').trigger('reset');
  });

});

function resetPass(e) {

  $.ajax({
    url: 'init/redo.php',
    dataType: "html",
    context: document.body,
    success: function(data) {
      $('#recuperaPass').html(data);
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });

}

//Muestra la lista de interes del cliente
$('form#formRedo').submit(function(event) {
  event.preventDefault();

  var formNombre = $(this).attr('name');
  var formData = $(this).serialize();
  var formMethod = $(this).attr('method');
  var rutaScrtip = $(this).attr('action');

  var request = $.ajax({
    url: rutaScrtip,
    method: formMethod,
    data: formData,
    dataType: "html"
  });

  // handle the responses
  request.done(function(data, req, err) {
    // update the user
    $('form[name="' + formNombre + '"]').trigger('reset');
  })
  request.fail(function(jqXHR, textStatus) {
    console.log(textStatus);
  })
  request.always(function(data) {
    // clear the form
    $('form[name="' + formNombre + '"]').trigger('reset');
  });

});

function createAccount(e){

  $.ajax({
    cache: 'false',
    url: "init/keyupInput.html",
    context: document.body,
    success: function(data) {

      //CARGA EL LOGIN EN TODA LA PANTALLA
      $('#loginDiv').html('');
      $('#bodyContent').html(data);

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })

}

//CIERRA EL LOGIN FORM
$('.close-login').on('click',function(){

  $('#loginDiv').html('');

});
