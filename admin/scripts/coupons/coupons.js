// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
      return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {
  modal.close();
});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){
	cargaTable();
});

function cargaTable(e) {

	$.ajax({
		url: 'php/coupons/carga-tabla.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {
        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({
				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],
  	    //ROW CONDITIONS
  	    'rowCallback': function(row, data, index){
          $(row).find('td:eq(3)').html(data[3] + '%');
  		    if(data[5] == 1){
  		        $(row).find('td:eq(5)').html('<i class="material-icons green-text text-lighten-2">thumb_up</i>');
  		    } else if (data[5] == 2){
  		        $(row).find('td:eq(5)').html('<i class="material-icons red-text text-darken-1">thumb_down</i>');
  		    }
				}
			});
			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
        var estatus, code, comments;

				var modalContent = '<div class="row">';
				modalContent += '<div class="col s12 m8 offset-m2">';
				modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + data[0] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + data[1] + '</b></i></p>';
        modalContent += '<p class="fiv-p"><i><em>Discount: </em></i><i class="right"><b id="orderNo">' + data[3] + '%</b></i></p>';
        modalContent += '<p class="fiv-p"><i><em>Coupon Code: </em></i><i class="right"><b id="orderNo">' + data[2] + '</b><i class="material-icons tiny blue-text handed right editNameCoupon" fol=' + data[0] + ' code="' + data[6] + '">edit</i></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Client: </em></i><i class="right"><b>' + data[4] + '</b></i></p>';
        //STATUS Y COLOR DEL ESTATUS
        if (data[5] == 1) {
          var icon = '<i class="material-icons handed changeVigencyStatus green-text text-lighten-2" fol=' + data[0] + ' code="' + data[6] + '"">thumb_up</i>';
        } else if (data[5] == 2) {
          var icon = '<i class="material-icons handed changeVigencyStatus red-text text-darken-1" fol=' + data[0] + ' code="' + data[6] + '"">thumb_down</i>';
        }
				modalContent += '<p class="fiv-p"><i><em>Vigency: </em></i><i class="right"><b>'+icon+'</b></i></p>';
				modalContent += '</div>';
				modalContent += '</div>';

				//SET MODAL
				modal.setContent(modalContent);
				modal.open();

        //CAMBIA EL NOMBRE DEL CUPON
        $('.editNameCoupon').on('click', function(e){
          var fol = $(this).attr('fol');
          var code = $(this).attr('code');
          var name;
          var nameCoupon = prompt("Enter new name", "ej: BLUEMONDAY");
          if (nameCoupon != null) {
            name = nameCoupon
          }
          $.ajax({
            type: 'post',
            url: 'php/coupons/coupons-name.php',
            data: {
              fol: fol,
              code: code,
              name : name
            },
            dataType: "html",
            context: document.body,
            success: function(data) {
              M.toast({html: data});
              modal.close();
              menuNav('template/coupons/coupons.php');
            },
            error: function(xhr, tst, err) {
              console.log(err);
            }
          });
        });

        $('.changeVigencyStatus').on('click', function(e){
          //CAMBIA EL STATUS DE PAGO DE LA ORDEN
          var fol = $(this).attr('fol');
          var code = $(this).attr('code');

          $.ajax({
            type: 'post',
            url: 'php/coupons/coupons-vigencia.php',
            data: {
              fol: fol,
              code: code
            },
            dataType: "html",
            context: document.body,
            success: function(data) {
              M.toast({html: data});
              modal.close();
              menuNav('template/coupons/coupons.php');
            },
            error: function(xhr, tst, err) {
              console.log(err);
            }
          });
        })

			});
    },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});

}

$('.addCoupon').on('click', function() {
  var modalContent = '<div class="row">';
  modalContent += '<div class="col s12 m8 offset-m2">';
  modalContent += '<p class="fiv-p"><h4>Create Coupons</h4></p>';
  modalContent += '<div class="col s12 m6">';
  modalContent += '<p class="fiv-p"><input id="noCoupons" type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></p>';
  modalContent += '<label for="noCoupons">Number of Coupons</label>';
  modalContent += '</div>';
  modalContent += '<div class="col s12 m6">';
  modalContent += '<p class="fiv-p"><input id="diCoupons" type="text" placholder="Discount %" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></p>';
  modalContent += '<label for="diCoupons">Discount</label>';
  modalContent += '<p class="right fiften-p"><button class="tingle-btn tingle-btn--primary createCoupons">Create</button></p>';
  modalContent += '</div>';
  modalContent += '</div>';

  //SET MODAL
  modal.setContent(modalContent);
  modal.open();

  $('.createCoupons').on('click', function(e){
    //VARS
    var num = $('#noCoupons').val();
    var desc = $('#diCoupons').val();
    $.ajax({
      type: 'post',
      url: 'php/coupons/coupons-add.php',
      data: {
        num: num,
        desc: desc
      },
      dataType: "html",
      context: document.body,
      success: function(data) {
        M.toast({html: data});
        modal.close();
        menuNav('template/coupons/coupons.php');
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
  })

})
