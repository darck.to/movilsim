// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {
    
    modal.close();

});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

	$.ajax({
		url: 'php/messaging/carga-questions.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({

				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],			    

			});

			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
				
				var modalContent = '<div class="row">';
				modalContent += '<div class="col s12 m8 offset-m2">';
				modalContent += '<p class="fiv-p left cyan lighten-4 border-r fiv-m"><i><em>Message: </em></i>' + data[4] + '</p>';
				modalContent += '</div>';
				modalContent += '<div class="col s12 m8 offset-m2">';
				modalContent += '<p class="fiv-p right right-align blue lighten-4 border-r fiv-m"><i><em>Your answer: </em></i>' + data[5] + '</p>';
				modalContent += '</div>';
				modalContent += '</div>';
				modalContent += '<div class="row">';
				modalContent += '<div class="col s12 m8 offset-m2">';
				modalContent += '<textarea id="adminAnswer" class="materialize-textarea"></textarea>';
				modalContent += '<label for="adminAnswer">Your answer</label>';
				modalContent += '<a class="waves-effect waves-light btn right white-text hidden sendQuestion">Send</a>';
				modalContent += '<i class="hidden mailQuestion" val="' + data[1] + '"></i>';
				fecha = data[2].replace(" ", "");
				fecha = fecha.replace(":", "");
				fecha = fecha.replace("-", "");
				fecha = fecha.replace("-", "");
				fecha = fecha.replace(":", "");
				modalContent += '<i class="hidden dateQuestion" val="' + fecha + '"></i>';
				modalContent += '</div>';
				modalContent += '</div>';
				modalContent += '<small class="right">If you send another answer, it will replace the current one. It will send another email to the customer</small>';

				//SET MODAL
				modal.setContent(modalContent);

				modal.open();

				//BUTTON SEND HIDE ON OFF
				$('#adminAnswer').keyup(function() {
					if (!$('#adminAnswer').val()) {
					    $('.sendQuestion').addClass('hidden');
					} else {
						$('.sendQuestion').removeClass('hidden');
					}
				});

				//SEND MESSAGE
				$('.sendQuestion').on('click', function () {
					
					var question = $('#adminAnswer').val();
					var mail = $('.mailQuestion').attr('val');
					var message = $('.dateQuestion').attr('val');

					$.ajax({
						type: 'post',
						url: 'php/messaging/guarda-questions.php',
						data: {
					    	question: question,
					    	mail: mail,
					    	message: message
					    },
						dataType: "html",
			    		context: document.body,
				        success: function(result) {
				        	//M.toast(result);
				        	menuNav('template/messaging/messaging-questions.php');
				        	modal.close();
				        },

						error: function(xhr, tst, err) {
							console.log(err);
						}

					});
				});

			} );

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}