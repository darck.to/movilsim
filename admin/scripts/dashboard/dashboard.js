$(document).ready(function(){

	cargaCards();

	cargaGraph();

});

function cargaCards(e) {

	$.ajax({
		url: 'php/dashboard/carga-cards.php',
		contentType: "application/json",
        context: document.body,
        success: function(data) {

	    	$('#imeiReceived').html(data[0]);
	    	$('#imeiCompleted').html(data[1]);

	    	$('#clientsRegistered').html(data[2]);
	    	$('#clientsPaid').html(data[3]);

	    	$('#commentsTotal').html(data[4]);

	    	$('#checkReceived').html(data[5]);
	    	$('#checkCompleted').html(data[6]);

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}

function cargaGraph(e) {

	var chart2 = document.getElementById("dashboardLines").getContext('2d');

	var datos;

	//LABELS
	$.ajax({
		url: 'php/dashboard/carga-graph.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

	    	datos = result;

	    	var labels = datos.map(function(e) {
			   return e.mes;
			});
			var data = datos.map(function(e) {
			   return e.orders;
			});;

			var dashboardLines = new Chart(chart2, {
			    type: 'line',
			    data: {
			        labels: labels,
			        datasets: [{
			            label: 'Unlock Orders',
			            data: data,
			            backgroundColor: [
			                'rgba(21, 101, 192, 0.2)'
			            ],
			            borderColor: [
			                'rgba(21, 101, 192, 1)'
			            ],
			            borderWidth: 2
			        }],
			    },
			    options: {
			    	responsive: true,
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                }
			            }]
			        }
			    }
			});

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});
	
}