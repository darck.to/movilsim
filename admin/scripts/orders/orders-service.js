//AQUI EMPIEZA LA ACCION
$(document).ready(function(){
	//1 BLACKLIST 2 OPERATORS 3 MODEL
	$('.muestra').on('click',function(e){
		$('.muestra').removeClass('blue-grey darken-1');
		$(this).addClass('blue-grey darken-1');
		var id = $(this).attr('val');
		if (id == 1) {
			$('.muestra-title').html('Blacklist Service')
		} else if (id == 2) {
			$('.muestra-title').html('Operator Services')
		} else if (id == 3) {
			$('.muestra-title').html('Model Services')
		}
		$('#source').attr('val',id);
		$('#source').html('');
		$('#result').html('');
		cargUlsApi(id);
	})
});

function cargUlsApi(e) {
	$.ajax({
		type: 'post',
		url: 'php/orders/orders-service-api.php',
		dataType: "json",
		contentType: "application/json",
		context: document.body,
		success: function(data) {
			$.each(data, function (name, value) {
				$.each(value, function (index, content) {
					$('#source').append('<li class="collection-item grey lighten-1">' + index + '</li>');
					$.each(content, function (id, val) {
						$('#source').append('<li class="collection-item handed font-medium selectValueOrder" val="' + val.value + '">' + val.name + '</li>');
					})
				})
			})
			cargaCollectionConfig(e);

			$('.selectValueOrder').on('click', function(i){
				var id = $(this).attr('val');
				var price = prompt('(only numbers) Price:');
				var name = prompt('Name:');
				if (!price || !name) {alert('One of the values it\'s empty, try again'); return false}
				$.ajax({
					type: 'post',
					url: 'php/orders/orders-service-add.php',
					data: {
						file: e,
						value: id,
						price: price,
						name: name
					},
					context: document.body,
					success: function(data) {
						cargaCollectionConfig(e);
					},
					error: function(xhr, tst, err) {
						console.log(err);
					}
				});
			})

		},
		error: function(xhr, tst, err) {
				console.log(err);
		}
	});
}

function cargaCollectionConfig(e) {
	$('#result').html('');
	$.ajax({
		type: 'post',
		url: 'php/orders/orders-service-config.php',
		data: {
			index: e
		},
		dataType: "json",
		context: document.body,
		success: function(data) {
			$.each(data, function (name, value) {
				$('#result').append('<li class="collection-item handed font-medium delValue" val="' + value.value + '">' + value.name + ' | $' + value.price + '</li>');
			})

			$('.delValue').on('click', function(i){
				var id = $(this).attr('val');
				if (confirm("Confirm Delete of: " + $(this).html())) {
					$.ajax({
						type: 'post',
						url: 'php/orders/orders-service-del.php',
						data: {
							file: e,
							value: id
						},
						context: document.body,
						success: function(data) {
							cargaCollectionConfig(e);
						},
						error: function(xhr, tst, err) {
							console.log(err);
						}
					});
				}
			})
		},
		error: function(xhr, tst, err) {
				console.log(err);
		}
	});
}
