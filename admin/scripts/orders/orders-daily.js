//HOY
var today = new Date();

$(document).ready(function(){

	var editor = new MediumEditor('.editable', {
		toolbar: {
	        buttons: ['bold', 'italic', 'underline', 'h2', 'h4', 'quote']
	    }
	});

	//OBTENEMOS LOS DATOS DE JSON
	getcalendarDaily();

});

function getcalendarDaily(e) {

	$.ajax({
	    cache: 'false',
	    url: "php/orders/orders-getData.php",
	    context: document.body,
	    success: function(data) {

	    	//CALENDARIO
	      	$('#daily').fullCalendar({
	      		nowIndicator: true,
	      		height: 650,
				defaultView: 'agendaDay',
				displayEventTime: true,
				navLinks: true,
			    eventLimit: true,
				header: {
					center: '',
					right: ''
				},
				events: data
			})
	      
	    },
	    error: function(xhr, tst, err) {
	      console.log(err);
	    }
	})

};