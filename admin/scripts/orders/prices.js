//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaBundle();

});

//Guardamos los datos del formulario de edicion de perfil
$('.sendBundle').on('click',function(event) {
	var n = $(this).attr('val');
	var paquete = $('#paq'+n).val();
	var hour = $('#hour'+n).val();
	var precio = $('#pri'+n).val();

	$.ajax({
		type: 'post',
		url: 'php/orders/edita-bundle.php',
		data: {
	    	number : n,
	    	paquete : paquete,
	    	hour : hour,
	    	precio : precio
	    },
		dataType: "html",
		context: document.body,
        success: function(result) {
        	  M.toast({html: result});
        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});
});

function cargaBundle() {
	$.ajax({
		type: 'post',
		url: 'php/orders/carga-bundle.php',
		dataType: "html",
		context: document.body,
        success: function(result) {
        	 $.each(JSON.parse(result), function(index, value) {
        	 	var n = index + 1;
                $('#paq'+n).val(value.nombre);
                $('#hour'+n).val(value.duracion);
                $('#pri'+n).val(value.discount);
            });
        	 $('input').focus();
        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});
}