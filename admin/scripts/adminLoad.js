$(document).ready(function(){

	$('.collapsible').collapsible();
	$('.sidenav').sidenav();

	$.ajax({
		url: 'template/dashboard/dashboard.php',
		dataType: "html",
		context: document.body,

		success: function(data) {

			$('#bodyContent').html(data);
			$('html, body').animate({ scrollTop: 0 }, 'fast');

		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

});

function apiFunction() {
  
  $('#apiBox').toggleClass('hide');

  $.ajax({
    type: 'post',
    url: 'apo/carga-servicios.php',
    data: JSON.stringify({
      src: 1
    }),
    dataType: "json",
    context: document.body,success: function(data) {
      $('#apiBox').html('');
      $('#apiBox').append('<i class="material-icons right handed reloadApi">refresh</i>');
      $.each(data, function (name, value) {
  			$('#apiBox').append('<span>' + value.SERVICEID + ' ' + value.SERVICENAME + ' ' + value.CREDIT + '</span>');
      });
    },

    error: function(xhr, tst, err) {
      console.log(err);
    }
    
  });
  
}