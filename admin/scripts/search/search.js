// instanciate new modal
var modal = new tingle.modal({
  footer: true,
  stickyFooter: false,
  closeMethods: ['overlay', 'escape'],
  closeLabel: "Close",
  cssClass: ['custom-class-1', 'custom-class-2'],
  onOpen: function() {
  },
  onClose: function() {
  },
  beforeClose: function() {
    $('#unlockCode').remove();
	$('.sendUnlockCode').remove();
    return true;
  }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {
  modal.close();
});

$(document).ready(function() {

	$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
      return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
	});
	var jsonSearch;
	$.ajax({
		url: "php/search/search.php",
		dataType: "json",
		success: function(html){
			jsonSearch = html;
		}
	});

	//GENERAL SEARCH
	$("input.omniSearch").on("keyup", function(e) {
		// Set Search String
		var search_string = $(this).val().toLowerCase();
		if (e.which == 13) {
			$('#results').html('');
			return false;    //<---- Add this line
		}
		// Do Search
		if (search_string == '') {
			$('#results').html('');
			$("#results").fadeOut();
			$('#results-container').addClass('hide');
		}else{
			$("#results").fadeIn();
			$('#results-container').removeClass('hide');
      $.each(jsonSearch, function (id, key) {
      	$.each(this, function (name, value) {
      		if (key.Name == 'IMEI Request') {var kind = 'check'} else {var kind = 'order'}
      		str = JSON.stringify(value);
          //REVISA SI ES LA CADENA DEL CORREO, O DEL NUMERO DE ORDEN O DEL FOLIO
          if (name == 'Folio' || name == 'Order' || name == 'IMEI') {
        		if (str.toLowerCase().indexOf(search_string) >= 0) {
        			$('#results').append('<h6 class="searchItem" val=' + key.Folio + '_' + kind + '_' + key.Order + '><b><span class="grey-text">' + name + ':</span> ' + value + '</b></h6>');
            }
      		} else if (name == 'Email') {
            if (str.toLowerCase().indexOf(search_string) >= 0) {
        			$('#results').append('<h6 class="searchClient" val=' + key.Email + '><b><span class="grey-text">' + name + ':</span> ' + value + '</b></h6>');
            }
          }
      	});
      });
		}

		//ELIMINADOR CUALQUIERA QUE YA NO COINCIDA
		$('#results h6:not(:contains(' + search_string + '))').remove();
		var seen = {};
		$('#results h6').each(function() {
	    var txt = $(this).text();
	    if (seen[txt])
        $(this).remove();
	    else
        seen[txt] = true;
		});

		//BUSQUEDA DE ITEMS
		$(".searchItem").on("click", function(e) {
			var find = $(this).attr('val');
			$.ajax({
				type: "POST",
				url: "php/search/search-data.php",
				data: { find: find },
				cache: false,
				success: function(data){
					var modalContent;
					$.each(data, function(index, marca) {
						modalContent = '<div class="row">';
						modalContent += '<div class="col s12 m4 offset-m2">';
						modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + marca['Folio'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + marca['Date'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Client: </em></i><i class="right"><b>' + marca['Name'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Email: </em></i><i class="right grey-text text-darken-3"><b>' + marca['Email'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Order No.: </em></i><i class="right"><b id="orderNo">' + marca['Order'] + '</b></i></p>';
						modalContent += '</div>';
						modalContent += '<div class="col s12 m4">';
						modalContent += '<p class="fiv-p"><i><em>IMEI: </em></i><i class="right grey-text text-darken-3"><b>' + marca['IMEI'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Company: </em></i><i class="right"><b>' + marca['Brand'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Model: </em></i><i class="right"><b>' + marca['Equipment'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Cost: </em></i><i class="right"><b>$&nbsp;' + marca['Cost'] + '</b></i></p>';
						//STATUS Y COLOR DEL ESTATUS
						if (marca['Status'] == 0) {var color = 'red-text text-lighten-2';} else if (marca['Status'] == 1) { var color = 'green-text text-darken-1';}
						modalContent += '<p class="fiv-p"><i><em>Status: </em></i><i class="right"><i class="material-icons '+color+'">payment</i><b></b></i></p>';
						modalContent += '<input id="codeUnlock" type="hidden" value="' + marca['Code'] + '">';
						modalContent += '</div>';
						modalContent += '</div>';
					});
					//SET MODAL
					modal.setContent(modalContent);
					modal.open();
				}
			});
      $("input.omniSearch").val('');
      $('#results').html('');
			$("#results").fadeOut();
			$('#results-container').addClass('hide');
		});

    //BUSQUEDA DE ITEMS
		$(".searchClient").on("click", function(e) {
			var mail = $(this).attr('val');
      //SOLICITAMOS LAS RESPUESTAS
      $.ajax({
        type: 'post',
        url: 'php/search/search-perfil.php',
        dataType: "json",
        data: { mail: mail },
        context: document.body,
        success: function(result) {
          //CREAMOS LA TABLA EN MAIN BODY Y AHI CARGAMOS TABLA
          var tableHtml ='<div class="row">';
            tableHtml +='<div class="row">';
              tableHtml +='<div class="col s12">';
                tableHtml +='<span class="admin-title">Client: <b>' + mail + '</b></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>';
              tableHtml +='</div>';
            tableHtml +='</div>';
            tableHtml +='<div class="col s12 m10 offset-m1">';
              tableHtml +='<table id="table" class="display" style="width:100%">';
                tableHtml +='<thead>';
                  tableHtml +='<tr>';
                    tableHtml +='<th>Folio</th>';
                    tableHtml +='<th>Fecha</th>';
                    tableHtml +='<th>#</th>';
                    tableHtml +='<th>IMEI</th>';
                    tableHtml +='<th>Marca</th>';
                    tableHtml +='<th>Equipo</th>';
                    tableHtml +='<th>Precio</th>';
                    tableHtml +='<th>Status</th>';
                  tableHtml +='</tr>';
                tableHtml +='</thead>';
                tableHtml +='<tfoot>';
                  tableHtml +='<tr>';
                    tableHtml +='<th>Folio</th>';
                    tableHtml +='<th>Fecha</th>';
                    tableHtml +='<th>#</th>';
                    tableHtml +='<th>IMEI</th>';
                    tableHtml +='<th>Marca</th>';
                    tableHtml +='<th>Equipo</th>';
                    tableHtml +='<th>Precio</th>';
                    tableHtml +='<th>Status</th>';
                  tableHtml +='</tr>';
                tableHtml +='</tfoot>';
              tableHtml +='</table>';
            tableHtml +='</div>';
          tableHtml +='</div>';
          $('#bodyContent').html(tableHtml);
          var table = $('#table').DataTable({
            data: result,
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'pdf'
            ],
            //ROW CONDITIONS
            'rowCallback': function(row, data, index){
              if(data[7] != 0){
                  $(row).find('td:eq(7)').html('<i class="far fa-thumbs-up has-text-link"></i>');
              } else {
                  $(row).find('td:eq(7)').html('<i class="far fa-thumbs-down has-text-danger"></i>');
              }
            }
          });

          //EVENTOS DE CLICK
          $('#table tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();

            var modalContent = '<div class="row has-text-white contentColumn">';
              modalContent += '<div class="col s12 m6">';
                modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + data[0] + '</b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + data[1] + '</b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>Order No.: </em></i><i class="right"><b id="orderNo">' + data[2] + '</b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>Company: </em></i><i class="right"><b>' + data[4] + '</b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>Model: </em></i><i class="right"><b>' + data[5] + '</b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>IMEI: </em></i><i class="right"><b>' + data[3] + '</b></i></p>';
              modalContent += '</div>';
              modalContent += '<div class="col s12 m6">';
                modalContent += '<p class="fiv-p"><i><em>Cost: </em></i><i class="right"><b>$&nbsp;' + data[6] + '</b></i></p>';
                //STATUS Y COLOR DEL ESTATUS
                if (data[7] == 0) {var icon = '<i class="far fa-thumbs-down has-text-danger right"></i>'; var button ='';} else if (data[7] == 1) { var icon = '<i class="far fa-thumbs-up has-text-white right"></i>'; var button = '<button class="button is-success ma-oneinput-format border-c-no shadow-blue-darker msgButton" val=' + data[2] + ' fol=' + data[0] + ' msg="' + data[11] + '">Leave Feedback&nbsp;<i class="far fa-comment-dots"></i></button>';}
                modalContent += '<p class="fiv-p"><i><em>Estatus: </em></i>' + icon + '</i></p>';
                modalContent += '<p class="fiv-p"><i><em>Ticket MP (if any): </em></i><i class="right"><b><a class="has-text-white ref-text-white" href="' + data[10] + '" target=_blank><i class="fas fa-print"></i></a></b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>Service Info: </em></i><i class="right"><b>' + data[9] + '</b></i></p>';
                modalContent += '<p class="fiv-p"><i><em>Unlock Code: </em></i><i class="right"><b>' + data[8] + '</b></i></p>';
              modalContent += '</div>';
            modalContent += '</div>';

            //SET MODAL
            modal.setContent(modalContent);
            $('.tingle-modal-box__content').addClass('background-p-1');
            modal.open();
           });

          },
          error: function(xhr, tst, err) {
            console.log(err)
          }
      });

      $("input.omniSearch").val('');
      $('#results').html('');
			$("#results").fadeOut();
			$('#results-container').addClass('hide');
		});

	});

});
