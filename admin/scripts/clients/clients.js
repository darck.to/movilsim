// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {

    modal.close();

});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

  $.ajax({
		url: 'php/clients/carga-tabla.php',
		contentType: "application/json",
    context: document.body,
    success: function(result) {
    	//CARGAMOS TABLA
    	var table = $('#table').DataTable({
    		data: result,
    		dom: 'Bfrtip',
    		buttons: [
          'excel', 'pdf', 'print'
        ],
    	});

    	//EVENTOS DE CLICK
    	$('#table tbody').on('click', 'tr', function () {
    		var data = table.row( this ).data();
        var mail = data[0];

        //SOLICITAMOS LAS RESPUESTAS
        $.ajax({
          type: 'post',
          url: 'php/search/search-perfil.php',
          dataType: "json",
          data: { mail: mail },
          context: document.body,
          success: function(result) {
            //CREAMOS LA TABLA EN MAIN BODY Y AHI CARGAMOS TABLA
            var tableHtml ='<div class="row">';
              tableHtml +='<div class="row">';
                tableHtml +='<div class="col s12">';
                  tableHtml +='<span class="admin-title" onclick=menuNav("template/clients/clients.php")><b class="handed red-text">&nbsp;&lsaquo;&nbsp;</b>Client: <b>' + mail + '</b></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>';
                tableHtml +='</div>';
              tableHtml +='</div>';
              tableHtml +='<div class="col s12 m10 offset-m1">';
                tableHtml +='<table id="table" class="display" style="width:100%">';
                  tableHtml +='<thead>';
                    tableHtml +='<tr>';
                      tableHtml +='<th>Folio</th>';
                      tableHtml +='<th>Fecha</th>';
                      tableHtml +='<th>#</th>';
                      tableHtml +='<th>IMEI</th>';
                      tableHtml +='<th>Marca</th>';
                      tableHtml +='<th>Equipo</th>';
                      tableHtml +='<th>Precio</th>';
                      tableHtml +='<th>Status</th>';
                    tableHtml +='</tr>';
                  tableHtml +='</thead>';
                  tableHtml +='<tfoot>';
                    tableHtml +='<tr>';
                      tableHtml +='<th>Folio</th>';
                      tableHtml +='<th>Fecha</th>';
                      tableHtml +='<th>#</th>';
                      tableHtml +='<th>IMEI</th>';
                      tableHtml +='<th>Marca</th>';
                      tableHtml +='<th>Equipo</th>';
                      tableHtml +='<th>Precio</th>';
                      tableHtml +='<th>Status</th>';
                    tableHtml +='</tr>';
                  tableHtml +='</tfoot>';
                tableHtml +='</table>';
              tableHtml +='</div>';
            tableHtml +='</div>';
            $('#bodyContent').html(tableHtml);
            var table = $('#table').DataTable({
              data: result,
              responsive: true,
              dom: 'Bfrtip',
              buttons: [
                  'excel', 'pdf', 'print'
              ],
              //ROW CONDITIONS
              'rowCallback': function(row, data, index){
                if(data[7] != 0){
                    $(row).find('td:eq(7)').html('<i class="far fa-thumbs-up has-text-link"></i>');
                } else {
                    $(row).find('td:eq(7)').html('<i class="far fa-thumbs-down has-text-danger"></i>');
                }
              }
            });

            //EVENTOS DE CLICK
            $('#table tbody').on('click', 'tr', function () {
              var data = table.row( this ).data();

              var modalContent = '<div class="row has-text-white contentColumn">';
                modalContent += '<div class="col s12 m6">';
                  modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + data[0] + '</b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + data[1] + '</b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Order No.: </em></i><i class="right"><b id="orderNo">' + data[2] + '</b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Company: </em></i><i class="right"><b>' + data[4] + '</b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Model: </em></i><i class="right"><b>' + data[5] + '</b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>IMEI: </em></i><i class="right"><b>' + data[3] + '</b></i></p>';
                modalContent += '</div>';
                modalContent += '<div class="col s12 m6">';
                  modalContent += '<p class="fiv-p"><i><em>Cost: </em></i><i class="right"><b>$&nbsp;' + data[6] + '</b></i></p>';
                  //STATUS Y COLOR DEL ESTATUS
                  if (data[7] == 0) {var icon = '<i class="far fa-thumbs-down has-text-danger right"></i>'; var button ='';} else if (data[7] == 1) { var icon = '<i class="far fa-thumbs-up has-text-white right"></i>'; var button = '<button class="button is-success ma-oneinput-format border-c-no shadow-blue-darker msgButton" val=' + data[2] + ' fol=' + data[0] + ' msg="' + data[11] + '">Leave Feedback&nbsp;<i class="far fa-comment-dots"></i></button>';}
                  modalContent += '<p class="fiv-p"><i><em>Estatus: </em></i>' + icon + '</i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Ticket MP (if any): </em></i><i class="right"><b><a class="has-text-white ref-text-white" href="' + data[10] + '" target=_blank><i class="fas fa-print"></i></a></b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Service Info: </em></i><i class="right"><b>' + data[9] + '</b></i></p>';
                  modalContent += '<p class="fiv-p"><i><em>Unlock Code: </em></i><i class="right"><b>' + data[8] + '</b></i></p>';
                modalContent += '</div>';
              modalContent += '</div>';

              //SET MODAL
              modal.setContent(modalContent);
              $('.tingle-modal-box__content').addClass('background-p-1');
              modal.open();
             });

            },
            error: function(xhr, tst, err) {
              console.log(err)
            }
        });

    	});
    },
		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}
