function menuNav(link) {
  var ruta = link;

  //Separamos la caddena para el Titulo
  var breadCrumbs = ruta.split('/');

  $.ajax({
    url: ruta,
    dataType: "html",
    context: document.body,

    success: function(data) {

      $('#bodyContent').html(data);
      $('html, body').animate({ scrollTop: 0 }, 'fast');
      //$('#pageTitle').html(breadCrumbs[0].toUpperCase() + ' | ' + breadCrumbs[1].charAt(0).toUpperCase() + breadCrumbs[1].slice(1));

    },

    error: function(xhr, tst, err) {
      console.log(err);
    }
    
  });
  
}