// instanciate new modal
var modal = new tingle.modal({
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {

    	//Opciones del editor de texto
		var editor = new MediumEditor('.editable', {
			imageDragging: false,
			toolbar: {
		    	buttons: ['bold', 'underline', 'h5', 'h6', 'quote', 'orderedlist'],// options go here
		    	disableExtraSpaces: true
			}
		});

    	//SAVE NEW BRAND
		$(".saveBrand").on("click", function(e) {
			var name = $('#nameModel').val();
			var type = $('#type').val();
			var form_data = new FormData();
		    form_data.append("name", name);
		    form_data.append("type", type);
		    form_data.append("file1", document.getElementById('fileModel').files[0]);
		    //GUARDAR BRAND
		    $.ajax({
		        type: 'post',
		        url: 'php/models/guarda-models.php',
		        data: form_data,
				cache: false,
		        contentType: false,
		        processData: false,
				dataType: "html",
		        context: document.body,
		        success: function(data) {

		        	M.toast({html: data});
		        	$('#modelCollection').html('');
		        	cargaModels();
		            modal.close();

		        },

		        error: function(xhr, tst, err) {
		            console.log(err);
		        }

		    });
		});

		//SAVE NEW EQUIPMENT
		$(".savePhone").on("click", function(e) {
			var fatherId = $('#fatherId').val();
			var name = $('#nameModel').val();
			var type = $('#type').val();
			var desc = $('.editable').html();
			var form_data = new FormData();
			form_data.append("fatherId", fatherId);
			form_data.append("name", name);
			form_data.append("type", type);
			form_data.append("desc", desc);
			form_data.append("file1", document.getElementById('fileModel').files[0]);
			//GUARDAR BRAND
			$.ajax({
			    type: 'post',
			    url: 'php/models/guarda-models.php',
			    data: form_data,
				cache: false,
			    contentType: false,
			    processData: false,
				dataType: "html",
			    context: document.body,
			    success: function(data) {

			    	M.toast({html: data});
			    	$('#modelCollection').html('');
			    	cargaModels();
			        modal.close();

			    },

			    error: function(xhr, tst, err) {
			        console.log(err);
			    }

			});
		});

		//DELETE BUTTON
		$(".deleteButton").on("click", function(e) {
			var id = $(this).attr('id');
			var type = $(this).attr('type');
			var form_data = new FormData();
		    form_data.append("id", id);
		    form_data.append("type", type);

			//GUARDAR BRAND
		    $.ajax({
		        type: 'post',
		        url: 'php/models/borra-models.php',
		        data: form_data,
				cache: false,
		        contentType: false,
		        processData: false,
				dataType: "html",
		        context: document.body,
		        success: function(data) {

		        	M.toast({html: data});
		        	$('#modelCollection').html('');
		        	cargaModels();
		        	modal.close();
		        },

		        error: function(xhr, tst, err) {
		            console.log(err);
		        }

		    });
		});

		//STAR ITEM
		$(".portadaBrand").on("click", function(e) {
			var id = $(this).attr('id');
			var form_data = new FormData();
		    form_data.append("id", id);

			//GUARDAR BRAND
		    $.ajax({
		        type: 'post',
		        url: 'php/models/portada-models.php',
		        data: form_data,
				cache: false,
		        contentType: false,
		        processData: false,
				dataType: "html",
		        context: document.body,
		        success: function(data) {

		        	M.toast({html: data});
		        	$('#modelCollection').html('');
		        	cargaModels();
		        	modal.close();
		        },

		        error: function(xhr, tst, err) {
		            console.log(err);
		        }

		    });
		});

    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaModels();

});

function cargaModels(e) {

	$.ajax({
		url: 'php/models/carga-models.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	var lastId;
        	var starColor;

        	$('#firstBullet').html('<b class="handed addModel">ADD Brand</b>');

            $.each(result, function (name, value) {

            	if (value.portada == "1") {
            		starColor = "amber-text";
            	} else {
            		starColor = "grey-text";
            	}
          
    			    $('#modelCollection').append("<li class='collection-item'>" + value.name + "<i class='material-icons left " + starColor + " handed starBrand' val=" + value.id + ">star</i><i class='material-icons right circle light-blue darken-4 white-text handed modelClick' val=" + value.id + " name='" + value.name + "'>chevron_right</i><i class='material-icons right red-text handed deleteBrand' val=" + value.id + " type=0>cancel</i><img class='right adm-model-img handed editImg1' opt='1' val=" + value.id + " src='../img/" + value.img + "'></li>")
    			lastId = value.id;

            });

            //CALL EDIT FUNCTION 
			$(".editImg1").on("click", function(e) {

				var id = $(this).attr('val');
				var opt = $(this).attr('opt');

				editThis(id, opt);

			})

            //CALL DELETE FUNCTION 
			$(".deleteBrand").on("click", function(e) {

				var id = $(this).attr('val');
				var type = $(this).attr('type');

				deletePhone(id, type);

			})

			//CLICKS MODELOS
			$(".starBrand").on("click", function(e) {

				var id = $(this).attr('val');

				var text = "<h4>Add to Home</h4>";
				text += "<div class='row grey lighten-4 border-r center-align'>";
				text += "<div class='col s12 input-field center-align'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text portadaBrand' id="+id+">Save to Main Page</a>";
				text += "</div>";
				text += "</div>";

				// set content
            	modal.setContent(text);
				modal.open();

			});

            //CLICKS MARCAS
			$(".addModel").on("click", function(e) {

				var text = "<h4>Add brand</h4>";
				text += "<div class='row'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<input id='nameModel' type='text'>";
				text += "<input id='type' type='hidden' value=0>";
				text += "<label for='nameModel'>Brand Name</label>";
				text += "</div>";
				text += "<div class='col s12 m6 file-field input-field'>";
				text += "<div class='btn'>";
				text += "<span>Brand Logo</span>";
				text += "<input id='fileModel' type='file' accept='image/*'>";
				text += "</div>";
				text += "<div class='file-path-wrapper'>";
				text += "<input class='file-path validate' type='text'>";
				text += "</div>";
				text += "</div>";
				text += "</div>";
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text saveBrand'>Save Brand</a>";
				text += "</div>";
				text += "</div>";

				// set content
            	modal.setContent(text);
				modal.open();

			});

			//CLICKS MODELOS
			$(".modelClick").on("click", function(e) {
				
				var id = $(this).attr('val');
				var name = $(this).attr('name');


				$.ajax({
					url: 'php/models/carga-equipos.php',
					contentType: "application/json",
			        context: document.body,
			        success: function(result) {

			        	$('#equipCollection').html('');

						$('#secBullet').html('<b class="handed addEquipo" val=' + id + '>ADD Model for Brand: ' + name + '</b>');

			            $.each(result, function (name, value) {


			            	if (value.idm == id) {

			    				$('#equipCollection').append("<li class='collection-item' val=" + value.id + ">" + value.name + "&nbsp;|&nbsp;<i class='material-icons right red-text handed deleteModel' val=" + value.id + " type=1>cancel</i><img class='right adm-model-img handed editImg2' opt='2' val=" + value.id + " idm=" + value.id_m + " src='../img/" + value.img + "'></li>");
			            	
			            	} else {
			            	}
			          

			            });

			            //CALL EDIT FUNCTION 
						$(".editImg2").on("click", function(e) {

							var id = $(this).attr('val');
							var opt = $(this).attr('opt');
							var idm = $(this).attr('idm');

							editThis(id, opt, idm);

						})

			            //CALL DELETE FUNCTION 
						$(".deleteModel").on("click", function(e) {

							var id = $(this).attr('val');
							var type = $(this).attr('type');

							deletePhone(id, type);

							$(this).parent('li').css('display','none');

						})

			            $('html, body').animate({ scrollTop: 0 }, 'fast');

			            //CLICKS MODELOS
						$(".addEquipo").on("click", function(e) {

							var fatherId = $(this).attr('val');

							var text = "<h4>Add Model</h4>";
							text += "<div class='row'>";
							text += "<div class='col s12 m6 input-field'>";
							text += "<input id='nameModel' type='text'>";
							text += "<input id='fatherId' type='hidden' value=" + fatherId + ">";
							text += "<input id='type' type='hidden' value=1>";
							text += "<input id='serviceid' type='hidden'>";
							text += "<input id='servicename' type='hidden'>";
							text += "<label for='nameModel'>Model Name</label>";
							text += "</div>";
							text += "<div class='col s12 m6 file-field input-field'>";
							text += "<div class='btn'>";
							text += "<span>Model Logo</span>";
							text += "<input id='fileModel' type='file' accept='image/*'>";
							text += "</div>";
							text += "<div class='file-path-wrapper'>";
							text += "<input class='file-path validate' type='text'>";
							text += "</div>";
							text += "</div>";
							text += "</div>";
							text += "<div class='row grey lighten-4 border-r'>";
							text += "<div class='editable text-editor'></div>";
							text += "</div>";
							text += "<div class='row grey lighten-4 border-r'>";
							text += "<div class='col s12 m6 input-field'>";
							text += "<a class='tingle-btn tingle-btn--primary white-text savePhone'>Save Brand</a>";
							text += "</div>";
							text += "</div>";

							// set content
			            	modal.setContent(text);
							modal.open();

						});

			        },

					error: function(xhr, tst, err) {
						console.log(err);
					}

				});

			});

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}

function deletePhone(i, t){

	var id = i;
	var type = t;

	var text = "<h4 class='center-align'>Confirm your Action</h4>";
	text += "<div class='row center-align grey lighten-3 border-r'>";
	text += "<div class='col s12 input-field'>";
	text += "<a class='tingle-btn tingle-btn--danger white-text center-align deleteButton' id="+id+" type="+type+"><h5><b>DELETE</b></h5></a>";
	text += "</div>";

	// set content
	modal.setContent(text);
	modal.open();

}

function editThis(i, o, im){

	var id = i;
	var opt = o;

	//CARGA BRAND
    $.ajax({
        type: 'post',
        url: 'php/models/carga-edit.php',
        data: JSON.stringify({
                	id : id,
                	opt : opt
                }),
        dataType: "json",
        context: document.body,
        success: function(result) {
			
			var id;
			var name;
			var img;

			if (opt == 1) {
				$.each(result, function (key, value) {
					id = value.id;
					name = value.name;
					img = value.img;
				});
				var text = "<h4>Edit Brand</h4>";
				text += "<div class='row'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<input id='nameModel' type='text' value='" + name + "'>";
				text += "<label for='nameModel'>Brand Name</label>";
				text += "</div>";
				text += "<div class='col s12 m6 file-field input-field'>";
				text += "<div class='btn'>";
				text += "<span>Brand Logo</span>";
				text += "<input id='fileModel' type='file' accept='image/*'>";
				text += "</div>";
				text += "<div class='file-path-wrapper'>";
				text += "<input class='file-path validate' type='text'>";
				text += "</div>";
				text += "</div>";
				text += "<div class='col s12 m6 right'>";
				text += "<img class='right responsive-img' src='../img/" + img + "'>";
				text += "<small>Uploading another image will delete the current one</small>";
				text += "</div>";
				text += "</div>";
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text editBrand' opt=1 id=" + id + ">Edit Brand</a>";
				text += "</div>";
				text += "</div>";
				// set content
				modal.setContent(text);
				modal.open();

				$('input').focus();

			} else if (opt == 2) {
				var idm;
				
				$.each(result, function (key, value) {
					id = value.id;
					idm = value.idm;
					name = value.name;
					img = value.img;
					desc = value.desc;
				});
				var text = "<h4>Edit Model</h4>";
				text += "<div class='row'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<input id='nameModel' type='text' value='" + name + "'>";
				text += "<label for='nameModel'>Model Name</label>";
				text += "<input id='serviceid' type='hidden'>";
				text += "<input id='servicename' type='hidden'>";
				text += "</div>";
				text += "<div class='col s12 m6 file-field input-field'>";
				text += "<div class='btn'>";
				text += "<span>Model Logo</span>";
				text += "<input id='fileModel' type='file' accept='image/*'>";
				text += "</div>";
				text += "<div class='file-path-wrapper'>";
				text += "<input class='file-path validate' type='text'>";
				text += "</div>";
				text += "</div>";
				text += "<div class='col s12 m6'>";
				text += "<img class='right responsive-img' src='../img/" + img + "'>";
				text += "<small>Uploading another image will delete the current one</small>";
				text += "</div>";
				text += "</div>";
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='editable text-editor'>"; + desc + "</div>";
				text += "</div>";
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text editBrand' opt=2 id=" + id + " idm=" + idm + ">Edit Model</a>";
				text += "</div>";
				text += "</div>";
				// set content
				modal.setContent(text);
				modal.open();
				
				$('input').focus();
			}

			//MANDA A EDITAR Y GUARDAR LOS CAMBIOS
			$(".editBrand").on("click", function(e) {
				var opt = $(this).attr('opt');
				var id = $(this).attr('id');
				if (opt == 1) {
					var name = $('#nameModel').val();
					var type = 1;
					var form_data = new FormData();
				    form_data.append("id", id);
				    form_data.append("name", name);
				    form_data.append("type", type);
				    if( document.getElementById("fileModel").files.length == 0 ){
					} else if (document.getElementById("fileModel").files.length == 1) {
				    	form_data.append("file1", document.getElementById('fileModel').files[0]);
					}
				} else if (opt == 2) {
					var fatherId = $('#fatherId').val();
					var name = $('#nameModel').val();
					var type = 2;
					var desc = $('.editable').html();
					var form_data = new FormData();
					form_data.append("fatherId", fatherId);
					form_data.append("id", id);
					form_data.append("name", name);
					form_data.append("type", type);
					form_data.append("desc", desc);
					if( document.getElementById("fileModel").files.length == 0 ){
					} else if (document.getElementById("fileModel").files.length == 1) {
				    	form_data.append("file1", document.getElementById('fileModel').files[0]);
					}
				}
				//GUARDAR BRAND
			    $.ajax({
			        type: 'post',
			        url: 'php/models/guarda-edit.php',
			        data: form_data,
					cache: false,
			        contentType: false,
			        processData: false,
					dataType: "html",
			        context: document.body,
			        success: function(data) {

			        	M.toast({html: data});
			            modal.close();
			        	menuNav('template/models/models.php');

			        },

			        error: function(xhr, tst, err) {
			            console.log(err);
			        }

			    });

			});

        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

}