var serviciosArray;
var bundleData;
var tagData;
var equipmentArray;
var edit1 = 0;
var edit2 = 0;

// instanciate new modal
var modal = new tingle.modal({
  stickyFooter: false,
  closeMethods: ['overlay', 'button', 'escape'],
  cssClass: ['custom-class-1', 'custom-class-2'],
  onOpen: function() {

  //Opciones del editor de texto
	var editor = new MediumEditor('.editable', {
		imageDragging: false,
		toolbar: {
    	buttons: ['bold', 'underline', 'h5', 'h6', 'quote', 'orderedlist'],// options go here
    	disableExtraSpaces: true
		}
	});

  	//SAVE NEW COUNTRY
	$(".saveCountry").on("click", function(e) {
		var name = $('#nameModel').val();
		var type = $('#type').val();
		var form_data = new FormData();
    form_data.append("name", name);
    form_data.append("type", type);
    //GUARDAR COUNTRY
    $.ajax({
      type: 'post',
      url: 'php/models/guarda-country.php',
      data: form_data,
	    cache: false,
      contentType: false,
      processData: false,
	    dataType: "html",
      context: document.body,
      success: function(data) {
      	M.toast({html: data});
        modal.close();
      	menuNav('template/models/countries.php');
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    });
	});

	//SAVE NEW OPERATOR
	$(".saveOperator").on("click", function(e) {
    event.preventDefault();
    //REVISA QUE LOS TAGS Y PRICES ESTEN COMPLETOS ALSO NAME
    if (!$('#nameModel').val()) {
      alert('Name of Operator not Set');
      return false;
    }
		var fatherId = $('#fatherId').val();
		var name = $('#nameModel').val();
    var min = $('#minModel').val();
    var hour = $('#hourModel').val();
    var day = $('#dayModel').val();
    var rate = $('#rateModel').val();
    if (!$('#rateModel').val()) {
      alert('Rate of Succes not Set');
      return false;
    }
		var cost = $('#costModel').val();
		var type = $('#type').val();
		var idser = $('#serviceList').val();
		bundleData = $("input[type=radio]:checked").map(function() {
			return $(this).val();
		}).get().join( "," );
		tagData = $("input[type=radio]:checked").map(function() {
			return $(this).attr('tag');
		}).get().join( "," );
		equipmentArray = $('.selectEquipment').map(function() {
			if ( $(this).hasClass('lc--active') ) {
				return $(this).attr('val');
			}
		}).get().join( "," );
    if (!$('.labelInput').val()) {
      alert('At least one service tag it\s missing, please check your tags');
      return false;
    }
    if ($('.editable').html() == 'Unlock instructions for mail response...') {
      alert('Unlock description missing');
      return false;
    }
		var desc = $('.editable').html();
		var form_data = new FormData();
		form_data.append("fatherId", fatherId);
		form_data.append("name", name);
		form_data.append("type", type);
    form_data.append("min", min);
    form_data.append("hour", hour);
    form_data.append("day", day);
    form_data.append("rate", rate);
		form_data.append("cost", cost);
		form_data.append("idser", idser);
		form_data.append("idbun", bundleData);
		form_data.append("idtag", tagData);
		form_data.append("eqArr", equipmentArray);
		form_data.append("desc", desc);
		//GUARDAR OPERATOR
		$.ajax({
	    type: 'post',
	    url: 'php/models/guarda-country.php',
	    data: form_data,
		  cache: false,
	    contentType: false,
	    processData: false,
		  dataType: "html",
	    context: document.body,
	    success: function(data) {
	    	M.toast({html: data});
	      modal.close();
	    	menuNav('template/models/countries.php');
	    },
	    error: function(xhr, tst, err) {
	      console.log(err);
	    }
		});
	});

  $(".portadaCountry").on("click", function(e) {
      var id = $(this).attr('id');
      var form_data = new FormData();
      form_data.append("id", id);
      //GUARDAR BRAND
      $.ajax({
        type: 'post',
        url: 'php/models/portada-country.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html",
        context: document.body,
        success: function(data) {
          M.toast({html: data});
          $('#countryCollection').html('');
          cargaCountries();
          modal.close();
        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
      });
    });

	//DELETE BUTTON
	$(".deleteButton").on("click", function(e) {
		var id = $(this).attr('id');
		var type = $(this).attr('type');
		var form_data = new FormData();
    form_data.append("id", id);
    form_data.append("type", type);

		//GUARDAR BRAND
    $.ajax({
      type: 'post',
      url: 'php/models/borra-country.php',
      data: form_data,
		  cache: false,
      contentType: false,
      processData: false,
		  dataType: "html",
      context: document.body,
      success: function(data) {
      	M.toast({html: data});
      	modal.close();
      	menuNav('template/models/countries.php');
      },

      error: function(xhr, tst, err) {
        console.log(err);
      }
    });
	});

  //PREPARA EL BUSCADOR DE OPERADORES
  var jsonSearch;
  $.ajax({
    url: "php/models/search-operators.php",
    dataType: "json",
    success: function(html){
      jsonSearch = html;
    }
  });
  //LO INSERTA EN CADA INPUT DE nameModel PARA EL UL searchResults
  //GENERAL SEARCH
  $("input#nameModel").on("keyup", function(e) {
    // Set Search String
    var search_string = $(this).val().toLowerCase();
    if (e.which == 13) {
      $('#searchResults').html('');
      return false;    //<---- Add this line
    }
    // Do Search
    if (search_string == '') {
      $('#searchResults').html('');
      $("#searchResults").fadeOut();
    }else{
      $("#searchResults").fadeIn();
      $.each(jsonSearch, function (name, value) {
        str = JSON.stringify(value.operador);
        if (str.toLowerCase().indexOf(search_string) >= 0) {
          $('#searchResults').append('<li class="grey-text blue white-text border-r-5 fiv-m fiv-p handed tagNameModel">' + value.operador + '</li>');
        }
      });
    }
    //CLICK AL TAG PARA INSERTARLO
    $('.tagNameModel').on('click', function(e){
      $('#nameModel').val($(this).html());
      $(this).remove();
    })
    //ELIMINADOR DE DUPLICADOS
		$('#searchResults li:not(:contains(' + search_string + '))').remove();
		var seen = {};
		$('#searchResults li').each(function() {
	    var txt = $(this).text();
	    if (seen[txt])
        $(this).remove();
	    else
        seen[txt] = true;
		});
  });

  },
  onClose: function() {
  },
  beforeClose: function() {
      return true;
  }
});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaCountries();

});

function cargaCountries(e) {

	$.ajax({
		url: 'php/models/carga-countries.php',
		contentType: "application/json",
    context: document.body,
    success: function(result) {
    	$('#firstBullet').html('<b class="handed addCountry">ADD Country</b>');
      $.each(result, function (name, value) {
        if (value.portada == "1") {
          starColor = "amber-text";
        } else {
          starColor = "grey-text";
        }
  			$('#countryCollection').append("<li class='collection-item'>" + value.pais + "</i><i class='material-icons left " + starColor + " handed starCountry' val=" + value.id + ">star</i><i class='material-icons right circle light-blue darken-4 white-text handed operatorClick' val=" + value.id + " name='" + value.pais + "'>chevron_right</i><i class='material-icons right red-text handed deleteCountry' val=" + value.id + " type=0>cancel</i><i class='material-icons right blue-text handed editCountry1' opt='1' val=" + value.id + ">keyboard</i>")
  			lastId = value.id;
      });
      //CLICKS MODELOS
			$(".starCountry").on("click", function(e) {
				var id = $(this).attr('val');
				var text = "<h4>Add to Home</h4>";
				text += "<div class='row grey lighten-4 border-r center-align'>";
				text += "<div class='col s12 input-field center-align'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text portadaCountry' id="+id+">Save to Main Select</a>";
				text += "</div>";
				text += "</div>";
				// set content
        modal.setContent(text);
				modal.open();
			});
      //CALL EDIT FUNCTION
  		$(".editCountry1").on("click", function(e) {
  			var id = $(this).attr('val');
  			var opt = $(this).attr('opt');
  			editThis(id, opt);
  		})
      //CALL DELETE FUNCTION
			$(".deleteCountry").on("click", function(e) {
				var id = $(this).attr('val');
				var type = $(this).attr('type');
				deleteThis(id, type);
			})
      //CLICKS ADD COUNTRY
			$(".addCountry").on("click", function(e) {
				var text = "<h4>Add Country</h4>";
				text += "<div class='row'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<input id='nameModel' type='text'>";
				text += "<input id='type' type='hidden' value=0>";
				text += "<label for='nameModel'>Brand Name</label>";
				text += "</div>";
				text += "<div class='col s12 m6 file-field input-field'>"
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text saveCountry'>Save Country</a>";
				text += "</div>";
				text += "</div>";
				// set content
        modal.setContent(text);
				modal.open();
			});
			//CLICKS OPERATORS
			$(".operatorClick").on("click", function(e) {
				var id = $(this).attr('val');
				var name = $(this).attr('name');
			  var equipname;

				$.ajax({
					url: 'php/models/carga-operators.php',
					contentType: "application/json",
	        context: document.body,
	        success: function(result) {
            //PRIMERO LEER EL ARREGLO RESULT Y LIMPIAR PARA SOLO MOSTRAR UN COLLECTION CON LAS UNICAS
            var duplicados = [];
            var unicos = [];
            $.each(result, function(name,content) {
              if (!duplicados[content.operador]) {
                duplicados[content.operador] = true;
                unicos.push(content);
              }
            });
            $('#operatorCollection').html('');
			      $('#secBullet').html('<b class="handed addOperator" val=' + id + '>ADD Operator for Country: ' + name + '</b>');
            var n = 1;
            $.each(unicos, function (name, value) {
              var equipmentsTooltip = "";
              if (id == value.idm) {
                $('#operatorCollection').append("<li class='collection-item collectionShow handed' value='" + value.operador + "'><i class='material-icons left upArrowCollection'>arrow_drop_down</i>" + value.operador + "</li>");
                $('#operatorCollection').append('<ul class="collection collection-' + value.id + ' su-hidden">');
              	//LOS SUBELEMENTOS
  		        	$.each(result, function (index, content) {
                  if (value.operador === content.operador ) {
                    $('.collection-' + value.id).append("<li class='collection-item' val=" + content.id + "><i class='font-small wrapper'  style='background-color: #" + intToRGB(hashCode(content.operador+content.id+content.idm)) + "; padding: 10px; margin-right: 10px; border-radius: 0 20px 20px 20px; font-weight: bolder; color: white !important;'>" + n + "<span class='tooltip tooltip--" + content.id + "' value=" + content.id + "></span></i>" + content.operador + "<i class='material-icons right red-text handed deleteOperator' val=" + content.id + " type=1>cancel</i><i class='material-icons right blue-text handed editCountry2' opt='2' val=" + content.id + " idm=" + content.idm + ">keyboard</i></li>");
                    n++;
                  }
    		        })
                $('#operatorCollection').append('</ul>');
              }
            });
            $('html, body').animate({ scrollTop: 0 }, 'fast');
            //CLICKS MODELOS
						$(".addOperator").on("click", function(e) {
							var fatherId = $(this).attr('val');
							var text = "<h4>Add Operator</h4>";
							text += "<div class='row'>";
								text += "<div class='row'>";
									text += "<div class='col s12 m3 input-field p-r'>";
										text += "<input id='nameModel' type='text' required>";
                    text += "<ul id='searchResults' class='white fiv-p p-a'></ul>";
										text += "<input id='fatherId' type='hidden' value=" + fatherId + ">";
										text += "<input id='type' type='hidden' value=1>";
										text += "<label for='nameModel'>Operator Name</label>";
									text += "</div>";
									text += '<div class="col s12 m9">';
										text += '<div class="input-field col s12">';
											text += '<select multiple id="serviceList">';
											text += '</select>';
											text += '<label>Add Service</label>';
										text += '</div>';
									text += '</div>';
                  text += "<div class='col s12 m2 input-field'>";
										text += "<input id='minModel' type='text'>";
										text += "<label for='minModel'>Response Time (Mins)</label>";
                  text += "</div>";
                  text += "<div class='col s12 m2 input-field'>";
                    text += "<input id='hourModel' type='text'>";
										text += "<label for='hourModel'>Response Time (Hrs)</label>";
                  text += "</div>";
                  text += "<div class='col s12 m2 input-field'>";
                    text += "<input id='dayModel' type='text'>";
										text += "<label for='dayModel'>Response Time (Days)</label>";
									text += "</div>";
                  text += "<div class='col s12 m6 input-field'>";
										text += "<input id='rateModel' type='text'>";
										text += "<label for='rateModel'>Success Rate (%)</label>";
									text += "</div>";
								text += '</div>';
								text += '<div class="row"><div class="col s12 addTags"></div></div>';
								leeApoServices();
								text += '<div class="row"><div class="addEquips"></div></div>';
								leeEquipList();
								text += "<div class='row grey lighten-4 border-r'>";
									text += "<div class='editable text-editor'>Unlock instructions for mail response...</div>";
								text += "</div>";
								text += "<div class='row grey lighten-4 border-r'>";
									text += "<div class='col s12 m6 input-field'>";
									text += "<a class='tingle-btn tingle-btn--primary white-text saveOperator'>Save Operator</a>";
								text += "</div>";
							text += "</div>";
							// set content
            	modal.setContent(text);
							modal.open();
						});
            //SHOW SUB collectionShow
            $('.collectionShow').on('click', function(e) {
              $('.collectionShow').children('.upArrowCollection').html('arrow_drop_down');
              $('.collectionShow').removeClass('grey lighten-3');
              $('.collectionShow').next('ul').addClass('su-hidden');
              $(this).children('.upArrowCollection').html('arrow_drop_up');
              $(this).addClass('grey lighten-3');
              $(this).next('ul').removeClass('su-hidden');
            })
            $('.wrapper').mouseover(function(e) {
              var id = $(this).children('.tooltip').attr('value');
              $.get('php/models/carga-operators.php', function (data) {
                var returnvalue = "";
                var equipments = [];
                $.each(data, function(index, content) {
                  if (content.id == id) {
                    equipments = content.eqarr;
                    equipments = equipments.toString();
                    equipments = equipments.split(',');
                    $.each(equipments, function(i, equipment) {
                      $.get('php/models/carga-equipos.php', function (result) {
                        $.each(result, function(name, value) {
                          if (value.id == equipment) {
                            returnvalue = returnvalue + '<br>' + value.name;
                            $('.tooltip--' + id).html(returnvalue);
                          }
                        })
                      })
                    })
                  }
                })
              });
            })
            //CALL EDIT FUNCTION
            $(".editCountry2").on("click", function(e) {
              var id = $(this).attr('val');
              var opt = $(this).attr('opt');
              var idm = $(this).attr('idm');
              editThis(id, opt, idm);
            })
            //CALL DELETE FUNCTION
            $(".deleteOperator").on("click", function(e) {
              var id = $(this).attr('val');
              var type = $(this).attr('type');
              deleteThis(id, type);
            })
	        },
					error: function(xhr, tst, err) {
						console.log(err);
					}
				});
			});
    },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});
}

function deleteThis(i, t){
	var id = i;
	var type = t;
	var text = "<h4 class='center-align'>Confirm your Action</h4>";
	text += "<div class='row center-align grey lighten-3 border-r'>";
	text += "<div class='col s12 input-field'>";
	text += "<a class='tingle-btn tingle-btn--danger white-text center-align deleteButton' id="+id+" type="+type+"><h5><b>DELETE</b></h5></a>";
	text += "</div>";
	// set content
	modal.setContent(text);
	modal.open();
}

function editThis(i, o, im){
	var id = i;
	var opt = o;
	//CARGA BRAND
  $.ajax({
    type: 'post',
    url: 'php/models/carga-editc.php',
    data: JSON.stringify({
    	id : id,
    	opt : opt
    }),
    dataType: "json",
    context: document.body,
    success: function(result) {
			var id;
			var name;
			if (opt == 1) {
				$.each(result, function (key, value) {
					id = value.id;
					name = value.pais;
				});
				var text = "<h4>Edit Country</h4>";
				text += "<div class='row'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<input id='nameModel' type='text' value='" + name + "'>";
				text += "<label for='nameModel'>Country Name</label>";
				text += "</div>";
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text editCountry' opt=1 id=" + id + ">Edit Country</a>";
				text += "</div>";
				text += "</div>";
				// set content
				modal.setContent(text);
				modal.open();

				$('input').focus();

			} else if (opt == 2) {
				var idm;
				$.each(result, function (key, value) {
					id = value.id;
					idm = value.idm;
					name = value.operador;
          min = value.min;
          hour = value.hour;
          day = value.day;
          rate = value.rate;
					desc = value.desc;
					cost = value.cost;
				});
				var text = "<h4>Edit Operator</h4>";
				text += "<div class='row'>";
					text += "<div class='row no-m-bot'>";
						text += "<div class='col s12 m3 input-field p-r'>";
							text += "<input id='nameModel' type='text' value='" + name + "' required>";
              text += "<ul id='searchResults' class='white fiv-p p-a'></ul>";
							text += "<label for='nameModel'>Operator Name</label>";
						text += "</div>";
						text += '<div class="col s12 m9 unlock-row unlock1"><small class="white-text">&nbsp;Unlock only if you want to edit the IDSERVICE and COST, this action deletes previous values</small>';
							text += '<div class="input-field col s12">';
							text += '<select multiple id="serviceList">';
							text += '</select>';
							text += '<label>Add Service</label>';
							text += '</div>';
						text += '</div>';
          text += '</div>';
          text += "<div class='row'>";
          text += "<div class='col s12 m2 input-field'>";
            text += "<input id='minModel' type='text' value='" + min + "'>";
            text += "<label for='minModel'>Response Time (Mins)</label>";
          text += "</div>";
          text += "<div class='col s12 m2 input-field'>";
            text += "<input id='hourModel' type='text' value='" + hour + "'>";
            text += "<label for='hourModel'>Response Time (Hrs)</label>";
          text += "</div>";
          text += "<div class='col s12 m2 input-field'>";
            text += "<input id='dayModel' type='text' value='" + day + "'>";
            text += "<label for='dayModel'>Response Time (Days)</label>";
          text += "</div>";
            text += "<div class='col s12 m6 input-field'>";
							text += "<input id='rateModel' type='text' value='" + rate + "'>";
							text += "<label for='rateModel'>Operator Name</label>";
						text += "</div>";
					text += '</div>';
					text += '<div class="row"><div class="col s12 addTags"></div></div>';
					text += '<div class="row unlock-row unlock2"><small class="white-text">&nbsp;Unlock only if you want to edit the EQUIPMENTS, this action deletes previous values</small><div class="addEquips"></div></div>';
					text += "<div class='row grey lighten-4 border-r'>";
						text += "<div class='editable text-editor'>" + desc + "</div>";
					text += "</div>";
					text += "<div class='col s12 m6 input-field'>";
						text += "<a class='tingle-btn tingle-btn--primary white-text editCountry' opt=2 id=" + id + " idm=" + idm + ">Edit Operator</a>";
					text += "</div>";
				text += "</div>";
				// set content
				modal.setContent(text);
				modal.open();
				$('input').focus();
				$('.unlock-row').on('click',function() {
					if ( $(this).hasClass('unlock1') ) {
						$(this).removeClass('unlock-row');
						$(this).removeClass('unlock1');
						edit1 = 1;
						leeApoServices();
					}
					if ( $(this).hasClass('unlock2') ) {
						$(this).removeClass('unlock-row');
						$(this).removeClass('unlock2');
						edit2 = 1;
						leeEquipList();
					}
				})
			}
			//MANDA A EDITAR Y GUARDAR LOS CAMBIOS
			$(".editCountry").on("click", function(e) {
        event.preventDefault();
        //REVISA QUE LOS TAGS Y PRICES ESTEN COMPLETOS ALSO NAME
        if (!$('#nameModel').val()) {
          alert('Name of Operator not Set');
          return false;
        }
				var opt = $(this).attr('opt');
				var id = $(this).attr('id');
				if (opt == 1) {
					var name = $('#nameModel').val();
					var type = 1;
					var form_data = new FormData();
			    form_data.append("id", id);
			    form_data.append("name", name);
			    form_data.append("type", type);
				} else if (opt == 2) {
					var fatherId = $('#fatherId').val();
					var name = $('#nameModel').val();
          var min = $('#minModel').val();
          var hour = $('#hourModel').val();
          var day = $('#dayModel').val();
          var rate = $('#rateModel').val();
					var type = 2;
					var idser = $('#serviceList').val();
					bundleData = $("input[type=radio]:checked").map(function() {
						return $(this).val();
					}).get().join( "," );
					tagData = $("input[type=radio]:checked").map(function() {
						return $(this).attr('tag');
					}).get().join( "," );
					equipmentArray = $('.selectEquipment').map(function() {
						if ( $(this).hasClass('lc--active') ) {
							return $(this).attr('val');
						}
					}).get().join( "," );
          if ($('.labelInput').lenght) {
            if (!$('.labelInput').val()) {
              alert('At least one service tag it\s missing, please check your tags');
              return false;
            }
          }
          if ($('.editable').html() == 'Unlock instructions for mail response...') {
            alert('Unlock description missing');
            return false;
          }
					var desc = $('.editable').html();
					var cost = $('#costModel').val();
					var form_data = new FormData();
					form_data.append("fatherId", fatherId);
					form_data.append("id", id);
					form_data.append("name", name);
          form_data.append("min", min);
          form_data.append("hour", hour);
          form_data.append("day", day);
          form_data.append("rate", rate);
					form_data.append("type", type);
					form_data.append("edit1", edit1);
					form_data.append("idser", idser);
					form_data.append("idbun", bundleData);
					form_data.append("idtag", tagData);
					form_data.append("edit2", edit2);
					form_data.append("eqArr", equipmentArray);
					form_data.append("desc", desc);
					form_data.append("cost", cost);
				}
				//GUARDAR BRAND
		    $.ajax({
	        type: 'post',
	        url: 'php/models/guarda-editc.php',
	        data: form_data,
			    cache: false,
	        contentType: false,
	        processData: false,
			    dataType: "html",
	        context: document.body,
	        success: function(data) {
	        	M.toast({html: data});
	          modal.close();
	        	menuNav('template/models/countries.php');
	        },
	        error: function(xhr, tst, err) {
	            console.log(err);
	        }
		    });

		  });
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
}

//LEE APO PARA SERVICIOS
function leeApoServices(e) {
  $.ajax({
    type: 'post',
    url: 'apo/carga-servicios.php',
    data: JSON.stringify({
      src: 1
    }),
    dataType: "json",
    context: document.body,
    success: function(data) {
  	serviciosArray = data;
      $.each(data, function(index, value) {
        $("#serviceList").append("<option value=" + value.SERVICEID + " nam='" + value.SERVICENAME + "'>" + value.SERVICENAME + "&nbsp;-&nbsp;credits:&nbsp;" + value.CREDIT + "</option>");
      });
      $('select').formSelect();
      $("#serviceList").change(function(){
    	var id = $(this).val();
    	leeBundle(id);
		});
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
}

function leeBundle(ide) {
	var id = ide;
  $.ajax({
    method: 'post',
    url: '../php/desbloquea/lee-bundle.php',
    dataType: "json",
    context: document.body,
    success: function(data) {
    	$(".addTags").html('');
      $.each(id, function(i, id) {
      	var resultName = leeGroupService(id)
      	$(".addTags").append('<p class="pTags' + i + ' light-blue darken-1 fiv-m fiv-p border-r-10"></p>');
      	$('.pTags' + i).append("<p class='p-r'><label><input name='group" + i + "' type='radio' checked><span class='white-text'>" + resultName + "</span></label><i class='clickLabel fas fa-money-bill-alt white-text handed p-a icon-right-price'><input type='text' pattern='[0-9]' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class='labelInput label-price su-hidden' placeholder='price' val='group" + i + "'></i><i class='clickLabel fas fa-tag white-text handed p-a icon-right-tag'><input type='text' class='labelInput label-tag su-hidden' placeholder='tag' val='group" + i + "'></i></p>");
      })
      bundleData = $("label input[type=radio]:checked").map(function() {
				return $(this).val();
			}).get().join( "," );
      //APARECE LOS INPUTS DE TAG Y PRECIO
      $('.clickLabel').on('click',function(){
      	$('.labelInput').addClass('su-hidden');
      	$(this).find('.labelInput').removeClass('su-hidden');
      });
      //ASIGNA EL VALOR DE LOS TAGS A LOS VALORES DEL RADIO BUTTON
			$('.label-price').on('keyup', function() {
				var precio = $(this).val();
				var group = $(this).attr('val');
				$("input[name=" + group + "]").val(precio);
			});
			$('.label-tag').on('keyup', function() {
				var tag = $(this).val();
				var group = $(this).attr('val');
				$("input[name=" + group + "]").attr('tag',tag);
			});
      //REGRESA EL NOMBRE DEL SERVICIO DE ACUERDO AL ID
      function leeGroupService(e) {
      	var id = e;
      	var result;
  	    $.each(serviciosArray, function(index, value) {
  	    	if (value.SERVICEID == id){
  	        	result = value.SERVICENAME;
  	        }
  	    });
		    return result;
      }
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
}

//LEE EQUIP LIST
function leeEquipList(e) {
  $.ajax({
    type: 'post',
    url: 'php/models/carga-equipos.php',
    dataType: "json",
    context: document.body,
    success: function(data) {
      $(".addEquips").html("<ul class='collection equipoCollection'></ul>");
    	$(".equipoCollection").append('<input type="text" class="equipoSearch" placeholder="search for equip...">');
    	$(".equipoCollection").append('<i class="material-icons handed blue-text toggleCheck">check</i><i class="material-icons red-text handed cancelCheck">cancel</i>');
      $.each(data, function(index, value) {
      	$(".equipoCollection").append("<li class='collection-item avatar handed li-collection-adm selectEquipment lc-" + value.id + "' val=" + value.id + "><img class='responsive-img circle' src='../img/" + value.img + "'><span>" + value.name + "</span><li>");
      });
      //SELECCIONA EQUIPOS DE LA LISTA PARA AGREGARLOS A UN ARRAY Y LLEVARTELOS A JSON
      $('.selectEquipment').on('click',function() {
      	$(this).toggleClass('amber white-text');
      	$(this).toggleClass('lc--active');
      });
      $('.toggleCheck').on('click',function() {
			  $(".equipoCollection li").each(function() {
			    if ( !$(this).hasClass("su-hidden") ) {
            $(this).addClass("amber white-text")
            $(this).addClass('lc--active');
          }
		    });
      });
      $('.cancelCheck').on('click',function() {
  	    $('.selectEquipment').removeClass("amber white-text")
  	    $('.selectEquipment').removeClass('lc--active');
      });
			//OMNI Buscador
			$(".equipoSearch").on("keyup", function(e) {
		    var input, filter, parent, contenedor, p, i;
		    input = $(this);
		    filter = input.val().toUpperCase();
		    parent = $('.equipoCollection > .selectEquipment');
		    contenedor = $('.equipoCollection > .selectEquipment');
		    for (i = 0; i < contenedor.length; i++) {
	        p = contenedor[i].getElementsByTagName("span")[0];
	        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
	            parent[i].classList.remove('su-hidden');
	        } else {
	            parent[i].classList.add('su-hidden');
	        }
		    }
			});
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
}
function hashCode(str) { // java String#hashCode
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
     hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}
function intToRGB(i){
  var c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();
  return "00000".substring(0, 6 - c.length) + c;
}
