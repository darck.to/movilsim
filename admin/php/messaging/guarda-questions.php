<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	include('../../func/abre_conexion.php');

	//RECIBIMOS LA INFORMACION
	$question = mysqli_real_escape_string($mysqli, $_POST['question']);
    $mail = mysqli_real_escape_string($mysqli, $_POST['mail']);
    $message = mysqli_real_escape_string($mysqli, $_POST['message']);

	$jsonString = file_get_contents('../../assets/questions/question'.$message.'.json');
	$data = json_decode($jsonString, true);

	$data[0]['status'] = 1;
	$data[0]['answer'] = $question;

	//LO VOLVEMOS A GUARDAR
	$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
	file_put_contents('../../assets/questions/question'.$message.'.json', $newJsonString);

    include('../../func/cierra_conexion.php');

    //ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Respuesta a tu pregunta con los expertos de movilSim";
	$mensaje = "<p><h2>Tu pregunta ha sido respondida:</h2></p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Tu respuesta: \"".$question."\"</p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
				<div style='display: block; padding: 10px 0;'></div>
				<p>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de Lizipar para poder auxiliarte: <a href='mailto:contact@movilsim.com' style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent; color: #4b4b4b; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$mail);

?>
