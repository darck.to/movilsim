<?php
	
	header('Content-type: application/json');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/bundle/bundle.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$resultados[] = array("nombre"=>$content['nombre'], "duracion"=>$content['duracion'], "discount"=>$content['discount']);

			}

		}

	}

	print json_encode($resultados);

?>