<?php
 	error_reporting(0);

	header('Content-type: application/json');
	include_once('../../func/functions.php');
  include_once('../../func/abre_conexion.php');

  $sql = $mysqli->query("SELECT folio, fecha, mes, random, imei, email, nombre, marca, equipo, pais, operator, precio, coin, serviceid, servicename, comentario, estatus, serviceanswer, serviceinfo, mporder, code FROM ord_movi");
	if ($sql->num_rows > 0) {
    $resultados = array();
    while($row = $sql->fetch_assoc()) {
      $resultados[] = array($row['folio'], $row['fecha'], $row['random'], $row['imei'], $row['email'], $row['nombre'], $row['marca'], $row['equipo'], $row['precio'], $row['estatus'], $row['code'], leePais($row['pais']), leeOperador($row['operator']), $row['serviceinfo'], revisaOrderMP($row['mporder']));
    }
  }

  //$resultados = array();

	//NOMBRE DE ARCHIVO
	//$fileList = glob('../../assets/*order*.json');

	//RECORREMOS LOS ARCHIVOS
	//foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	//	if (file_exists($filename)) {

	//		$filename = file_get_contents($filename);
	//		$json = json_decode($filename, true);

	//		foreach ($json as $content) {

	//			$resultados[] = array($content['folio'], $content['fecha'], $content['random'], $content['imei'], $content['email'], $content['nombre'], $content['marca'], $content['equipo'], $content['precio'], $content['estatus'], $content['code'], leePais($content['pais']), leeOperador($content['operator']), $content['serviceinfo'], revisaOrderMP($content['mporder']));

	//		}

	//	}

	//}

	print json_encode($resultados);

  include('../../func/cierra_conexion.php');
?>
