<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	include('../../func/abre_conexion.php');

	//RECIBIMOS LA INFORMACION
	$number = mysqli_real_escape_string($mysqli, $_POST['number']);
  $paquete = mysqli_real_escape_string($mysqli, $_POST['paquete']);
  $hour = mysqli_real_escape_string($mysqli, $_POST['hour']);
  $precio = mysqli_real_escape_string($mysqli, $_POST['precio']);

  //INDEX FORMAT
  $number = $number - 1;

	$filename = file_get_contents('../../assets/bundle/bundle.json');
	$data = json_decode($filename, true);

	$data[$number]['nombre'] = $paquete;
	$data[$number]['duracion'] = (int)$hour;
	$data[$number]['discount'] = $precio;

	//LO VOLVEMOS A GUARDAR
	$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
	if ( file_put_contents('../../assets/bundle/bundle.json', $newJsonString) ) {
		echo "Package saved";
	} else {
		echo "Error, contact support";
	}

    include('../../func/cierra_conexion.php');

?>
