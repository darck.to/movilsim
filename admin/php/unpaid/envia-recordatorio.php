<?php

  include('../../func/abre_conexion.php');

  $folio = mysqli_real_escape_string($mysqli, $_POST['folio']);
	$order = mysqli_real_escape_string($mysqli, $_POST['order']);

  $sql = $mysqli->query("SELECT email, nombre, marca, equipo FROM ord_movi WHERE folio = $folio AND random = '".$order."'");
	if ($sql->num_rows > 0) {
    $cabecera = array();
    $row = $sql->fetch_assoc();
    $nombre = $row['nombre'];
    $email = $row['email'];
    $marca = $row['marca'];
    $equipo = $row['equipo'];
  }

	$filename = file_get_contents('../../assets/'.$folio.'_order_'.$order.'.json');
	$data = json_decode($filename, true);

	//ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Regresa con nosotros";
	$mensaje = "<p><h2>".$nombre." Guarda tu n&uacute;mero de orden:</h2></p>
				<p><h3><b>".$random."</b></h3></p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Tu orden para el desbloqueo de tu equipo ".$marca." ".$equipo." se encuentra esperandote</p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
				<div style='display: block; padding: 10px 0;'></div>
				<p>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de movilSim para poder auxiliarte: <a href='mailto:ordenes@movilsim.com' style='color: #fff; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'><b>AQUI</b></a></p>
  	";
  	include_once('../../func/mailFunctions.php');
  	enviaCorreo($asunto,$mensaje,$email);

    include('../../func/cierra_conexion.php');

?>
