<?php

	header('Content-type: application/json');
	//NOMBRE DE ARCHIVO
	$filename = '../../../php/desbloquea/files/operadores.json';

	$resultados = array();

   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists($filename)) {

		$filename = file_get_contents($filename);
		$json = json_decode($filename, true);

		foreach ($json as $content) {

			$resultados[] = array("operador"=>$content['operador']);

		}

	} else {

	}

	print json_encode($resultados, JSON_PRETTY_PRINT);

?>
