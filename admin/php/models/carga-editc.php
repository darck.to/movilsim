<?php

	header('Content-type: application/json');
	$_POST = json_decode(file_get_contents('php://input'), true);
	//RECIBIMOS LOS POST
	$id = $_POST['id'];
	$opt = $_POST['opt'];

	if ($opt == 1) {

		//NOMBRE DE ARCHIVO
		$filename = '../../../php/desbloquea/files/paises.json';

		$resultados = array();

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				if ($content['id'] == $id) {
					$resultados[] = array("id"=>$content['id'], "pais"=>$content['pais']);
				}


			}

		} else {

		}

	} elseif ($opt == 2) {

		//NOMBRE DE ARCHIVO
		$filename = '../../../php/desbloquea/files/operadores.json';

		$resultados = array();

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				if ($content['id'] == $id) {
					$resultados[] = array("id"=>$content['id'], "idm"=>$content['id_m'], "operador"=>$content['operador'], "serviceid"=>$content['serviceid'], "min"=>$content['min'], "hour"=>$content['hour'], "day"=>$content['day'], "rate"=>$content['rate'], "servicebu"=>$content['servicebu'], "desc"=>$content['desc'], "cost"=>$content['cost']);
				}


			}

		} else {

		}


	}

	print json_encode($resultados);

?>
