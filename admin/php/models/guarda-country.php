<?php

	include_once('../../func/functions.php');

	//RECIBIMOS DATOS POST
    if ($_POST["type"] == 0) {
    	$filename = '../../../php/desbloquea/files/paises.json';
		$lastId = readLastId($filename);
		$lastId++;
    	$name = $_POST["name"];
		$change = array("id"=>$lastId,"pais"=>$name,"portada"=>0);
    } else {
		$filename = '../../../php/desbloquea/files/operadores.json';
		$lastId = readLastId($filename);
		$lastId++;
    	$fatherId = $_POST["fatherId"];
		$name = $_POST["name"];
		$min = $_POST["min"];
		$hour = $_POST["hour"];
		$day = $_POST["day"];
		$rate = $_POST["rate"];
		$idser = $_POST["idser"];
		$idbun = $_POST["idbun"];
		$idtag = $_POST["idtag"];
		$eqArr = $_POST["eqArr"];
		$desc =	$_POST["desc"];
		$cost =	$_POST["cost"];
    	$change = array("id"=>$lastId,"id_m"=>$fatherId,"operador"=>$name,"min"=>$min,"hour"=>$hour,"day"=>$day,"rate"=>$rate,"serviceid"=>$idser,"servicebu"=>$idbun,"servicetag"=>$idtag,"eqarr"=>$eqArr,"desc"=>$desc,"cost"=>$cost);
    }

	$file = file_get_contents($filename);
	$json = json_decode($file, true);

	$json[] = $change;

	$filejson = fopen($filename, 'w') or die ('No se guardo el archivo');
	if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
		echo $name." Saved";
	} else {
		echo "Error";
	}
	fclose($filejson);

?>
