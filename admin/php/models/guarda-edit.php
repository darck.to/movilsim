<?php

	include_once('../../func/functions.php');

	//RECIBIMOS DATOS POST
    if ($_POST["type"] == 1) {
    	$filename = "../../../php/home/files/marcas.json";
    	$id = $_POST["id"];
    	$name = $_POST["name"];
    	if (isset($_FILES["file1"])) {
			$img = stripslashes(fileImag($_FILES["file1"], $id, ''));
    	} else {
    		$img = "logos/logo-".$id;
    	}
    } elseif ($_POST["type"] == 2) {
		$filename = "../../../php/desbloquea/files/marcas.json";
		$id = $_POST["id"];
		$name = $_POST["name"];
		if (isset($_FILES["file1"])) {
			$img = stripslashes(fileImag($_FILES["file1"], $id, 'phones/'));
		} else {
			$img = "logos/phones/logo-".$id;
		}
    	$desc =	$_POST["desc"];
    }

	if (file_exists($filename)) {
		
		$file = file_get_contents($filename);
		$json = json_decode($file, true);

		foreach ($json as &$content) {
			if ($content['id'] == $id && $_POST["type"] == 1) {
				$content['name'] = $name;
				$content['img'] = stripslashes($img);
			} elseif ($content['id'] == $id && $_POST["type"] == 2) {
				$content['name'] = $name;
				$content['img'] = stripslashes($img);
				$content['desc'] = $desc;
			}
		}

		$filejson = fopen($filename, 'w') or die ('No file found, contact support');
		if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
			echo $name." Saved";
		} else {
			echo "Error, contact support";
		}
		fclose($filejson);
		
	} else {

	}
	
?>