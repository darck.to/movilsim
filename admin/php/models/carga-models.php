<?php
	
	header('Content-type: application/json');
	//NOMBRE DE ARCHIVO
	$filename = '../../../php/home/files/marcas.json';

	$resultados = array();

   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists($filename)) {
		
		$filename = file_get_contents($filename);
		$json = json_decode($filename, true);

		foreach ($json as $content) {

			$resultados[] = array("id"=>$content['id'],"name"=>$content['name'],"img"=>$content['img'],"portada"=>$content['portada']);

		}

	} else {

	}

	print json_encode($resultados, JSON_PRETTY_PRINT);

?>