<?php

	header('Content-type: application/json');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*order*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$resultados[] = array('Folio'=>$content['folio'], 'Date'=>$content['fecha'], 'Order'=>$content['random'], 'IMEI'=>$content['imei'], 'Email'=>$content['email'], 'Name'=>$content['nombre'], 'Brand'=>$content['marca'], 'Equipment'=>$content['equipo']);

			}

		}

	}

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*check*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$resultados[] = array('Folio'=>$content['folio'], 'Date'=>$content['fecha'], 'Order'=>$content['random'], 'IMEI'=>$content['imei'], 'Email'=>$content['email'], 'Name'=>$content['nombre']);

			}

		}

	}

	print json_encode($resultados);

?>
