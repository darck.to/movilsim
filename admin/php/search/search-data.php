<?php

	header('Content-type: application/json');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$filename = '../../assets/'.$_POST['find'].'.json';

   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists($filename)) {
		
		$filename = file_get_contents($filename);
		$json = json_decode($filename, true);

		foreach ($json as $content) {

			if (isset($content['marca'])) {
				$resultados[] = array('Folio'=>$content['folio'], 'Date'=>$content['fecha'], 'Order'=>$content['random'], 'IMEI'=>$content['imei'], 'Email'=>$content['email'], 'Name'=>$content['nombre'], 'Brand'=>$content['marca'], 'Equipment'=>$content['equipo'], 'Cost'=>$content['precio'], 'Status'=>$content['estatus'], 'Code'=>$content['code']);
			} else {
				$resultados[] = array('Folio'=>$content['folio'], 'Date'=>$content['fecha'], 'Order'=>$content['random'], 'IMEI'=>$content['imei'], 'Email'=>$content['email'], 'Name'=>$content['nombre'], 'Brand'=>'', 'Equipment'=>'', 'Cost'=>$content['precio'], 'Status'=>$content['estatus'], 'Code'=>$content['code']);
			}

		}

	}

	print json_encode($resultados);

?>