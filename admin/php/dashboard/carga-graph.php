<?php

	header('Content-type: application/json');
	include_once('../../func/abre_conexion.php');

	$imei = array(
					array('mes'=> 'JAN', 'orders' => 0),
					array('mes'=> 'FEB', 'orders' => 0),
					array('mes'=> 'MAR', 'orders' => 0),
					array('mes'=> 'APR', 'orders' => 0),
					array('mes'=> 'MAY', 'orders' => 0),
					array('mes'=> 'JUN', 'orders' => 0),
					array('mes'=> 'JUL', 'orders' => 0),
					array('mes'=> 'AUG', 'orders' => 0),
					array('mes'=> 'SEP', 'orders' => 0),
					array('mes'=> 'OCT', 'orders' => 0),
					array('mes'=> 'NOV', 'orders' => 0),
					array('mes'=> 'DIC', 'orders' => 0)
				);

	$sql = $mysqli->query("SELECT folio, fecha, mes, random, imei, email, nombre, marca, equipo, pais, operator, precio, coin, serviceid, servicename, comentario, estatus, serviceanswer, serviceinfo, mporder, code FROM ord_movi WHERE estatus <> 1");
	if ($sql->num_rows > 0) {
    //$resultados = array();
    while($row = $sql->fetch_assoc()) {
			//CONTAMOS MESES Y ORDENES
			$mes = $row['mes'];
			if ($mes == "01") {
				$imei[0]['orders'] += 1;
			}
			if ($mes == "02") {
				$imei[1]['orders'] += 1;
			}
			if ($mes == "03") {
				$imei[2]['orders'] += 1;
			}
			if ($mes == "04") {
				$imei[3]['orders'] += 1;
			}
			if ($mes == "05") {
				$imei[4]['orders'] += 1;
			}
			if ($mes == "06") {
				$imei[5]['orders'] += 1;
			}
			if ($mes == "07") {
				$imei[6]['orders'] += 1;
			}
			if ($mes == "08") {
				$imei[7]['orders'] += 1;
			}
			if ($mes == "09") {
				$imei[8]['orders'] += 1;
			}
			if ($mes == "10") {
				$imei[9]['orders'] += 1;
			}
			if ($mes == "11") {
				$imei[10]['orders'] += 1;
			}
			if ($mes == "12") {
				$imei[11]['orders'] += 1;
			}
    }
  }

	//NOMBRE DE ARCHIVO
	//$fileList = glob('../../assets/*order*.json');

	date_default_timezone_set("America/Mexico_City");

	//RECORREMOS LOS ARCHIVOS
	//foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	//	if (file_exists($filename)) {

	//		$filename = file_get_contents($filename);
	//		$json = json_decode($filename, true);;

	//		foreach ($json as $content) {

				//CONTAMOS MESES Y ORDENES
	//			$mes = $row['mes'];

		//		if ($mes == "01") {
		//			$imei[0]['orders'] += 1;
		//		}
		//		if ($mes == "02") {
		//			$imei[1]['orders'] += 1;
		//		}
		//		if ($mes == "03") {
		//			$imei[2]['orders'] += 1;
		//		}
		//		if ($mes == "04") {
			//		$imei[3]['orders'] += 1;
		//		}
		//		if ($mes == "05") {
		//			$imei[4]['orders'] += 1;
	//			}
		//		if ($mes == "06") {
		//		$imei[5]['orders'] += 1;
	//			}
	//			if ($mes == "07") {
	//				$imei[6]['orders'] += 1;
		//		}
		//		if ($mes == "08") {
		//			$imei[7]['orders'] += 1;
		//		}
	//			if ($mes == "09") {
		//			$imei[8]['orders'] += 1;
		//		}
	//			if ($mes == "10") {
		//			$imei[9]['orders'] += 1;
		//		}
		//		if ($mes == "11") {
		//			$imei[10]['orders'] += 1;
		//		}
		//		if ($mes == "12") {
	//				$imei[11]['orders'] += 1;
	//			}

		//	}

	//	}

//	}

	print json_encode($imei);

?>
