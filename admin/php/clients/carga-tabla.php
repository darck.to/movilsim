<?php

	header('Content-type: application/json');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*_*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$key = $content['email'];

				if (!array_key_exists($key, $resultados)) {
					$resultados[$key] = array(
						'email' => $content['email'],
						'nombre' => $content['nombre'],
            'precio' => $content['precio'],
            'orders' => 1, 
					);
				} else {
					$resultados[$key]['precio'] = $resultados[$key]['precio'] + $content['precio'];
					$resultados[$key]['orders'] += 1;
				}

			}

		}

	}

	$resultados = array_map('array_values', $resultados);
	$resultados = array_values($resultados);

	print json_encode($resultados);

?>
