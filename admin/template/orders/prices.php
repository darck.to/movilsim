<div class="row">

	<div class="row">

		<div class="col s12">
			<span class="admin-title"><?php echo _("Orders"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>
		</div>

	</div>

	<div class="col s12 m10 offset-m1">

		<div class="row">
			<div class="col s12 l4">
				<div class="card">
					<div class="card-content blue darken-3">
						<span class="card-title white-text">Descuento</span>
					</div>
					<div class="card-content">
						<div class="row">
							<div class="input-field col s12">
								<input id="paq1" type="text" class="validate">
								<label for="paq1">Name</label>
							</div>
							<div class="input-field col s12">
								<input id="hour1" type="number" class="validate" pattern="[0-9]" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
								<label for="hour1">Timer (minutes)</label>
							</div>
							<div class="input-field col s12">
								<input id="pri1" type="number" class="validate"  min="0" value="0" step=".01">
								<label for="pri1">Price (Percentual 20 = +20%)</label>
							</div>
							<button class="btn waves-effect waves-light right sendBundle" type="submit" val="1">Submit
								<i class="material-icons right">send</i>
							</button>
						</div>
					</div>
				</div>
			</div>

	</div>

</div>

<script type="text/javascript" src="scripts/orders/prices.js"></script>
