<div class="row">

	<div class="row">
			
		<div class="col s12">
			<span class="admin-title"><?php echo _("Orders Calendar"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>
		</div>

	</div>
	
	<div class="col s12 m8">
	
		<div class="card z-depth-0">
			<div class="card-content">
				<div class="row">
					<div id='calendar' class="col s12"></div>
				</div>
			</div>
		</div>
		
	</div>

	<div class="col s12 m4">
		
		<div class="card">
			<div class="card-content">
				<h5><i class="material-icons right indigo-text text-lighten-2">dns</i>Order summary</h5>
				<div class="divider orange darken-1"></div>
				<div class="row">
					<div id="calendarSummary" class="col s12"></div>
			</div>
		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/orders/orders-date.js"></script>