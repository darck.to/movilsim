<?php

	session_start();
	if (isset($_SESSION['usuario_nombre'])) {

?>


<div class="row">
	
	<div class="row">
		
		<div class="col s12">
			<span class="admin-title"><?php echo _("Dashboard"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>
		</div>

	</div>

	<div class="col s12 m6 l3">
		
		<div class="card z-depth-1 light-blue darken-2 border-r">
			<div class="card-content">
				<span class="card-title white-text">UNLOCK ORDERS</span>
			</div>
			<div class="card-content white border-r2-b ">
				<div class="row">
					<div class="col s6 center-align grey-text text-darken-2">
						<h4 id="imeiReceived" class="no-m">&nbsp;</h4>
						<p>Received</p>
					</div>
					<div class="col s6 center-align grey-text text-darken-2">
						<h4 id="imeiCompleted" class="no-m">&nbsp;</h4>
						<p>Completed</p>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col s12 m6 l3">
		
		<div class="card z-depth-1 pink darken-2 border-r">
			<div class="card-content">
				<span class="card-title white-text">CLIENTS</span>
			</div>
			<div class="card-content white border-r2-b ">
				<div class="row">
					<div class="col s6 center-align grey-text text-darken-2">
						<h4 id="clientsRegistered" class="no-m">&nbsp;</h4>
						<p>Total</p>
					</div>
					<div class="col s6 center-align grey-text text-darken-2">
						<h4 id="clientsPaid" class="no-m">&nbsp;</h4>
						<p>Unique</p>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col s12 m6 l3">
		
		<div class="card z-depth-1 orange darken-2 border-r">
			<div class="card-content">
				<span class="card-title white-text">COMMENTS</span>
			</div>
			<div class="card-content white border-r2-b ">
				<div class="row">
					<div class="col s12 center-align grey-text text-darken-2">
						<h4 id="commentsTotal" class="no-m">&nbsp;</h4>
						<p>Received</p>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col s12 m6 l3">
		
		<div class="card z-depth-1 light-blue darken-2 border-r">
			<div class="card-content">
				<span class="card-title white-text">IMEI CHECK ORDERS</span>
			</div>
			<div class="card-content white border-r2-b ">
				<div class="row">
					<div class="col s6 center-align grey-text text-darken-2">
						<h4 id="checkReceived" class="no-m">&nbsp;</h4>
						<p>Received</p>
					</div>
					<div class="col s6 center-align grey-text text-darken-2">
						<h4 id="checkCompleted" class="no-m">&nbsp;</h4>
						<p>Completed</p>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<!--Graph-->
<div class="row">

	<div class="col s12 m10 offset-m1 l8 offset-l2">

		<canvas id="dashboardLines"></canvas>

	</div>
	
</div>

<script type="text/javascript" src="scripts/dashboard/dashboard.js"></script>

<?php

	} else {

	}

?>