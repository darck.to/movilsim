<div class="row">
	
	<div class="row">
		
		<div class="col s12">
			<span class="admin-title"><?php echo _("Clients"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>
		</div>

	</div>

	<div class="col s12 m10 offset-m1">
		
		<table id="table" class="display" style="width:100%">
	        <thead>
	            <tr>
	                <th>Email</th>
	                <th>Customer</th>
	                <th>Gain</th>
	                <th>Orders</th>
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	                <th>Email</th>
	                <th>Customer</th>
	                <th>Gain</th>
	                <th>Orders</th>
	            </tr>
	        </tfoot>
	    </table>

	</div>

</div>

<script type="text/javascript" src="scripts/clients/clients.js"></script>