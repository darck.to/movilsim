<div class="row">

	<div class="row">

		<div class="col s12">
			<span class="admin-title"><?php echo _("Countries"); ?></i></span><span class="admin-bullets">&nbsp;<i id="firstBullet" class="blue title-bullets"></i><i id="secBullet" class="amber title-bullets"></i></span>
		</div>

	</div>

	<div class="col s12 m10 offset-m1">

		<div class="col s12 m6">
			<ul id="countryCollection" class="collection height-750-a"></ul>		
		</div>

		<div class="col s12 m6">
			<ul id="operatorCollection" class="collection height-750-a"></ul>
		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/models/countries.js"></script>
