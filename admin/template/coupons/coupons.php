<div class="row">

	<div class="row">

		<div class="col s12">
			<span class="admin-title"><?php echo _("Coupons"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets handed addCoupon"><b>ADD Coupon</b></i><i class="amber title-bullets"></i></span>
		</div>

	</div>

	<div class="col s12 m10 offset-m1">

		<table id="table" class="display" style="width:100%">
	        <thead>
	            <tr>
	                <th>Folio</th>
	                <th>Date</th>
	                <th>Coupon Code</th>
	                <th>Discount</th>
                  <th>Client</th>
	                <th>Vigency</th>
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	                <th>Folio</th>
	                <th>Date</th>
	                <th>Coupon Code</th>
	                <th>Discount</th>
                  <th>Client</th>
	                <th>Vigency</th>
	            </tr>
	        </tfoot>
	    </table>

	</div>

</div>

<script type="text/javascript" src="scripts/coupons/coupons.js"></script>
