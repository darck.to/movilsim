<div class="row">
	
	<div class="row">
		
		<div class="col s12">
			<span class="admin-title"><?php echo _("Contact Form"); ?></i></span><span class="admin-bullets">&nbsp;<i id="firstBullet" class="blue title-bullets"></i><i id="secBullet" class="amber title-bullets"></i></span>
		</div>

	</div>

	<div class="col s12 m10 offset-m1">
		
		<table id="table" class="display" style="width:100%">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>Mail</th>
	                <th>Date</th>
	                <th>Status</th>
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	                <th>Name</th>
	                <th>Mail</th>
	                <th>Date</th>
	                <th>Status</th>
	            </tr>
	        </tfoot>
	    </table>

	</div>

</div>

<script type="text/javascript" src="scripts/messaging/messaging-contact.js"></script>