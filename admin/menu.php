<div class="navbar-fixed">

	<nav>

		<div class="nav-wrapper grey lighten-3">

			<a href="." class="brand-logo">
				<h5>Panel</h5>
			</a>

			<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons grey-text text-darken-3">menu</i></a>

			<ul id="nav-mobile" class="right">
<?php

	session_start();
	if (isset($_SESSION['usuario_nombre'])) {
?>
				<li><a><i class="material-icons grey-text text-darken-3" onclick="apiFunction()">apps</i></a><div id="apiBox" class="z-depth-1 hide"></div></li>
<?php
  }
?>
        <li><a href="."><i class="material-icons grey-text text-darken-3">account_box</i></a></li>
				<li><a class="handed sidenav-close" onclick="initFunction()"><i class="material-icons grey-text text-darken-3">vpn_key</i></a></li>

			</ul>


		</div>

	</nav>

</div>

<?php
	if (isset($_SESSION['usuario_nombre'])) {
?>

<ul id="slide-out" class="sidenav sidenav-fixed">

	<div class="row">
		<div class="col s12">
			<a href="."><li><img class="responsive-img" src="img/logo.png"></li></a>
		</div>
	</div>

	<div class="search-wrapper">
		<input id="search" class="omniSearch" placeholder=<?php echo _("Buscar"); ?> autocomplete="off">
		<i class="material-icons grey-text text-lighten-1">search</i>
	</div>
	<li class="no-padding">
		<ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header sidenav-close" onclick="menuNav('template/dashboard/dashboard.php')">Dashboard<i class="material-icons">dashboard</i></a>
			</li>
		</ul>
	</li>
	<li class="no-padding">
	    <ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header sidenav-close" onclick="menuNav('template/orders/orders.php')">Unlock Orders<i class="material-icons">dns</i></a>
				<div class="collapsible-body">
				  <ul>
				    <li><a class="handed" onclick="menuNav('template/orders/orders-date.php')"><i class="material-icons grey-text text-lighten-2">today</i>Orders by date</a></li>
				    <li><a class="handed" onclick="menuNav('template/orders/orders-daily.php')"><i class="material-icons grey-text text-lighten-2">view_day</i>Orders daily</a></li>
				  </ul>
				</div>
			</li>
	    </ul>
	</li>
	<li class="no-padding">
	    <ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header sidenav-close" onclick="menuNav('template/requests/requests.php')">IMEI Request<i class="material-icons">dns</i></a>
				<div class="collapsible-body">
				  <ul>
				    <li><a class="handed" onclick="menuNav('template/requests/requests-date.php')"><i class="material-icons grey-text text-lighten-2">today</i>Orders by date</a></li>
				    <li><a class="handed" onclick="menuNav('template/requests/requests-daily.php')"><i class="material-icons grey-text text-lighten-2">view_day</i>Orders daily</a></li>
				  </ul>
				</div>
			</li>
	    </ul>
	</li>
	<li class="no-padding">
	    <ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header sidenav-close" onclick="menuNav('template/clients/clients.php')">Clients<i class="material-icons">people</i></a>
			</li>
	    </ul>
	</li>
	<li class="no-padding">
		<a class="collapsible-header sidenav-close" onclick="menuNav('template/unpaid/unpaid.php')">Unpaid Clients<i class="material-icons">thumb_down</i></a>
	</li>
	<li class="no-padding">
	    <ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header" >Config<i class="material-icons">settings</i></a>
				<div class="collapsible-body">
				  <ul>
				    <li><a class="handed sidenav-close" onclick="menuNav('template/models/models.php')"><i class="material-icons grey-text text-lighten-2">phone_android</i>Models</a></li>
				    <li><a class="handed sidenav-close" onclick="menuNav('template/models/countries.php')"><i class="material-icons grey-text text-lighten-2">flag</i>Countrys</a></li>
						<li><a class="handed sidenav-close" onclick="menuNav('template/orders/orders-service.php')"><i class="material-icons grey-text text-lighten-2">playlist_add_check</i>IMEI Check Service</a></li>
						<li><a class="handed" onclick="menuNav('template/coupons/coupons.php')"><i class="material-icons grey-text text-lighten-2">local_play</i>Coupons</a></li>
				    <li><a class="handed" onclick="menuNav('template/orders/prices.php')"><i class="material-icons grey-text text-lighten-2">local_offer</i>Discounts</a></li>
				  </ul>
				</div>
			</li>
	    </ul>
	</li>
	<li class="no-padding">
	    <ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header" >Messenger Service<i class="material-icons">dns</i></a>
				<div class="collapsible-body">
				  <ul>
				    <li><a class="handed sidenav-close" onclick="menuNav('template/messaging/messaging-contact.php')"><i class="material-icons grey-text text-lighten-2">help</i>Contact</a></li>
				    <li><a class="handed sidenav-close" onclick="menuNav('template/messaging/messaging-questions.php')"><i class="material-icons grey-text text-lighten-2">help_outline</i>Questions to Experts</a></li>
				    <li><a class="handed sidenav-close" href="https://unepic.com.mx/apps/ultimate_support_chat/index.php" target="_blank"><i class="material-icons grey-text text-lighten-2">chat</i>Help Desk</a></li>
				  </ul>
				</div>
			</li>
	    </ul>
	</li>
	<li class="no-padding">
		<a class="collapsible-header sidenav-close" onclick="initFunction()">Logout<i class="material-icons">vpn_key</i></a>
	</li>

</ul>

<?php

	} else {

	}

?>
