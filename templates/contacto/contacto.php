<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _CONTT1; ?><p><?php echo _CONTT11; ?></p>
			</h1>
		</div>
	</div>
</section>

<!--CONT&Aacute;CTANOS-->
<div class="section pa-no-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _CONTT2; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _CONTT2S1; ?></h5>

		<!--BOX IMEI-->
		<div class="columns">
			<div class="column is-8 is-offset-2 pa-one">
				<div class="box is-radiusless is-shadowless background-p-1">
					<div class="columns">
						<div class="column">
							<div class="content pa-one background-p-3 radius-5" id="messageForm">

								<div class="columns is-multiline">
									<div class="column is-8">
										<div class="content">
											<input class="input input-format" type="text" id="name" placeholder="<?php echo _CONTFI1;?>">
										</div>
									</div>
									<div class="column is-4">
										<div class="content">
											<input class="input input-format" type="text" id="mail" placeholder="<?php echo _CONTFI2;?>">
											<small class="is-hidden emailCheck"><?php echo _CONTFI2R1; ?></small>
										</div>
									</div>
									<div class="column is-12">
										<div class="field">
											<div class="control">
												<textarea class="textarea is-small input-format" id="messa" placeholder="<?php echo _CONTFI3;?>"></textarea>
											</div>
										</div>
									</div>
									<div class="column has-text-centered">
										<a class="button is-medium input-format enviarContacto"><?php echo _CONTBTN1;?></a>
									</div>
								</div>

							</div>
							<h5 id="messageMsg" class="has-text-centered has-text-white"></h5>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
