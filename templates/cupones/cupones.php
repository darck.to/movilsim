<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _COUPT1; ?><p><?php echo _COUPT11; ?></p>
			</h1>
		</div>
	</div>
</section>

<!--INTRODUCE TU C&Oacute;DIGO CON DESCUENTO-->
<div class="section pa-no-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _COUPT2; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _COUPT2S1; ?></h5>

		<!--BOX IMEI-->
		<div id="orderDiv" class="box is-radiusless is-shadowless background-p-1">
			<div class="columns">
				<div class="column pa-one">
					<div class="content pa-one background-p-3 radius-5">

						<div class="columns">
							<div class="column is-half is-offset-one-quarter">
								<div class="content">
									<input class="input input-format" id="cuponForm" type="text" placeholder="<?php echo _COUPFI1;?>">
								</div>
							</div>
						</div>

						<div class="column is-half is-offset-one-quarter has-background-white">
							<div class="tile is-ancestor">
								<div class="tile is-parent is-4 has-text-centered">
									<div class="content is-relative width-one">
										<h2 class="has-text-weight-bold has-text-centered vertical-align-abs width-one discountCupon"></h2>
									</div>
								</div>
								<div class="tile is-parent is-4">
									<div class="content is-relative">
										<p class="is-marginless is-capitalized is-size-7 has-text-weight-bold has-text-justified font-s-2">
											<i class="font-s-1 statusCupon"></i>
										<br><i class="font-color-1 timeCupon"></i>
										</p>
									</div>
								</div>
								<div class="tile is-parent is-4 background-p-2 handed button-carga-cupon">
									<div class="content width-one">
										<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white width-one"><?php echo _COUPBTN1;?></p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!--COMENTARIOS DE NUESTROS CLIENTES-->
<div class="section pa-no-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _COMMT1;?></h1>
		<div class="columns">
			<div class="column is-6">
				<div class="box">
					<article class="media">
						<div class="media-content">
							<div class="content">
								<p>
									<strong id="commentsName" class="is-size-5 has-text-weight-bold"></strong> <small id="commentsPhone" class="is-size-5 as-text-weight-bold font-s-2"></small><br><small id="commentsDate" class="is-size-7 font-color-1"></small>
									<div class="columns">
										<div class="column is-1">
											<strong class="is-size-1 has-text-weight-bold font-color-2">"</strong>
										</div>
										<div class="column">
											<p id="commentsDesc" class="font-s-1"></p>
										</div>
									</div>
								</p>
							</div>
						</div>
					</article>
				</div>
			</div>
			<div class="column is-6">
				<div class="box">
					<article class="media">
						<div class="media-content">
							<div class="content">
								<p>
									<strong id="commentsName2" class="is-size-5 has-text-weight-bold"></strong> <small id="commentsPhone2" class="is-size-5 as-text-weight-bold font-s-2"></small><br><small id="commentsDate2" class="is-size-7 font-color-1"></small>
									<div class="columns">
										<div class="column is-1">
											<strong class="is-size-1 has-text-weight-bold font-color-2">"</strong>
										</div>
										<div class="column">
											<p id="commentsDesc2" class="font-s-1"></p>
										</div>
									</div>
								</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</div>
