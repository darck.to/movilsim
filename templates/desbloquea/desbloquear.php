<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _HT1; ?><p><?php echo _HT2; ?></p>
			</h1>
		</div>
	</div>
</section>

<!--COMPLETA TU ORDEN-->
<div class="section pa-no-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _ORDET1; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _ORDES1; ?></h5>

		<!--DATOS-->
		<?php

			$desbloqueaSectionHidden = "is-hidden";

			$marca = isset($_GET['p']) ? $_GET['p'] : '';
			$modelo = isset($_GET['f']) ? $_GET['f'] : '';

			if ($_POST['paso']) {
				$paso = $_POST['paso'];
				if ($paso == 1) {
					$idMarca = $_POST['id'];
					$nameMarca = $_POST['marca'];
				} elseif ($paso == 2) {
					$paso = $_POST['paso'];
					$idMarca = $_POST['id'];
					$nameMarca = $_POST['marca'];
					$modelGET = $_POST['equipo'];
					$idmodelGET = $_POST['equipoID'];
					$imgGET = $_POST['img'];
				}
			}

			if (empty($marca)) {
			} else {
				include_once('../../func/functions.php');
				$idMarca = cargaIdMarca($_GET['p']);
				$nameMarca = cargaNombreMarca($idMarca);
				$paso = 1;

			}
			if (empty($modelo)) {
			} else {
				include_once('../../func/functions.php');
				$idmodelGET = cargaIdEquipo($idMarca,$_GET['f']);
				$modelGET = cargaNombreEquipo($idmodelGET);
				$imgGET = "img/" . cargaImgEquipo($idmodelGET);
				$paso = 2;
			}

			if ($paso == 1) {

				//SECCION DE CUERPO
				echo "<div class='is-hidden' id='id'>".$idMarca."</div>";
				echo "<div class='is-hidden' id='marca'>".$nameMarca."</div>";

				include('templates/desbloquea/desbloquea-equipos.php');

			} elseif ($paso == 2) {

				$desbloqueaSectionHidden = "";
				//SECCION DE CUERPO
				echo "<div class='is-hidden' id='id'>".$idMarca."</div>";
				echo "<div class='is-hidden' id='marca'>".$nameMarca."</div>";
				echo "<div class='is-hidden' id='equipo'>".$modelGET."</div>";
				echo "<div class='is-hidden' id='equipoID'>".$idmodelGET."</div>";
				echo "<div class='is-hidden' id='img'>".$imgGET."</div>";

				include('templates/desbloquea/desbloquea-selects.php');

			} elseif ($_POST['paso'] == 3) {

				$desbloqueaSectionHidden = "";
				//SECCION DE CUERPO
				echo "<div class='is-hidden' id='id'>".$_POST['id']."</div>";
				echo "<div class='is-hidden' id='marca'>".$_POST['marca']."</div>";
				echo "<div class='is-hidden' id='equipo'>".$_POST['equipo']."</div>";
				echo "<div class='is-hidden' id='equipoID'>".$_POST['equipoID']."</div>";
				echo "<div class='is-hidden' id='pais'>".$_POST['pais']."</div>";
				echo "<div class='is-hidden' id='operator'>".$_POST['operator']."</div>";

				include('templates/desbloquea/desbloquea-imei.php');
			}

		?>

	</div>
</div>

<!--CONOCE TUS DATOS-->
<div class="section background-s-2 pa-half-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _ORDEKNOT1; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _ORDEKNOT1S1; ?></h5>
		<div class="box">
			<div class="columns">
				<div class="column is-9">
					<div class="content">
						<p>
							<h5 class="is-size-5 is-marginless"><?php echo _ORDEKNOS1; ?></h5>
							<ul class="ma-t-zero">
								<li class="is-size-6"><?php echo _ORDEKNOS1TXT1; ?></li>
								<li class="is-size-6"><?php echo _ORDEKNOS1TXT2; ?></li>
							</ul>
							<h5 class="is-size-5 is-marginless"><?php echo _ORDEKNOS2; ?></h5>
							<ul class="ma-t-zero">
								<li class="is-size-6">A)&nbsp;<?php echo _ORDEKNOS2TXT1;?></li>
								<li class="is-size-6">B)&nbsp;<?php echo _ORDEKNOS2TXT2;?></li>
								<li class="is-size-6">C)&nbsp;<?php echo _ORDEKNOS2TXT3;?></li>
							</ul>
							<h5 class="is-size-5"><?php echo _ORDEKNOS3; ?>&nbsp;<b class="font-color-2"><?php echo _ORDEKNOS33; ?></b></h5>
						</p>
					</div>
				</div>
				<div class="column is-3">
					<div class="image">
						<img src="img/desbloquear-img.png" alt="Conoce tu IMEI">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--COMO DESBLOQUEAR UN-->
<div class="section <?php echo $desbloqueaSectionHidden;?> pa-half-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _ORDEUNLT1; ?>&nbsp;<i id="sectionMarcaEquipo" class="text-upp"></i></h1>
		<div class="box">
			<div class="columns">
				<div class="column is-9">
					<div id="descPhone" class="content">

					</div>
				</div>
				<div class="column is-3">
					<div class="image">
						<img id="sectionImgEquipo" src="img/desbloquear-img.png" alt="<?php echo _altORDEUNLIMG;?>">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--POR QUE DESBLOQUEAR CON NOSOTROS-->
<div class="section background-s-2 pa-half-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _ORDEWHYT1; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _ORDEWHYT1S1; ?></h5>
		<nav class="level">
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-check-circle font-color-2 pa-b-one"></i>
					<p class="heading is-size-5"><?php echo _ORDEWHYS1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS1TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS1TXT2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS1TXT3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-thumbs-up font-color-2 pa-b-one"></i>
					<p class="heading is-size-5"><?php echo _ORDEWHYS2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS2TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS2TXT2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS2TXT3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-question-circle font-color-2 pa-b-one"></i>
					<p class="heading is-size-5"><?php echo _ORDEWHYS3;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS3TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS3TXT2;?>
					<p class="title is-size-6 has-text-weight-light"><?php echo _ORDEWHYS3TXT3;?></p>
				</div>
			</div>
		</nav>
	</div>
</div>

<!--CARGA SCRIPT LOCAL-->
<script type="text/javascript" src="scripts/desbloquea/desbloquea.js"></script>
