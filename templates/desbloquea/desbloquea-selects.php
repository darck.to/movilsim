<!--BOX PROCESO-->
<div class="box is-radiusless is-shadowless background-p-1">
	<div class="columns">
		<div class="column is-3 pa-one">
			<div class="image has-background-white pa-one shadow-blue">
				<img class="marca-brand" src="">
			</div>
		</div>
		<div class="column is-9 pa-one">
			<div class="content pa-one background-p-3 radius-5">
				<h4 class="has-text-white"><i id="marcaBrand"></i>&nbsp;|&nbsp;<b id="equipoBrand"></b></h4>
				<h4 class="has-text-white"><?php echo _ORDESES1;?></h4>
				<h6><smal class="has-text-white has-text-weight-light"><?php echo _ORDESETXT1;?></small></h6>

				<div class="columns">
					<div class="column">
						<div class="content">
							<div class="select is-fullwidth">
								<select id="selectCountry" class="input-format">
									<option value="" readonly selected><?php echo _ORDESESEL1; ?></option>
								</select>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="content">
							<div class="select is-fullwidth">
								<select id="selectOperator" class="input-format">
									<option value="" readonly selected><?php echo _ORDESESEL2; ?></option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<a class="button thirdStepOrder ma-b-one input-format"><b><?php echo _ORDESEBTN2; ?></b></a>

				<!--MENSAJE NO OPERATOR-->
				<div class="row">
					<div class="columns">
						<div id="boxMessage" class="column is-12">
						</div>
					</div>
				</div>

				<!--BARRA DE PROGRESO-->
				<div class="columns">
					<div class="column pa-one">
						<div class="content pa-one background-p-3 radius-5">
							<progress class="progress is-large is-warning" value="66" max="100">66%</progress>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include_once('init.php'); ?>

<!--CARGA SCRIPT LOCAL-->
<script type="text/javascript" src="scripts/desbloquea/desbloquea-selects.js"></script>
