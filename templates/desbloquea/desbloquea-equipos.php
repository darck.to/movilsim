<!--BOX PROCESO-->
<div class="box is-radiusless is-shadowless background-p-1">
	<div class="columns">
		<div class="column pa-one">
			<div class="content pa-one background-p-3 radius-5">
				<h2 class="has-text-white"><i id="marcaBrand"></i><small></small></h2>
				<h6 class="has-text-white"><?php echo _ORDEEQS1;?></h6>

				<div id="desqloqueaBox" class="columns is-multiline"></div>

				<!--BARRA DE PROGRESO-->
				<div class="columns">
					<div class="column pa-one">
						<div class="content pa-one background-p-3 radius-5">
							<progress class="progress is-large is-warning" value="33" max="100">33%</progress>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include_once('init.php'); ?>

<!--CARGA SCRIPT LOCAL-->
<script type="text/javascript" src="scripts/desbloquea/desbloquea-equipos.js"></script>
