<!--BOX PROCESO-->
<div class="box is-radiusless is-shadowless background-p-1">
	<div class="columns">
		<div class="column is-3 pa-one">
			<div class="image has-background-white pa-one shadow-blue">
				<img class="marca-brand" src="">
			</div>
		</div>
		<div class="column is-9 pa-one">
			<div class="content pa-one background-p-3 radius-5">
				<!--CURRENCY-->
				<div class="content is-pulled-right selectCurrency">
					<div class="content has-text-right ma-b-twopx">
						<h6 class="has-text-weight-light has-text-white is-size-7 ma-b-zero"><?php echo _CURRTXT;?></h6>
					</div>
					<div class="content has-text-right">
						<div class="select is-small">
							<select  class="changeCurrency">
								<option value=1>PESO - MXN</option>
								<option value=2>DOLAR - USD</option>
								<option value=3>EURO - EUR</option>
							</select>
						</div>
					</div>
				</div>

				<h4 class="has-text-white"><i id="marcaBrand"></i>&nbsp;|&nbsp;<b id="equipoBrand" class="text-upp"></b></h4>

				<div class="is-hidden ma-b-one paymentCheck">
					<div class="content pa-one ma-b-zero">
						<h5 class="has-text-centered has-text-weight-semibold has-text-white"><?php echo _ORDEORDT1;?></h5>
						<h5 class="has-text-centered has-text-weight-light has-text-white is-paddingless ma-b-zero"><?php echo _ORDEORDT1S1; ?></h5>
					</div>

					<!--CAJA DE OFERTAS-->
					<div class="columns is-multiline offerBox">
					</div>

				</div>

				<!--ORDERBOX-->
				<div class="columns pa-one is-hidden order-background">
					<div id="orderDiv" class="has-background-white column is-hidden radius-5 shadow-blue-darker"></div>
				</div>

				<div id="inputOrderForm" class="is-hidden">

					<div id="inputTextForm" class="column is-8 is-offset-2 has-text-centered">

						<div class="content">
							<h4 class="has-text-centered has-text-weight-semibold has-text-white"><?php echo _ORDEORDS1;?></h4>
						</div>

						<div class="columns">
							<div class="column">
								<div class="content is-relative">
									<i class="is-hidden desbloquea-input-check"></i>
									<input class="input input-format" id="name" type="text" name="name" placeholder="<?php echo _ORDEORDFI1; ?>" tabindex=1 focus>
								</div>
							</div>
							<div class="column">
								<div class="content is-relative">
									<i class="is-hidden desbloquea-input-check"></i>
									<input class="input input-format" id="email" type="email" name="email" class="validate" placeholder="<?php echo _ORDEORDFI2; ?>" tabindex=3>
									<small class="is-hidden red-text emailCheck"><?php echo _ORDEORDFI2R; ?></small>
								</div>
							</div>
						</div>

						<div class="columns">
							<div class="column">
								<div class="content is-relative">
									<i class="is-hidden desbloquea-input-check"></i>
									<input class="input input-format" id="tel" pattern="[0-9]" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="tel" placeholder="<?php echo _ORDEORDFI4; ?>" tabindex=2>
								</div>
							</div>
							<div class="column">
								<div class="content is-relative">
									<i class="is-hidden desbloquea-input-check"></i>
									<input class="input input-format" id="confirm" type="email" name="confirm" class="validate" placeholder="<?php echo _ORDEORDFI3; ?>" tabindex=4>
									<small class="is-hidden confirmCheck"><?php echo _ORDEORDFI3R; ?></small>
								</div>
							</div>
						</div>

						<div class="control ma-b-one is-relative">
							<i class="is-hidden desbloquea-input-check"></i>
							<input class="input input-format" id="imei" type="text" name="imei" placeholder="<?php echo _ORDEORDFI5 ?>" tabindex=6 disabled>
							<small class="is-hidden imeiCheck"><?php echo _ORDEORDFI5R; ?></small>
						</div>

						<div class="columns">
              <div class="column">
              	<form>
                  <div class="control has-text-centered ma-one">
                    <label class="radio pa-one radius-5 desbloquea-option">
                      <input type="radio" name="payoption" value=1>
                      <i class="far fa-credit-card"></i>
                      <span class="has-text-grey"><?php echo _ORDEORDOPT1; ?></span>
                    </label>
                    <label class="radio pa-one radius-5 desbloquea-option">
                      <input type="radio" name="payoption" value=2>
                      <i class="fab fa-cc-paypal"></i>
                      <span class="has-text-grey"><?php echo _ORDEORDOPT2; ?></span>
                    </label>
                    <label class="radio pa-one radius-5 desbloquea-option">
                      <input type="radio" name="payoption" value=3>
                      <i class="far fa-money-bill-alt"></i>
                      <span class="has-text-grey"><?php echo _ORDEORDOPT3; ?></span>
                    </label>
                  </div>
                </form>
              </div>
          </div>

					<a class="button is-medium has-text-white has-text-weight-bold is-hidden ma-b-one input-format background-p-2 disableButton"><b><?php echo _ORDEORDBTN1; ?></b></a>

					</div>

					<div class="column is-8 is-offset-2">
						<div id="confirmOrder" class="content"></div>
					</div>

				</div>

				<!--BARRA DE PROGRESO-->
				<div class="columns">
					<div class="column pa-one">
						<div class="content pa-one background-p-3 radius-5">
							<progress class="progress is-large is-warning" value="99" max="100">99%</progress>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>

<?php include_once('init.php'); ?>

<!--CARGA SCRIPT LOCAL-->
<script type="text/javascript" src="scripts/desbloquea/desbloquea-imei.js"></script>

<!-- Paypal-->
<script src="//www.paypalobjects.com/api/checkout.js"></script>
