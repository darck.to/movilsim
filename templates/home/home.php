<!--FULL WIDTH HEADER-->
<section class="hero is-medium background-p-1">
	<div class="hero-body">
		<div class="container is-relative">

		</div>
	</div>
</section>

<!--OMNI SEARCH-->
<div class="container">
	<div class="columns">
		<div class="column is-three-fifths is-offset-one-fifth">
			<div class="omni-buscador background-p-2">
				<input class="input is-relative omni-input omniSearch" type="text" placeholder="<?php echo _IPH; ?>">
			</div>
		</div>
	</div>
</div>

<!--MARCAS-->
<div class="section pa-no-mobile pa-b-one">
	<div class="container">
		<div class="columns">
			<div class="column is-12">
				<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _BRAT1; ?></h1>
				<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _BRASUB; ?></h5>
				<div class="column is-12 is-relative pa-no">
					<div id="marcaBox" class="columns is-multiline has-background-white width-one">
					</div>
					<div class="showMarcas has-text-weight-bold has-background-white handed font-s-2">
						<p class="brandToogle"><?php echo _BRAMORE; ?></p>
						<p class="brandToogle is-hidden"><?php echo _BRALESS; ?></p>
						<i class='icon-show fas fa-chevron-down fa-5x'></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--NUESTROS PRINCIPIOS-->
<div class="section background-s-2 pa-no-mobile is-mobile-blo-lef-wid">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _PRINT1; ?></h1>
		<nav class="level">
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-thumbs-up font-color-2"></i>
					</div>
					<p class="heading is-size-5"><?php echo _PRINS1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS1TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS1TXT2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS1TXT3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-fast-forward font-color-2"></i>
					</div>
					<p class="heading is-size-5"><?php echo _PRINS2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS2TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS2TXT2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS2TXT3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-money-bill-wave font-color-2"></i>
					</div>
					<p class="heading is-size-5"><?php echo _PRINS3;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS3TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS3TXT2;?>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS3TXT3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-question font-color-2"></i>
					</div>
					<p class="heading is-size-5"><?php echo _PRINS4;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS4TXT1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS4TXT2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _PRINS4TXT3;?></p>
				</div>
			</div>
		</nav>
	</div>
</div>

<!--COMENTARIOS DE NUESTROS CLIENTES-->
<div class="section pa-no-mobile is-mobile-blo-lef-wid">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _COMMT1;?></h1>
		<div class="columns">
			<div class="column is-6">
				<div class="box">
					<article class="media">
						<div class="media-content">
							<div class="content">
								<p>
									<strong id="commentsName" class="is-size-5 has-text-weight-bold"></strong> <small id="commentsPhone" class="is-size-5 as-text-weight-bold font-s-2"></small><br><small id="commentsDate" class="is-size-7 font-color-1"></small>
									<div class="columns is-mobile is-multiline pa-no ma-no">
										<div class="column is-1 is-relative">
											<strong class="commmentSimbol has-text-weight-bold font-color-2">"</strong>
										</div>
										<div class="column is-9">
											<p id="commentsDesc" class="font-s-1"></p>
										</div>
									</div>
								</p>
							</div>
						</div>
					</article>
				</div>
			</div>
			<div class="column is-6">
				<div class="box">
					<article class="media">
						<div class="media-content">
							<div class="content">
								<p>
									<strong id="commentsName2" class="is-size-5 has-text-weight-bold"></strong> <small id="commentsPhone2" class="is-size-5 as-text-weight-bold font-s-2"></small><br><small id="commentsDate2" class="is-size-7 font-color-1"></small>
									<div class="columns is-mobile is-multiline pa-no ma-no">
										<div class="column is-1 is-relative">
											<strong class="commmentSimbol has-text-weight-bold font-color-2">"</strong>
										</div>
										<div class="column is-9">
											<p id="commentsDesc2" class="font-s-1"></p>
										</div>
									</div>
								</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</div>

<!--GRAY BLOCK-->
<div class="section background-s-1 pa-no-mobile is-mobile-blo-lef-wid">
	<div class="container">
		<div class="columns">
			<div class="column is-three-fifths">
				<div class="box is-radiusless is-shadowless background-none">
					<div class="content">
						<h1 class="title has-text-left has-text-weight-bold has-text-white"><?php echo _PARAT1;?></h1>
						<p class="has-text-white"><?php echo _PARATXT1; ?></p>
						<p class="has-text-white"><?php echo _PARATXT2; ?></p>
					</div>
				</div>
			</div>
			<div class="column is-two-fifths logo-descripcion"><div class="logo-descripcion-p"></div></div>
		</div>
	</div>
</div>
