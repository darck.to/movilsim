<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _NOSOT1; ?><p><?php echo _NOSOT11; ?></p>
			</h1>
		</div>
	</div>
</section>

<!--¿TIENES UNA DUDA? TE AYUDAMOS-->
<div class="section pa-no-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _NOSOT2; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1 is-uppercase"><?php echo _NOSOT2S1; ?></h5>
    
    <!--Tiles-->
    <div class="tile is-ancestor">
      <div class="tile is-vertical is-8">
        <div class="tile">
          <div class="tile is-parent is-vertical">
            <article class="tile is-child has-background-light pa-one">
              <p class="title"><?php echo _NOSOT3; ?></p>
              <p class="subtitle"><?php echo _NOSOT3S1; ?></p>
            </article>
            <article class="tile is-child has-background-light pa-one">
              <p class="title"><?php echo _NOSOT4; ?></p>
              <p class="subtitle"><?php echo _NOSOT4S1; ?></p>
            </article>
          </div>
        </div>
        <div class="tile is-parent">
          <article class="tile is-child has-background-light pa-one">
            <p class="title"><?php echo _NOSOT5; ?></p>
            <p class="subtitle"><?php echo _NOSOT5S1; ?></p>
            <div class="content">
              <!-- Content -->
            </div>
          </article>
        </div>
      </div>
      <div class="tile is-parent">
        <article class="tile is-child has-background-light pa-one">
          <div class="content">
            <p class="title"><?php echo _NOSOT6; ?></p>
            <p class="subtitle"><?php echo _NOSOT6S1; ?></p>
            <div class="content">
              <p><?php echo _NOSOT6P1; ?></p>
            </div>
          </div>
        </article>
      </div>
    </div>
    
	</div>
</div>
