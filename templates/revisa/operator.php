<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _CHECKT1; ?><p><?php echo _CHECKOT2; ?></p>
			</h1>
		</div>
	</div>
</section>

<!--REVISA TU IMEI-->
<div class="section">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _CHECKT3; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _CHECKOT3S1; ?></h5>

		<!--BOX IMEI-->
		<div id="orderDiv" class="box is-radiusless is-shadowless background-p-1">
			<div class="columns">
				<div class="column pa-one">
					<div class="content pa-one background-p-3 radius-5">

						<div id="inputFormOrder">
							<div id="inputSelectForm" class="columns">
								<div class="column is-half is-offset-one-quarter">
									<div class="content">
										<div class="select is-fullwidth">
											<select id="result" class="input-format">
												<option value="" readonly selected><?php echo _CHECKSEL1; ?></option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="columns">
								<div class="column is-half is-offset-one-quarter">
									<div class="content">
										<input class="input input-format is-hidden" id="imeiForm" type="text" placeholder="<?php echo _CHECKOFI1;?>">
										<small class="is-hidden red-text imeiCheck"><?php echo _CHECKOFI1R; ?></small>
									</div>
								</div>
							</div>
							<div class="columns">
								<div class="column is-half is-offset-one-quarter">
									<div class="content">
										<input class="input input-format is-hidden" id="emailImeiForm" type="text" placeholder="<?php echo _CHECKOFI2;?>">
										<small class="is-hidden red-text emailCheck"><?php echo _CHECKOFI2R; ?></small>
									</div>
								</div>
							</div>
						</div>

						<!--SOURCE-->
						<div id="buttonPayOptions" class="columns is-hidden">
              <div class="column is-half is-offset-one-quarter">
              	<form>
                  <div class="control has-text-centered ma-one">
                    <label class="radio pa-one radius-5 desbloquea-option">
                      <input type="radio" name="payoption" value=1>
                      <i class="far fa-credit-card"></i>
                      <span class="has-text-grey"><?php echo _ORDEORDOPT1; ?></span>
                    </label>
                    <label class="radio pa-one radius-5 desbloquea-option">
                      <input type="radio" name="payoption" value=2>
                      <i class="fab fa-cc-paypal"></i>
                      <span class="has-text-grey"><?php echo _ORDEORDOPT2; ?></span>
                    </label>
                    <label class="radio pa-one radius-5 desbloquea-option">
                      <input type="radio" name="payoption" value=3>
                      <i class="far fa-money-bill-alt"></i>
                      <span class="has-text-grey"><?php echo _ORDEORDOPT3; ?></span>
                    </label>
                  </div>
                </form>
              </div>
          	</div>

						<input id="srcForm" type="hidden" value="2">

						<div id="boxPayButton" class="column is-half is-offset-one-quarter has-background-white paymentCheck is-hidden">
							<div class="tile is-ancestor">
								<div class="tile is-parent is-6 pa-two has-text-centered">
									<div class="content is-relative width-one">
										<h2 class="has-text-weight-bold has-text-centered vertical-align-abs width-one precio">$00.00</h2>
									</div>
								</div>
								<div class="tile is-parent is-6 pa-two background-p-2 handed button-carga-imei">
									<div class="content is-relative width-one">
										<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white vertical-align-abs width-one"><?php echo _CHECKOBTN1;?></p>
									</div>
								</div>
							</div>
						</div>

						<!--PAYBUTTONS-->
						<div id="payOptions" class="column is-half is-offset-one-quarter">
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!--EN QUE CONSISTE ESTE SERVICIO-->
<div class="section background-s-2">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _CHECKOT4; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _CHECKOT4S1; ?></h5>

		<div class="columns">
			<div class="column">
				<div class="box is-radiusless is-shadowless background-none">
					<div class="content">
						<p class="is-size-6"><b class="is-size-3 has-text-weight-bold font-color-2">1.&nbsp;</b><?php echo _CHECKOT4E1; ?></p>
						<p class="is-size-6"><b class="is-size-3 has-text-weight-bold font-color-2">2.&nbsp;</b><?php echo _CHECKOT4E2; ?></p>
						<p class="is-size-6"><b class="is-size-3 has-text-weight-bold font-color-2">3.&nbsp;</b><?php echo _CHECKOT4E3; ?></p>
						<p class="is-size-6"><b class="is-size-3 has-text-weight-bold font-color-2">4.&nbsp;</b><?php echo _CHECKOT4E4; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--POR QUE DESBLOQUEAR CON NOSOTROS-->
	<div class="container">
		<nav class="level">
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-question-circle font-color-2 pa-b-one"></i>
					<p class="heading is-size-5"><?php echo _CHECKS1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS1P1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS1P2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS1P3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-at font-color-2 pa-b-one"></i>
					<p class="heading is-size-5"><?php echo _CHECKS2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS2P1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS2P2;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS2P3;?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-check-circle font-color-2 pa-b-one"></i>
					<p class="heading is-size-5"><?php echo _CHECKS3;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS3P1;?></p>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS3P2;?>
					<p class="title is-size-6 has-text-weight-light"><?php echo _CHECKS3P3;?></p>
				</div>
			</div>
		</nav>
	</div>
</div>

<!-- Paypal-->
<script src="//www.paypalobjects.com/api/checkout.js"></script>
