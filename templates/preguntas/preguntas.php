<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _ASKT1; ?><p><?php echo _ASKT11; ?></p>
			</h1>
		</div>
	</div>
</section>

<!--¿TIENES UNA DUDA? TE AYUDAMOS-->
<div class="section pa-no-mobile">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _ASKT2; ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _ASKT2S1; ?></h5>

		<!--BOX IMEI-->
		<div class="columns">
			<div class="column is-8 is-offset-2 pa-one">
				<div class="box is-radiusless is-shadowless background-p-1">
					<div class="columns">
						<div class="column">
							<div class="content pa-one background-p-3 radius-5">

								<div class="columns">
									<div class="column is-8 is-offset-2 ">
										<div class="content">
											<input class="input input-format omniSearch border-orange" type="text" placeholder="<?php echo _ASKFI1;?>">
										</div>
									</div>
								</div>

								<div class="column is-padingless">
									<div id="preguntasMensajes" class="container-pajinator"></div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
