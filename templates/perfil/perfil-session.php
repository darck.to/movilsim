<!--Tables-->
<link type="text/css" rel="stylesheet" href="css/datatables.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="css/buttons.dataTables.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"  media="screen,projection"/>

<div class="columns">
	<div class="column pa-one">
		<div class="content pa-one background-p-3 radius-5">
			
			<table id="table" class="display" style="width:100%">
		        <thead>
		            <tr>
		                <th>Folio</th>
		                <th>Fecha</th>
		                <th>#</th>
		                <th>IMEI</th>
		                <th>Marca</th>
		                <th>Equipo</th>
		                <th>Precio</th>
		                <th>Status</th>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th>Folio</th>
		                <th>Fecha</th>
		                <th>#</th>
		                <th>IMEI</th>
		                <th>Marca</th>
		                <th>Equipo</th>
		                <th>Precio</th>
		                <th>Status</th>
		        </tfoot>
		    </table>
		
		</div>
	</div>
</div>

<!--DataTable-->
<script type="text/javascript" src="js/datatables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>