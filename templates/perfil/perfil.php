<!--FULL WIDTH HEADER-->
<section class="hero background-p-1 hero-background is-hidden-mobile">
	<div class="">
		<div class="container">
		</div>
	</div>
</section>

<?php
	session_start();

	if (isset($_SESSION['user'])) {
		$titulo = "BIENVENIDO USUARIO" ."<i class='has-text-link'>&nbsp;".$_SESSION['user']."</i>";
		$logout = "<i class=\"fas fa-sign-out-alt has-text-danger handed exitSession\"></i>";
		$container = "templates/perfil/perfil-session.php";
		$contact = "<i class=\"fas fa-envelope-open-text handed ma-l-one createMessage\"></i>";
		$idhidden = "";
	} else {
		$titulo = "INICIA SESI&Oacute;N";
		$logout = "";
		$container = "templates/perfil/perfil-login.php";
		$contact = "";
		$idhidden = "is-hidden";
	}
?>

<div class="section pa-no-mobile">
	<div class="container">
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _($titulo); ?><span class="tag is-medium is-pulled-right background-p-2 shadow-blue <?php echo $idhidden;?>"><?php echo $contact; echo $logout; ?></span></h5>

		<!--INICIA SESION-->
		<div class="box is-radiusless is-shadowless background-p-1">
			<?php include ($container); ?>
		</div>

	</div>
</div>
