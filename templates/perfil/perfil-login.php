<div class="columns">
	<div class="column pa-one">
		<div class="content pa-one background-p-3 radius-5">
			
			<div class="columns">
				<div class="column is-half is-offset-one-quarter">
					<div class="columns">
						<div class="column is-half">
							<div class="content">
								<input class="input input-format" id="mail" type="text" placeholder="<?php echo _("Correo Electr&oacute;nico");?>">
							</div>
						</div>
						<div class="column is-half">
							<div class="content">
								<input class="input input-format" id="order" type="text" placeholder="<?php echo _("N&uacute;mero de orden");?>">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="column is-half is-offset-one-quarter handed revisaPerfil">
				<div class="content has-text-centered">
					<a class="button is-medium is-link input-format"><?php echo _("LOGIN");?></a>
				</div>
			</div>
		
		</div>
	</div>
</div>