<?php
	//HEADER HOME
	define('_TAGLANG', 'es');
	define('_HT1', 'Libera tu');
	define('_HT2', 'tel&eacute;fono');
	define('_HT3', 'desde donde');
	define('_HT4', 'est&eacute;s');
	//PROMOTION
	define('_PROMH1','¡DESBLOQUEA ESTA PROMOCI&Oacute;N!');
	define('_PROMSP1','Solo hoy puedes obtener un descuento de');
	define('_PROMSP2','En los pr&oacute;ximos');
	define('_PROMSP3','minutos obtendr&aacute;s un descuento en nuestro servicio de desbloqueo');
	//MENU
	define('_MENU1','DESBLOQUEA');
	define('_MENU2','REVISAR IMEI');
	define('_MENU2S1','REVISAR BLACKLIST');
	define('_MENU2S2','REVISAR OPERADOR ORIGINAL');
	define('_MENU2S3','REVISAR MODELO ESPEC&Iacute;FICO');
	define('_MENU3','CUPONES');
	define('_MENU4','PREGUNTA A UN EXPERTO');
	define('_MENU5','CONTACTO');
  define('_MENU6','M&Eacute;TODOS DE PAGO');
  define('_MENU7','¿C&Oacute;MO FUNCIONA?');
  define('_MENU8','ACERCA DE NOSOTROS');
	//FOOTERMENU
	define('_PAYT1','M&Eacute;TODOS DE PAGO');
	define('_PAYT2','PAGOS SEGUROS');
	define('_FMENU1','MARCAS DESTACADAS');
	define('_FMENU2','MARCAS POPULARES');
	define('_FMENU3','NAVEGACI&Oacute;N');
	define('_FMENU4','SOPORTE Y REDES SOCIALES');
	//HOMETEMPLATES
	//OMNI SEARCH
	define('_IPH', 'BUSCA TU MODELO');
	//BRANDBOX
	define('_BRAT1', 'MARCAS');
	define('_BRASUB', 'SELECCIONA LA MARCA DE TU EQUIPO');
	define('_BRAMORE', 'MOSTRAR M&Aacute;S');
	define('_BRALESS', 'MOSTRAR MENOS');
	//OUR PRINCIPLES
	define('_PRINT1', 'NUESTROS PRINCIPIOS');
	define('_PRINS1', 'GARANT&Iacute;A');
	define('_PRINS1TXT1', 'No corras riesgos');
	define('_PRINS1TXT2', 'nuestro servicio');
	define('_PRINS1TXT3', 'est&aacute; garantizado');
	define('_PRINS2', 'VELOCIDAD');
	define('_PRINS2TXT1', 'Obt&eacute;n tu c&oacute;digo');
	define('_PRINS2TXT2', 'en cuanto tu pago');
	define('_PRINS2TXT3', 'sea procesado');
	define('_PRINS3', 'AHORROS');
	define('_PRINS3TXT1', 'Con nuestros precios');
	define('_PRINS3TXT2', 'competitivos');
	define('_PRINS3TXT3', 'obtienes calidad y servicio');
	define('_PRINS4', 'SOPORTE');
	define('_PRINS4TXT1', 'Cont&aacute;ctenos');
	define('_PRINS4TXT2', 'tenemos soporte y garant&iacute;a');
	define('_PRINS4TXT3', 'las 24/7!');
	//COMMENTS
	define('_COMMT1','COMENTARIOS DE NUESTROS CLIENTES');
	//PARAGRAPH
	define('_PARAT1','¿POR QU&Eacute; DESBLOQUEAR TU EQUIPO CON NOSOTROS?');
	define('_PARATXT1','MovilSIM comenzó en el 2016, somos una empresa dedicada a las liberaciones de teléfonos móvil al mayoreo y menudeo. Nos esforzamos en tener los mejores precios del mercado y los mejores tiempos, para que así nuestros clientes se lleven una buena experiencia sobre todos los servicios que ofrece MovilSIM. Todos nuestros servicios trabajan las 24 horas del día los 7 días de la semana de manera automática, esto hace que nuestros procesos sean rápidos y eficaces y lo mejor de todo es que sin moverse de casa.');
	define('_PARATXT2','MovilSIM es tu mejor opción.');
	//UNLOCKTEMPLATES
	//COMPLETE YOUR ORDERS
	define('_ORDET1','COMPLETA TU ORDEN');
	define('_ORDES1','SELECCIONA TU MODELO, PA&Iacute;S, OPERADOR Y COMPLETA TUS DATOS PERSONALES');
	//EQUIPOS
	define('_ORDEEQS1','Selecciona el equipo de tu modelo');
	define('_ORDEEQS2','Desbloquea tu equipo');
	define('_ORDEEQS22','via IMEI');
	define('_ORDEEQTXT1','Desbloque seguro que respeta la garant&iacute;a del fabricante');
	define('_ORDEEQTXT2','M&eacute;todo recomendado por');
	define('_ORDEEQTXT22','operadores');
	define('_ORDEEQTXT3','Desbloquealo');
	define('_ORDEEQTXT33','para siempre');
	define('_ORDEEQTXT4','Rapido y en linea, sin dejar tu equipo en servicio');
	define('_ORDEEQBTN1','Desbloquea tu equipo');
	//EQUIPOS SELECTS
	define('_ORDESES1','Selecciona el pa&iacute;s y el operador de origen');
	define('_ORDESETXT1','No el operador al que quieres cambiar');
	define('_ORDESESEL1','Selecciona tu pa&iacute;s');
	define('_ORDESESEL2','Selecciona tu operador');
	define('_ORDESESEL3','Operadores');
	define('_ORDESEBTN2','Siguiente paso');
	define('_ORDERSESEH1','SERVICIO NO DISPONIBLE');
	define('_ORDERSESEP1','Si dejas tu correo electr&oacute;nico te avisaremos cuando contemos con el servicio para tu equipo');
	define('_ORDERSESEI1','Correo electr&oacute;nico');
	define('_ORDERSESEB1','¡Avisame!');
	//KNOW YOUR DATA
	define('_ORDEKNOT1','CONOCE LA INFORMACI&Oacute;N DE TU EQUIPO');
	define('_ORDEKNOT1S1','SIGUE ESTOS PASOS PARA CONOCER TU IMEI Y COMO DESBLOQUEAR TU EQUIPO');
	define('_ORDEKNOS1','OBT&Eacute;N TU IMEI');
	define('_ORDEKNOS1TXT1','MARCA *#06# EN TU EQUIPO Y SE MOSTRARA TU N&Uacute;MERO IMEI EN PANTALLA.');
	define('_ORDEKNOS1TXT2','EL N&Uacute;MERO IMEI ES &Uacute;NICO E INSTRANSFERIBLE QUE SE UTILIZA PARA REPORTAR Y BLOQUEAR EQUIPOS ROBADOS.');
	define('_ORDEKNOS2','CUANDO TENGAS TU N&Uacute;MERO IMEI');
	define('_ORDEKNOS2TXT1','CON UNA TARJETA SIM NO ACEPTADA POR TU OPERADOR ORIGINAL, PRENDE TU EQUIPO Y ESPERA A QUE INICIE EL MISMO.');
	define('_ORDEKNOS2TXT2','LA PANTALLA MOSTRARA "SIM CARD UNLOCK CODE".');
	define('_ORDEKNOS2TXT3','INTRODUCE EL C&Oacute;DIGO QUE TE PREVEEMOS Y PRESIONA "DESBLOQUEAR".');
	define('_ORDEKNOS3','¡DISFRUTA TU');
	define('_ORDEKNOS33','EQUIPO DESBLOQUEADO!');
	//HOW TO UNLOCK A...
	define('_ORDEUNLT1','COMO DESBLOQUEAR UN');
	define('_altORDEUNLIMG','Como desbloquear');
	//WHY UNLOCK WITH US
	define('_ORDEWHYT1','¿POR QU&Eacute; DESBLOQUEAR CON NOSOTROS?');
	define('_ORDEWHYT1S1','CONOCE LAS RAZONES QUE NOS DISTINGUEN DE NUESTRA COMPETENCIA');
	define('_ORDEWHYS1','PERMANENTE');
	define('_ORDEWHYS1TXT1','El desbloqueo de');
	define('_ORDEWHYS1TXT2','tu equipo esta garantizado');
	define('_ORDEWHYS1TXT3','por toda la vida de tu equipo.');
	define('_ORDEWHYS2','GARANTIZADO');
	define('_ORDEWHYS2TXT1','Te garantizamos atenci&oacute;n');
	define('_ORDEWHYS2TXT2','y calidad en todos nuestros');
	define('_ORDEWHYS2TXT3','servicios y productos.');
	define('_ORDEWHYS3','SOPORTE 24/7');
	define('_ORDEWHYS3TXT1','¿Tienes una pregunta?');
	define('_ORDEWHYS3TXT2','¡Contactanos!, nosotros');
	define('_ORDEWHYS3TXT3','podemos ayudarte.');
	//UNLOCK ORDER
	define('_ORDEPRO','¡PROMOCI&Oacute;N!');
	define('_ORDETIM', 'Completa tu orden y obten un descuento de');
	define('_ORDELEF','termina en:');
	define('_ORDEPRE','antes:');
	define('_ORDETAG','PRECIO EN PROMOCI&Oacute;N');
	define('_ORDEORDT1','TENEMOS LAS SIGUIENTES OPCIONES PARA DESBLOQUEAR TU EQUIPO');
	define('_ORDEORDT1S1','Selecciona tu m&eacute;todo de desbloqueo y opci&oacute;n de pago');
	define('_ORDEORDS1','Llena tus datos personales y de tu equipo');
	define('_ORDEORDFI1','Tu nombre');
	define('_ORDEORDFI2','Tu email');
	define('_ORDEORDFI2R','El email debe ser valido');
	define('_ORDEORDFI3','Confirma tu email');
	define('_ORDEORDFI3R','El email debe coincidir');
	define('_ORDEORDFI4','Tu n&uacute;mero celular');
	define('_ORDEORDFI5','N&uacute;mero IMEI de tu equipo');
	define('_ORDEORDFI5R','IMEI debe ser valido y de 15 digitos');
	define('_ORDEORDBTN1','Contin&uacute;a tu orden');
	define('_ORDEORDOPT1','Visa');
	define('_ORDEORDOPT2','PayPal');
	define('_ORDEORDOPT3','Efectivo');
	define('_ORDERMOD0','Tu IMEI');
	define('_ORDERMOD1','Pa&iacute;s');
	define('_ORDERMOD2','Operador');
	define('_ORDERMOD3','Servicio de Desbloqueo');
	define('_ORDERMOD4','Equipo celular');
	define('_ORDERMOD5','Costo');
	define('_ORDERMOD6','Selecciona tu m&eacute;todo de pago');
	define('_ORDERMOD7','Presiona "esc" para cancelar la orden');
	define('_ORDERMOD8','Cierra esta ventana');
	define('_ORDERMOD9','Tu n&uacute;mero de orden');
	define('_ORDERMOD9S1','con este n&uacute;mero puedes seguir tu orden in la secc&oacute;n del perfil de usuario, contacta a soporte t&eacute;cnico en caso de existir un problema. Te enviaremos otro correo electr&oacure;nico cuando tu pago sea procesado y y codigo este listo.');
	define('_ORDERMOD10','Guardalo para referencia futuras');
	define('_ORDERMOD11','TU PAGO NO HA SIDO PROCESADO, POR FAVOR CONTACTA SOPORTE DE TU METODO DE PAGO O AL DEL SITIO, SEGUN SEA NECESARIO
');
	define('_ORDERMOD12','TU PAGO ESTA SIENDO PROCESADO POR LA INSTITUCIÓN, POR FAVOR ESPERA EL TIEMPO REQUERIDO, NOS PONDREMOS EN CONTACTO CONTIGO EN CUANTO SEA PROCESADO');
	define('_ORDERMOD13','TU PAGO FUE REALIZADO CON EXITO, POR FAVOR ESPERA A QUE NOS CONTACTEMOS CONTIGO CON LA RESPUESTA AL SERVICIO PAGADO');
	define('_ORDERMOD14','HUBO UN ERROR Y EL PAGO ESTE PENDIENTE, POR FAVOR CONTACTA A SOPORTE');
	define('_ORDERBRET1','REGRESAR');
	define('_ORDERMOD15','PAGAR AHORA');
	define('_CUPONINP','CODIGO DE CUP&Oacute;N');
	define('_CUPONTXT','¿TIENES UN CUP&Oacute;N?');
	define('_ORDEEXP','Oferta expirada');
	//MAIL
	define('_MAIL01', 'Correo enviado');
	define('_MAIL02', 'Correo no enviado, contacta soporte');
	//REVISAIMEITEMPLATES
	//CHECK IMEI
	define('_CHECKT1','IMEI');
	define('_CHECKT2','LISTA NEGRA');
	define('_CHECKT3','REVISA TU IMEI');
	define('_CHECKT3S1','ASEGURA QUE TU EQUIPO NO CUENTE CON BLOQUEOS O REPORTES DE ROBO');
	define('_CHECKFI1','EL IMEI DE TU EQUIPO');
	define('_CHECKFI1R','EL IMEI DEBE SER DE 15 DIGITOS');
	define('_CHECKFI2','TU CORREO');
	define('_CHECKFI2R','EL EMAIL DEBE TENER FORMATO VALIDO');
	define('_CHECKSEL1','SERVICIOS IMEI');
	define('_CHECKSPA1','SERVICIO GRATUITO');
	define('_CHECKBTN1','REVISAR');
	define('_CHECKT4','¿DE QUE TRATA ESTE SERVICIO?');
	define('_CHECKT4S1','CONOCE POR EL EL IMEI ES TAN IMPORTANTE');
	define('_CHECKT4E1','CONOCES EL MODELO EXACTO DE TU EQUIPO');
	define('_CHECKT4E2','CONOCES EL AÑO DE FABRICACI&Oacute;N');
	define('_CHECKT4E3','CONOCES EL OPERADOR ORIGINAL POR EL QUE PUEDE ESTAR BLOQUEADO');
	define('_CHECKT4E4','CONOCE SI EL EQUIPO SE ENCUENTRA EN LISTADO NEGRO CON REPORTE DE ROBO, FALTA DE PAGO O DADO DE BAJA');
	define('_CHECKS1','CONOCE TU IMEI');
	define('_CHECKS1P1','Marca desde tu equipo:');
	define('_CHECKS1P2','*#06# para ver el IMEI');
	define('_CHECKS1P3','en la pantalla de tu equipo');
	define('_CHECKS2','REVISA');
	define('_CHECKS2P1','Completa el IMEI');
	define('_CHECKS2P2','y tu correo');
	define('_CHECKS2P3','en el formato');
	define('_CHECKS3','OBTEN');
	define('_CHECKS3P1','Verif&iacute;ca que tu IMEI');
	define('_CHECKS3P2','este libre y puedas');
	define('_CHECKS3P3','desbloquear tu equipo');
	define('_CHECKRES1','Te contactaremos en cuanto tu IMEI sea revisado y podamos enviarte el reporte');
	define('_CHECKMOD1', 'Confirma el pedido');
	//OPERATORS
	define('_CHECKOT2','OPERADOR ORIGINAL');
	define('_CHECKOT3','REVISA TU IMEI');
	define('_CHECKOT3S1','RECOMENDADO PARA EVALUAR EQUIPOS CUANDO SE COMPRA Y VENDE');
	define('_CHECKOFI1','TU IMEI');
	define('_CHECKOFI1R','IMEI DEBE SER DE 15 DIGITOS');
	define('_CHECKOFI2','TU EMAIL');
	define('_CHECKOFI2R','EMAIL DEBE SER DE FORMATO VALIDO');
	define('_CHECKOSPA1','SERVICIO GRATUITO');
	define('_CHECKOBTN1','REVISAR');
	define('_CHECKOT4','¿DE QUE TRATA ESTE SERVICIO?');
	define('_CHECKOT4S1','CONOCE LAS RAZONES POR LAS QUE EL IMEI ES IMPORTANTE');
	define('_CHECKOT4E1','PARA CONOCER EL OPERADOR ORIGINAL DE TU EQUIPO');
	define('_CHECKOT4E2','PARA VALUAR EL COSTO DE UN EQUIPO');
	define('_CHECKOT4E3','PUEDE INCLUIR INFORMACIÓN DEL MODELO, GARANTÍA, SOFTWARE, ETC');
	define('_CHECKOT4E4','ORIGEN DE LA INFORMACIÓN: BASE DE DATOS DEL FABRICANTE OFICIAL');
	//MODEL
	define('_CHECKMT1','DETALLE');
	define('_CHECKMT2','DEL MODELO');
	define('_CHECKMT3','REVISA TU IMEI');
	define('_CHECKMT3S1','CONOCE LOS DETALLES DE UN EQUIPO SOLO CON SU IMEI');
	define('_CHECKMFI1','EL IMEI DE TU EQUIPO');
	define('_CHECKMFI1R','IMEI DEBE SER DE 15 DIGITOS');
	define('_CHECKMFI2','TU EMAIL');
	define('_CHECKMFI2R','EMAIL DEBE SER DE FORMATO VALIDO');
	define('_CHECKMSPA1','SERVICIO GRATUITO');
	define('_CHECKMBTN1','REVISAR');
	define('_CHECKMT4','¿DE QUÉ TRATA ESTE SERVICIO?');
	define('_CHECKMT4S1','CONOCE LAS RAZONES POR LAS QUE EL IMEI ES IMPORTANTE');
	define('_CHECKMT4E1','CONFIRMAS EL TIPO DE TU EQUIPO PARA PODER SOLICITAR EL SERVICIO DE DESBLOQUEO');
	define('_CHECKMT4E2','ENCUENTRA EXACTAMENTE QUE MODELO ES SI ESTAS COMPRANDO UN EQUIPO USADO');
	define('_CHECKMT4E3','CONOCE LOS DETALLES DEL EQUIPO SI ESTAS BUSCANDO VENDERLO AL MEJOR PRECIO');
	define('_CHECKMT4E4','CONFIRMA QUE ES EL MODELO QUE QUIERES COMPRAR DE SEGUNDA MANO');
	//COUPONTEMPLATES
	//COUPON REVIEW
	define('_COUPT1', 'CUP&OacuteN');
	define('_COUPT11', 'REVISA');
	define('_COUPT2', 'INGRESA EL CODIGO DEL CUP&OacuteN EN EL FORMULARIO');
	define('_COUPT2S1', 'REVISA VIGENCIA Y DESCUENTO');
	define('_COUPFI1', 'LLENA CON EL CODIGO DEL CUP&Oacute;N');
	define('_COUPBTN1', 'REVISA');
	define('_COUPSTA1', 'EXPIRADO');
	define('_COUPSTA2', 'VALIDO');
	define('_COUPSTA3', 'NO-EXISTENTE');
	//PREGUNTAEXPERTOTEMPLATES
	//PREGUNTA A UN EXPERTO
	define('_ASKT1','PREGUNTA');
	define('_ASKT11','A UN EXPERTO');
	define('_ASKT2','¿TIENES UNA PREGUNTA? ¡TE PODEMOS AYUDAR!');
	define('_ASKT2S1','DESBLOQUE DE EQUIPOS, IMEI, MODELOS Y TODAS TUS PREGUNTAS');
	define('_ASKFI1','Buscar');
	define('_ASKANS1','RESPUESTAS');
	//CONTACTTEMPLATE
	//CONTACT FORM
	define('_CONTT1','FORMULARIO');
	define('_CONTT11','DE CONTACTO');
	define('_CONTT2','CONTACTANOS');
	define('_CONTT2S1','ENVIANOS TUS SOLICITUDES O PREGUNTAS DE SOPORTE POR ESTE MEDIO');
	define('_CONTFI1','Nombre Completo');
	define('_CONTFI2','Email');
	define('_CONTFI2R1','Formato de Email Valido');
	define('_CONTFI3','¿Cómo podemos ayudarte el día de hoy? Dudas, información adicional, soporte y más aquí. Escríbenos');
	define('_CONTBTN1','Enviar');
	define('_CONTRES1','Mensaje enviado');
	define('_CONTRES2','Nos pondremos en contacto contigo cuando tengamos una respuesta');
	define('_CONTRES3','');
	//CURRENCY
	define('_CURRTXT','Tipo de Cambio');
  //PAGOS
  define('_PAGOT1','M&Eacute;TODOS');
  define('_PAGOT11','DE PAGO');
  define('_PAGOT2','PAGOS MEDIANTE MOVILSIM');
  define('_PAGOT2S1','Conoce e identif&iacute;ca las formas de pago aceptadas en nuetra plataforma');
  define('_PAGOT3','');
  define('_PAGOT3S1','');
  define('_PAGOT3P1','');
  define('_PAGOT4','');
  define('_PAGOT4S1','');
  define('_PAGOT5','');
  define('_PAGOT5S1','');
  define('_PAGOT6','');
  define('_PAGOT6S1','');
  //COMO
  define('_COMOT1','¿COMO?');
  define('_COMOT11','FUNCIONA?');
  define('_COMOT2','EN QUE CONSISTE DESBLOQUAR TU EQUIPO');
  define('_COMOT2S1','TODO LO QUE NECESITAS SABER PARA HACER EL PROCESO DESDE CERO');
	define('_COMOT3','');
  define('_COMOT3S1','');
  define('_COMOT4','');
  define('_COMOT4S1','');
  define('_COMOT5','');
  define('_COMOT5S1','');
  define('_COMOT6','');
  define('_COMOT6S1','');
  define('_COMOT6P1','');
  //NOSOTROS
  define('_NOSOT1','ACERCA');
  define('_NOSOT11','DE NOSOTROS');
  define('_NOSOT2','TODO SOBRE MOVILSIM');
  define('_NOSOT2S1','CONOCENOS Y CONF&Iacute;A EN EL MEJOR SISTEMA DE DESBLOQUEO');
	define('_NOSOT3','');
  define('_NOSOT3S1','');
  define('_NOSOT4','');
  define('_NOSOT4S1','');
  define('_NOSOT5','');
  define('_NOSOT5S1','');
  define('_NOSOT6','');
  define('_NOSOT6S1','');
  define('_NOSOT6P1','');
?>
