<?php
	//HEADER HOME
	define('_TAGLANG', 'en');
	define('_HT1', 'Unlock your');
	define('_HT2', 'phone');
	define('_HT3', 'from wherever');
	define('_HT4', 'you are');
	//PROMOTION
	define('_PROMH1','UNLOCK THIS PROMOTION!');
	define('_PROMSP1','Only today you can get a discount of');
	define('_PROMSP2','In the next');
	define('_PROMSP3','minutes you\'ll get a discount on our unlock services');
	//MENU
	define('_MENU1','UNLOCK');
	define('_MENU2','CHECK IMEI');
	define('_MENU2S1','CHECK BLACKLIST');
	define('_MENU2S2','CHECK ORIGINAL OPERATOR');
	define('_MENU2S3','CHECK DEVICE MODEL');
	define('_MENU3','COUPONS');
	define('_MENU4','ASK AN EXPERT');
	define('_MENU5','CONTACT');
  define('_MENU6','PAYMENT METHODS');
  define('_MENU7','HOW DOES THIS WORKS?');
  define('_MENU8','ABOUT US');
	//FOOTERMENU
	define('_PAYT1','PAYMENT METHODS');
	define('_PAYT2','SECURE PAYMENTS');
	define('_FMENU1','FEATURED BRANDS');
	define('_FMENU2','POPULAR BRANDS');
	define('_FMENU3','NAVIGATION');
	define('_FMENU4','SUPPORT AND SOCIAL NETWORKS');
	//HOMETEMPLATES
	//OMNI SEARCH
	define('_IPH', 'SEARCH YOUR MODEL');
	//BRANDBOX
	define('_BRAT1', 'BRANDS');
	define('_BRASUB', 'SELECT THE BRAND OF YOUR EQUIPMENT');
	define('_BRAMORE', 'SHOW MORE');
	define('_BRALESS', 'SHOW LESS');
	//OUR PRINCIPLES
	define('_PRINT1', 'OUR PRINCIPLES');
	define('_PRINS1', 'WARRANTY');
	define('_PRINS1TXT1', 'Don\'t run risks');
	define('_PRINS1TXT2', 'with us, our service');
	define('_PRINS1TXT3', 'its guaranteed');
	define('_PRINS2', 'SPEED');
	define('_PRINS2TXT1', 'Get your code');
	define('_PRINS2TXT2', 'as soon as your payment');
	define('_PRINS2TXT3', 'is processed');
	define('_PRINS3', 'SAVINGS');
	define('_PRINS3TXT1', 'With our unbeatable');
	define('_PRINS3TXT2', 'prices, you get');
	define('_PRINS3TXT3', 'quality and improved service');
	define('_PRINS4', 'SUPPORT');
	define('_PRINS4TXT1', 'We have 24/7 chat');
	define('_PRINS4TXT2', 'and support,');
	define('_PRINS4TXT3', 'contact us!');
	//COMMENTS
	define('_COMMT1','COMMENTS FROM OUR CUSTOMERS');
	//PARAGRAPH
	define('_PARAT1','WHY UNLOCK YOUR EQUIPMENT WITH US?');
	define('_PARATXT1','MovilSIM started in 2016, we are a company dedicated to mobile phone unlock of wholesale and retail equipments. We strive to have the best prices in the market and the best response times, so that our customers get a good experience in all the MovilSIM services. All our services work 24 hours a day, 7 days a week automatically, this makes our processes fast and efficient, and best of all, without leaving home.');
	define('_PARATXT2','MovilSIM it\'s your best option!');
	//UNLOCKTEMPLATES
	//COMPLETE YOUR ORDERS
	define('_ORDET1','COMPLETE YOUR ORDER');
	define('_ORDES1','SELECT YOUR MODEL, COUNTRY, OPERATOR AND FILL YOUR PERSONAL DATA');
	//EQUIPOS
	define('_ORDEEQS1','Select your equipment model');
	define('_ORDEEQS2','Unlock your equipment');
	define('_ORDEEQS22','via IMEI');
	define('_ORDEEQTXT1','Secure unlocking that respects manufacturer\'s guarantee');
	define('_ORDEEQTXT2','Recommended method by');
	define('_ORDEEQTXT22','and operators');
	define('_ORDEEQTXT3','Unlock any');
	define('_ORDEEQTXT33','forever');
	define('_ORDEEQTXT4','Fast and Online, without leaving your equipment in service');
	define('_ORDEEQBTN1','Unlock your Equipment');
	//EQUIPOS SELECTS
	define('_ORDESES1','Select the country and the origin operator');
	define('_ORDESETXT1','Not the operator you want to migrate to');
	define('_ORDESESEL1','Select your country');
	define('_ORDESESEL2','Select your operator');
	define('_ORDESESEL3','Operators');
	define('_ORDESEBTN2','Next Step');
	define('_ORDERSESEH1','SERVICE NOT AVAILABLE');
	define('_ORDERSESEP1','If you leave your email we will notify you when we have the service for your equipment');
	define('_ORDERSESEI1','Email');
	define('_ORDERSESEB1','Notify me!');
	//KNOW YOUR DATA
	define('_ORDEKNOT1','KNOW YOUR EQUIPMENT INFORMATION');
	define('_ORDEKNOT1S1','FOLLOW THESE STEPS TO KNOW YOUR IMEI AND HOW TO UNLOCK YOUR PHONE');
	define('_ORDEKNOS1','GET YOUR EQUIPMENT IMEI');
	define('_ORDEKNOS1TXT1','DIAL *#06# IN YOUR EQUIPMENTO TO SHOW YOUR IMEI NUMBER ON SCREEN.');
	define('_ORDEKNOS1TXT2','IMEI NUMBER IT\'S A UNIQUE AND INSTRANSFERIBLE NUMBER THAT ITS USED TO REPORT AND LOCK STOLEN EQUIPMENTS.');
	define('_ORDEKNOS2','ONCE YOU HAVE YOUR IMEI NUMBER');
	define('_ORDEKNOS2TXT1','WITH A SIM CARD NOT ACCEPTED BY THE ORIGINAL OPERATOR OF YOUR EQUIPMENT, TURN IT ON AND WAIT FOR IT TO BEGIN.');
	define('_ORDEKNOS2TXT2','THE SCREEN WILL SHOW \"SIM CARD UNLOCK CODE\".');
	define('_ORDEKNOS2TXT3','ENTER THE CODE WE PROVIDE YOU AND PRESS UNLOCK.');
	define('_ORDEKNOS3','ENJOY YOUR');
	define('_ORDEKNOS33','UNLOCK EQUIPMENT!');
	//HOW TO UNLOCK A...
	define('_ORDEUNLT1','HOW TO UNLOCK A');
	define('_altORDEUNLIMG','How to unlock');
	//WHY UNLOCK WITH US
	define('_ORDEWHYT1','WHY UNLOCK WITH US?');
	define('_ORDEWHYT1S1','KNOW THE REASONS THAT DISTINGUISH US FROM OUR COMPETITION');
	define('_ORDEWHYS1','PERMANENT');
	define('_ORDEWHYS1TXT1','Unlocking with our');
	define('_ORDEWHYS1TXT2','system is maintained throughout');
	define('_ORDEWHYS1TXT3','the lifespan of your device.');
	define('_ORDEWHYS2','GUARANTEED');
	define('_ORDEWHYS2TXT1','We guarantee you top quality');
	define('_ORDEWHYS2TXT2',' attention and top quality in our');
	define('_ORDEWHYS2TXT3','service and products');
	define('_ORDEWHYS3','24/7 SUPPORT');
	define('_ORDEWHYS3TXT1','Do you have a question?');
	define('_ORDEWHYS3TXT2','Contact us, we can');
	define('_ORDEWHYS3TXT3','help answer it');
	//UNLOCK ORDER
	define('_ORDEPRO','PROMOTION!');
	define('_ORDETIM','Complete your order and get a discount of');
	define('_ORDELEF','ends in:');
	define('_ORDEPRE','before:');
	define('_ORDETAG','PROMOTION PRICE');
	define('_ORDEORDT1','WE HAVE THE FOLLOWING OPTIONS TO UNLOCK YOUR EQUIPMENT');
	define('_ORDEORDT1S1','Select your unlock method and payment option');
	define('_ORDEORDS1','Fill your personal and equipment data');
	define('_ORDEORDFI1','Your name');
	define('_ORDEORDFI2','Your email');
	define('_ORDEORDFI2R','Email must be valid');
	define('_ORDEORDFI3','Confirm your email');
	define('_ORDEORDFI3R','Email must coincide');
	define('_ORDEORDFI4','Your cellphone number');
	define('_ORDEORDFI5','IMEI number of your equipment');
	define('_ORDEORDFI5R','IMEI must be valid');
	define('_ORDEORDBTN1','Continue your Order');
	define('_ORDEORDOPT1','Visa');
	define('_ORDEORDOPT2','Paypal');
	define('_ORDEORDOPT3','Cash');
	define('_ORDERMOD0','Your IMEI');
	define('_ORDERMOD1','Country');
	define('_ORDERMOD2','Operator');
	define('_ORDERMOD3','Unlock Service');
	define('_ORDERMOD4','Equipment');
	define('_ORDERMOD5','Cost');
	define('_ORDERMOD6','Select your payment method');
	define('_ORDERMOD7','Dont close this window, you will be redirected at the end of the process');
	define('_ORDERMOD8','Close this window');
	define('_ORDERMOD9','Your order number');
	define('_ORDERMOD9S1','with this number you can track your order in the user profile section, contact technical support in case of any mishap. We will also send you another email when your payment it\'s proccesed and your code ready');
	define('_ORDERMOD10','Save it for future reference');
	define('_ORDERMOD11','YOUR PAYMENT HAS NOT BEEN PROCESSED, PLEASE CONTACT SUPPORT OF YOUR PAYMENT METHOD OR THAT OF THE SITE, AS REQUIRED');
	define('_ORDERMOD12','YOUR PAYMENT IS BEING PROCESSED BY THE INSTITUTION, PLEASE WAIT FOR THE REQUIRED TIME, WE WILL CONTACT YOU AS SOON AS IT IS PROCESSED');
	define('_ORDERMOD13','YOUR PAYMENT WAS MADE SUCCESSFULLY, PLEASE WAIT FOR US TO CONTACT YOU WITH THE RESPONSE TO THE PAID SERVICE');
	define('_ORDERMOD14','THERE WAS AN ERROR AND PAYMENT IT\'S PENDING, PLEASE CONTACT SUPPORT');
	define('_ORDERBRET1','RETURN');
	define('_ORDERMOD15','PAY NOW');
	define('_CUPONINP','COUPON CODE');
	define('_CUPONTXT','DO YOU HAVE A COUPON?');
	define('_ORDEEXP','Offer Expired');
	//MAIL
	define('_MAIL01', 'Mail sent');
	define('_MAIL02', 'Mail not sent, contact support');
	//REVISAIMEITEMPLATES
	//CHECK IMEI
	define('_CHECKT1','IMEI');
	define('_CHECKT2','BLACKLIST CHECK');
	define('_CHECKT3','CHECK YOUR IMEI');
	define('_CHECKT3S1','ENSURE THAT YOUR DEVICE IS FREE OF THEFT REPORTS AND BLOCKS');
	define('_CHECKFI1','YOUR DEVICE IMEI');
	define('_CHECKFI1R','IMEI MUST BE 15 DIGITS');
	define('_CHECKFI2','YOUR EMAIL');
	define('_CHECKFI2R','EMAIL MUST BE VALID FORMAT');
	define('_CHECKSEL1','SERVICIOS IMEI');
	define('_CHECKSPA1','FREE SERVICE');
	define('_CHECKBTN1','CHECK');
	define('_CHECKT4','WHAT IS THIS SERVICE ABOUT?');
	define('_CHECKT4S1','KNOW THE REASONS WHY IMEI IS IMPORTANT');
	define('_CHECKT4E1','TO KNOW THE EXACT MODEL OF YOUR DEVICE');
	define('_CHECKT4E2','GET THE MANUFACTURING YEAR');
	define('_CHECKT4E3','KNOW THE ORIGINAL OPERATOR WITH WHICH IT IS LOCKED');
	define('_CHECKT4E4','KNOW IF YOU ARE IN ANY BLACKLIST WITH THEFT REPORT, LACK OF PAYMENT OR LOW REPORT');
	define('_CHECKS1','KNOW YOUR IMEI');
	define('_CHECKS1P1','Dial from your device:');
	define('_CHECKS1P2','*#06# to see the IMEI');
	define('_CHECKS1P3','number on your device screen');
	define('_CHECKS2','CHECK');
	define('_CHECKS2P1','Fill your IMEI number');
	define('_CHECKS2P2','and your email');
	define('_CHECKS2P3','in the form above');
	define('_CHECKS3','GET');
	define('_CHECKS3P1','Verify that the IMEI');
	define('_CHECKS3P2','is clean and you can');
	define('_CHECKS3P3','unlock your device.');
	define('_CHECKRES1','We will contact you when the IMEI code is reviewed and we have an answer');
	define('_CHECKMOD1', 'Confirm your Request');
	//OPERATORS
	define('_CHECKOT2','ORIGINAL OPERATOR');
	define('_CHECKOT3','CHECK YOUR IMEI');
	define('_CHECKOT3S1','RECOMMENDED TO ASSES CELL PHONES WHEN BUYING AND SELLING');
	define('_CHECKOFI1','YOUR DEVICE IMEI');
	define('_CHECKOFI1R','IMEI MUST BE 15 DIGITS');
	define('_CHECKOFI2','YOUR EMAIL');
	define('_CHECKOFI2R','EMAIL MUST BE VALID FORMAT');
	define('_CHECKOSPA1','FREE SERVICE');
	define('_CHECKOBTN1','CHECK');
	define('_CHECKOT4','WHAT IS THIS SERVICE ABOUT?');
	define('_CHECKOT4S1','KNOW THE REASONS WHY IMEI IS IMPORTANT');
	define('_CHECKOT4E1','TO KNOW THE ORIGINAL OPERATOR OF YOUR DEVICE');
	define('_CHECKOT4E2','TO VALUE THE PURCHASE / SALE OF A CELL PHONE');
	define('_CHECKOT4E3','MAY INCLUDE MODEL INFORMATION, WARRANTY, SOFTWARE, ETC');
	define('_CHECKOT4E4','ORIGIN OF THE DATA: OFFICIAL MANUFACTURER\'S DATABASE');
	//MODEL
	define('_CHECKMT2','DETAILED MODEL');
	define('_CHECKMT3','CHECK YOUR IMEI');
	define('_CHECKMT3S1','KNOW THE DETAILS OF A CELL PHONE ONLY WITH IMEI');
	define('_CHECKMFI1','YOUR DEVICE IMEI');
	define('_CHECKMFI1R','IMEI MUST BE 15 DIGITS');
	define('_CHECKMFI2','YOUR EMAIL');
	define('_CHECKMFI2R','EMAIL MUST BE VALID FORMAT');
	define('_CHECKMSPA1','FREE SERVICE');
	define('_CHECKMBTN1','CHECK');
	define('_CHECKMT4','WHAT IS THIS SERVICE ABOUT?');
	define('_CHECKMT4S1','KNOW THE REASONS WHY IMEI IS IMPORTANT');
	define('_CHECKMT4E1','CONFIRM WHAT IS YOUR CELL PHONE TO REQUEST THE UNLOCK SERVICE');
	define('_CHECKMT4E2','FIND OUT THE EXACT MODEL IF YOU\'RE GOING TO BUY IT AS SECOND HAND');
	define('_CHECKMT4E3','KNOW THE DETAILS OF YOUR CELL PHONE IF YOU WANT TO SELL THEM AT THE BEST PRICE');
	define('_CHECKMT4E4','CONFIRM THAT IT IS THE CELL THAT YOU WANT TO BUY AS SECOND HAND');
	//COUPONTEMPLATES
	//COUPONREVIEW
	define('_COUPT1', 'COUPON');
	define('_COUPT11', 'REVIEW');
	define('_COUPT2', 'FILL THE FORM WITH YOUR COUPON CODE');
	define('_COUPT2S1', 'CHECK VIGENCY AND DISCOUNT AMOUNT');
	define('_COUPFI1', 'FILL YOUR COUPON CODE');
	define('_COUPBTN1', 'CHECK');
	define('_COUPSTA1', 'EXPIRED');
	define('_COUPSTA2', 'VALID');
	define('_COUPSTA3', 'NON-EXISTENT');
	//PREGUNTAEXPERTOTEMPLATES
	//PREGUNTA A UN EXPERTO
	define('_ASKT1','ASK AN');
	define('_ASKT11','EXPERT');
	define('_ASKT2','HAVE A QUESTION? WE CAN HELP!');
	define('_ASKT2S1','DEVICE UNLOCK, IMEI, MODELS AND ALL YOUR QUESTIONS');
	define('_ASKFI1','Search');
	define('_ASKANS1','ANSWER');
	//CONTACTTEMPLATE
	//CONTACT FORM
	define('_CONTT1','CONTACT');
	define('_CONTT11','FORM');
	define('_CONTT2','CONTACT US');
	define('_CONTT2S1','SEND US YOUR REQUESTS OR SUPPORT INQUIRIES BY THIS MEDIA');
	define('_CONTFI1','Full Name');
	define('_CONTFI2','Email');
	define('_CONTFI2R1','Valid Email Format');
	define('_CONTFI3','How can we help you today? Doubts, additional information, support and more here. Write us');
	define('_CONTBTN1','Send');
	define('_CONTRES1','Message sent');
	define('_CONTRES2','We will contact you when your message it\'s answered');
	define('_CONTRES3','');
	//CURRENCY
	define('_CURRTXT','CURRENCY');
  //PAGOS
  define('_PAGOT1','PAYMENT');
  define('_PAGOT11','OPTIONS');
  define('_PAGOT2','MOVILSIM PAY WAYS');
  define('_PAGOT2S1','Know and identify the payment methods accepted on our platform');
  define('_PAGOT3','');
  define('_PAGOT3S1','');
  define('_PAGOT3P1','');
  define('_PAGOT4','');
  define('_PAGOT4S1','');
  define('_PAGOT5','');
  define('_PAGOT5S1','');
  define('_PAGOT6','');
  define('_PAGOT6S1','');
  //COMO
  define('_COMOT1','HOW DOES');
  define('_COMOT11','IT WORKS?');
  define('_COMOT2','WHAT IS UNLOCKING YOUR EQUIPMENT?');
  define('_COMOT2S1','EVERYTHING YOU NEED TO KNOW TO BEGIN PROCESS FROM CERO');
  define('_COMOT3','');
  define('_COMOT3S1','');
  define('_COMOT4','');
  define('_COMOT4S1','');
  define('_COMOT5','');
  define('_COMOT5S1','');
  define('_COMOT6','');
  define('_COMOT6S1','');
  define('_COMOT6P1','');
  //NOSOTROS
  define('_NOSOT1','ABOUT');
  define('_NOSOT11','US');
  define('_NOSOT2','AVERYTHING ABOUT MOVILSIM');
  define('_NOSOT2S1','KNOW US AND TRUST THE BEST UNLOCK SYSTEM');
  define('_NOSOT3','');
  define('_NOSOT3S1','');
  define('_NOSOT4','');
  define('_NOSOT4S1','');
  define('_NOSOT5','');
  define('_NOSOT5S1','');
  define('_NOSOT6','');
  define('_NOSOT6S1','');
  define('_NOSOT6P1','');
?>
