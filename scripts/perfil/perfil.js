//PLANTILLAS CAJAS DE MENSAJES
var boxMensajes = '<div class="box is-radiusless single-item"><div class="content">';
var end = '</div></div>';

// instanciate new modal
var modal = new tingle.modal({
  footer: true,
  stickyFooter: false,
  closeMethods: ['overlay', 'button', 'escape'],
  closeLabel: "Close",
  cssClass: ['custom-class-1', 'custom-class-2'],
  onOpen: function() {
  },
  onClose: function() {
  },
  beforeClose: function() {
      return true;
  }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--primary', function() {
  modal.close();
});

$(document).ready(function(){
  //CHECK SESSION
  cargaUsuario();
});

$('.revisaPerfil').on( 'click' , function () {
  var mail = $('#mail').val();
  var order = $('#order').val();
  //SOLICITAMOS LAS RESPUESTAS
  $.ajax({
    method: 'post',
    url: 'php/perfil/revisa-perfil.php',
    data: {
      mail : mail,
      order : order
    },
    context: document.body,
    success: function(data) {
      location.reload();
    },
    error: function(xhr, tst, err) {
      console.log(err)
    }
  })
});

$('.exitSession').on( 'click' , function () {
  //SOLICITAMOS LAS RESPUESTAS
  $.ajax({
    method: 'post',
    url: 'php/perfil/cierra-perfil.php',
    context: document.body,
    success: function(data) {
      location.reload();
    },
    error: function(xhr, tst, err) {
      console.log(err)
    }
  })
});

function cargaUsuario() {
  //SOLICITAMOS LAS RESPUESTAS
  $.ajax({
    method: 'post',
    url: 'php/perfil/carga-perfil.php',
    contentType: "application/json",
    context: document.body,
    success: function(result) {
      //CARGAMOS TABLA
      var table = $('#table').DataTable({
        data: result,
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'print'
        ],
        //ROW CONDITIONS
        'rowCallback': function(row, data, index){
          if(data[7] != 0){
            $(row).find('td:eq(7)').html('<i class="far fa-thumbs-up has-text-link"></i>');
          } else {
            $(row).find('td:eq(7)').html('<i class="far fa-thumbs-down has-text-danger"></i>');
          }
        }
      });
      //EVENTOS DE CLICK
      $('#table tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();

        var modalContent = "<img class='img-center pa-one' src='img/logo_500.png'>";
        modalContent += '<div class="columns has-text-white contentColumn">';
        modalContent += '<div class="column">';
        modalContent += '<p><i><em>Folio: </em></i><i class="is-pulled-right"><b id="folioNo">' + data[0] + '</b></i></p>';
        modalContent += '<p><i><em>Date: </em></i><i class="is-pulled-right"><b>' + data[1] + '</b></i></p>';
        modalContent += '<p><i><em>Order No.: </em></i><i class="is-pulled-right"><b id="orderNo">' + data[2] + '</b></i></p>';
        modalContent += '<p><i><em>Company: </em></i><i class="is-pulled-right"><b>' + data[4] + '</b></i></p>';
        modalContent += '<p><i><em>Model: </em></i><i class="is-pulled-right"><b>' + data[5] + '</b></i></p>';
        modalContent += '<p><i><em>IMEI: </em></i><i class="is-pulled-right"><b>' + data[3] + '</b></i></p>';
        modalContent += '</div>';
        modalContent += '<div class="column">';
        modalContent += '<p><i><em>Cost: </em></i><i class="is-pulled-right"><b>$&nbsp;' + data[6] + '</b></i></p>';
        //STATUS Y COLOR DEL ESTATUS
        if (data[7] == 0) {var icon = '<i class="far fa-thumbs-down has-text-danger is-pulled-right"></i>'; var button ='';} else if (data[7] == 1) { var icon = '<i class="far fa-thumbs-up has-text-white is-pulled-right"></i>'; var button = '<button class="button is-success ma-oneinput-format border-c-no shadow-blue-darker msgButton" val=' + data[2] + ' fol=' + data[0] + ' msg="' + data[11] + '">Leave Feedback&nbsp;<i class="far fa-comment-dots"></i></button>';}
        modalContent += '<p><i><em>Estatus: </em></i>' + icon + '</i></p>';
        modalContent += '<p><i><em>Ticket MP (if any): </em></i><i class="is-pulled-right"><b><a class="has-text-white ref-text-white" href="' + data[10] + '" target=_blank><i class="fas fa-print"></i></a></b></i></p>';
        modalContent += '<p><i><em>Service Info: </em></i><i class="is-pulled-right"><b>' + data[9] + '</b></i></p>';
        modalContent += '<p><i><em>Unlock Code: </em></i><i class="is-pulled-right"><b>' + data[8] + '</b></i></p>';
        modalContent += '<p class="is-pulled-right">' + button + '</p>';
        modalContent += '</div>';
        modalContent += '</div>';

        //SET MODAL
        modal.setContent(modalContent);
        $('.tingle-modal-box__content').addClass('background-p-1');
        modal.open();

        $('.msgButton').on('click', function(){
          var ordernumber = $(this).attr('val');
          var folnumber = $(this).attr('fol');
          var msgold = $(this).attr('msg');
          var newContent = '<div class="column">';
          newContent += '<p><textarea class="textarea input-format border-c-no shadow-blue-darker textMessage" placeholder="Your message here">' + msgold +'</textarea></p>';
          newContent += '<p class="is-pulled-right"><button class="button is-success ma-one input-format border-c-no shadow-blue-darker saveButton">Save Message&nbsp;<i class="far fa-paper-plane"></i></button></p>';
          newContent += '<p><small>This may appear on the front page of this site</small></p>';
          newContent += '</div>';
          $('.contentColumn').html(newContent);

          //ENVIA EL FEEDBACK
          $('.saveButton').on('click', function () {
            var message = $('.textMessage').val();

            $.ajax({
              type: 'POST',
              url: 'php/perfil/envia-feedback.php',
              data: ({
                folio: folnumber,
                order: ordernumber,
                message: message
              }),
              context: document.body,
              success: function(data) {
                $('.contentColumn').html(data);
              },
              error: function(xhr, tst, err) {
                console.log(err);
              }
            })
          })
        })
      })
    },
    error: function(xhr, tst, err) {
      console.log(err)
    }
  });
};

//CREAR PREGUNTAS
//EVENTOS DE CLICK
$('.createMessage').on('click', function () {
  var modalContent = "<img class='image img-center pa-one' src='img/logo_500.png'>";
  modalContent += '<div class="columns has-text-white formQuestions">';
  modalContent += '<div class="column">';
  modalContent += '<div class="field">';
  modalContent += '<div class="control">';
  modalContent += '<h5>Display name:</h5>';
  modalContent += '<input class="input input-format border-c-no shadow-blue-darker" type="text" id="name">';
  modalContent += '</div>';
  modalContent += '</div>';
  modalContent += '<div class="field">';
  modalContent += '<div class="control">';
  modalContent += '<h5>Your question to the experts:</h5>';
  modalContent += '<textarea class="textarea is-small input-format border-c-no shadow-blue-darker" id="messa"></textarea>';
  modalContent += '</div>';
  modalContent += '</div>';
  modalContent += '<div class="content">';
  modalContent += '<a class="button input-format border-c-no shadow-blue-darker sendMessage">Send</a>';
  modalContent += '</div>';
  modalContent += '</div>';
  modalContent += '</div>';
  //SET MODAL
  modal.setContent(modalContent);
  $('.tingle-modal-box__content').addClass('background-p-1');
  modal.open();
  //ENVIA EL MENSAJE
  $('.sendMessage').on('click', function () {
    var nombre = $('#name').val();
    var message = $('#messa').val();
    $.ajax({
      type: 'POST',
      url: 'php/perfil/envia-mensaje.php',
      data: ({
        nombre: nombre,
        message: message
      }),
      context: document.body,
      success: function(data) {
        $('.formQuestions').html(data);
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
  })
})
