$(document).ready(function(){

	cargaContent();

	//REVISAR ORDER
	$('.checkButton').on('click',function(){
		$.getScript( 'scripts/order/order.js', function() {
		})
	})

	//NAVBAR VISUALIZACION MOVIL
	$(".navbar-burger").click(function() {
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
	});

	$(".navbar-dropdown").hover(function() {
    $(this).siblings("a.navbar-link").addClass("has-text-black");
	}, function() {
    $(this).siblings("a.navbar-link").removeClass("has-text-black");
	});

	cargaMenu();
  cargaMarcasFooter();
  cargaPopFooter();

});

function cargaContent(e) {

	//COMENTARIO AL AZAR
	$.ajax({
		url: 'php/home/carga-comments.php',
		contentType: "application/json",
    context: document.body,
    success: function(data) {
    	$('#commentsName').html(data[0]);
    	$('#commentsPhone').html(data[1]);
    	$('#commentsDesc').html(data[2]);
    	$('#commentsDate').html(data[3]);
    	$('#commentsName2').html(data[4]);
    	$('#commentsPhone2').html(data[5]);
    	$('#commentsDesc2').html(data[6]);
    	$('#commentsDate2').html(data[7]);
    },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});

}

//CARGA FOOTER
function cargaMarcasFooter(e) {
  $.ajax({
    type: "POST",
    url: "php/home/carga-marcas.php",
    dataType: "json",
    context: document.body,
    success: function(data) {
      $.each(data, function(index, home) {
        if (home.portada == 1) {
          $('#menuFooter').append('<li class="marcaFooter handed" val=' + home.id + ' mar="' + home.name + '"><a class="has-text-grey-light">' + home.name + '</a></li>');
        }
      });
      //CARGA LA MARCA SELECCIONADA
      $('.marcaFooter').on('click',function(){
        var val = $(this).attr('val');
        var marca = $(this).attr('mar');
        //cargaEquipos(val, marca);
        cargaImei(val, marca);
      })
			setTimeout(function(){ cargaPromo() }, 5000);
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });

}

//CARGA FOOTER
function cargaPopFooter(e) {
  $.ajax({
    type: "POST",
    url: "php/home/carga-pop.php",
    dataType: "json",
    context: document.body,
    success: function(data) {
      $.each(data, function(index, home) {
        $('#menuPop').append('<li class="menuPop handed" mar=' + home.marca + '><a class="has-text-grey-light">' + home.marca + '</a></li>');
      });
      //CARGA LA MARCA SELECCIONADA
      $('.menuPop').on('click',function(){
        var val = $(this).attr('val');
        var marca = $(this).attr('mar');
        //cargaEquipos(val, marca);
        cargaImei(val, marca);
      })
    },

    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
}

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
function cargaImei(id, mar) {
  var id = id;
  var marca = mar;

  var url = 'desbloquear.php';
  var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
  '<input type="text" name="paso" value=1 />' +
  '<input type="text" name="id" value="' + id + '" />' +
  '<input type="text" name="marca" value="' + marca + '" />' +
  '</form>');
  $('body').append(form);
  form.submit();
}

//MENU DESBLOQUEOS
function cargaMenu(e) {
  //NUMERO DE COMENTARIOS
  $.ajax({
    url: 'php/home/carga-marcas.php',
    contentType: "application/json",
    context: document.body,
    success: function(data) {
      $('#menuTotal').html();
      $.each(data, function(index, home) {
        if (home.portada == 1) {
          $('#desbloqueaMarcasMenu').append('<a class="navbar-item marcaNameMenu" val=' + home.id + ' mar=' + home.name + '>' + home.name + '</a>');
        }
      });
      $('#desbloqueaMarcasMenu').append('<hr class="navbar-divider">');
      $('#desbloqueaMarcasMenu').append('<a class="navbar-item" href="">More brands&nbsp;></a>');
      //CARGA LA MARCA SELECCIONADA
      $('.marcaNameMenu').on('click',function(){
        var val = $(this).attr('val');
        var marca = $(this).attr('mar');
        //cargaEquipos(val, marca);
        cargaImei(val, marca);
      })
    },
    error: function(xhr, tst, err) {
			console.log(err);
    }
  });
}

//SOLICITAMOS CADENAS TRADUCIDAS
function translateString(e) {
  var str = e
  $.ajax({
    method: 'post',
    url: 'locale/translate.php',
    data: {str: str},
    dataType: "html",
    async: false,
    context: document.body,
    success: function(data) {
        str = data
    },
    error: function(xhr, tst, err) {
        console.log(err);
    }
  });
  return str;
}

//PROMOCIONES
function cargaPromo(e) {
	//REVISA SI LA PROMOCIÓN NO ESTA activate
	if (localStorage.getItem('time') == 0 || !localStorage.getItem('time')) {
		var duracion;
		$.ajax({
			method: 'post',
			url: 'php/desbloquea/lee-bundle.php',
			dataType: "json",
			context: document.body,
			success: function(data) {
				$.each(data, function(key,val){
					//CARGA MODAL CON BANNER DE PROMOCIONE
					var content = '<div class="box background-promo radius-no">';
						content += '<h1 class="has-text-centered is-size-3 fo-w-b">' + translateString('_PROMH1') + '</h1>';
						content += '<h2 class="has-text-centered"><span class="is-size3-4 fo-w-b">' + translateString('_PROMSP1') + '&nbsp;' + val.discount + '%</span></h2>';
						content += '<p class="has-text-centered">' + translateString('_PROMSP2') + '&nbsp;' + val.duracion + '&nbsp;<span>' + translateString('_PROMSP3') + '</span></p>';
						content += '<button class="button is-danger is-medium ma-t-one activate-promo">Obtener Promoción</h1>';
					content += '</div>';
					carga_modal(content);
					$('.activate-promo').on('click', function(e) {
						$('.modal').remove();
						//TIMER
						duracion = val.duracion;
						var then = new Date();
						localStorage.setItem('time', (then.setMinutes( then.getMinutes() + duracion )));
						timer();
					})
				})
			},
			error: function(xhr, tst, err) {
				console.log(err);
			}
		});
	} else {
		timer();
	}
}
//TIMER
function timer(e) {
  $.ajax({
    method: 'post',
    url: 'php/desbloquea/lee-bundle.php',
    dataType: "json",
    context: document.body,
    success: function(data) {
      $.each(data, function(key,val){
        discount = val.discount
				$('#timer-tag').remove();
        var content = '<!--TIMER-->';
        content += '<div class="has-text-white is-hidden timer-tag">';
        content += '<span><b class="fo-w-b">' + translateString('_ORDEPRO') + '</b>&nbsp;' + translateString('_ORDETIM') + '&nbsp;' + discount + '%</span>';
        content += '&nbsp;' + translateString('_ORDELEF') + '&nbsp;<span id="timer-tag" class="fo-w-b">0</span>';
        content += '</div>';
        $('body').prepend(content);
        $('#timer-tag').parent().removeClass('is-hidden');
        // Set the date we're counting down to
        var then = localStorage.getItem('time');
        // Update the count down every 1 second
        var x = setInterval(function() {
          var now = new Date().getTime();
          var distance = then - now;
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          document.getElementById("timer-tag").innerHTML = minutes + "m " + seconds + "s ";
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("timer-tag").innerHTML = translateString('_ORDEEXP');
            localStorage.setItem('time', 0);
          }
        }, 1000);
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
}
//CREA UN MODAL PARA TODOS Y TODO
window.carga_modal = function(incoming) {
	//DESTRUYE VIEJOS MODALES
	if ($('.modal').length) {
		$('.modal').remove();
	}
	//CREA UN MODAL NUEVO
	var content = incoming;
	var modal = '<div class="modal">';
	modal += '<div class="modal-background"></div>';
	modal += '<div class="modal-content">';
	modal += content;
	modal += '</div>';
	modal += '<button class="modal-close is-large" aria-label="close"></button>';
	modal += '</div>';
	$('body').prepend(modal);
	$('.modal').css('z-index','9000');
	$('.modal').toggleClass("is-active");
	$('.modal-close').on('click', function(){
		$('.modal').toggleClass("is-active");
	});
	$('.modal-background').on('click', function(){
		$('.modal').toggleClass("is-active");
	})
}
