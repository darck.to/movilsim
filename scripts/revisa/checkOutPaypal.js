//SCRIPT DE PAYPAL PARA EL BOTON Y PAGOS
var form_data = new FormData();

form_data.append("imei", imei);
form_data.append("email", email);
//CAMBIO DE CURRENCY A DOLARES SI COIN NO ES DOLAR
form_data.append("price", price);
form_data.append("coin", coin);

paypal.Button.render({
env: 'production',
style: {
	layout: 'horizontal',
  shape: 'rect',
  size: 'responsive',
  tagline: 'false',
  height: 45
},
locale: 'en_US',
client: {
production: 'AfTNcysU0h84ZpFXozs32EOtbSkrMMzPbIDWYUVDo1LBQ5xPEwW4JMcMhsmzr0AF2d6N39xkLGa3K4Tf'
},
payment: function (data, actions) {

	return actions.payment.create({
	  transactions: [{
	    amount: {
	      total: '' + price +'',
	      currency: coin
	    },
	    description: 'MovilSIM Unlock Services',
	    custom: '9004863002',
	    //invoice_number: '12345', Insert a unique invoice number
	  }]
	});

},

onAuthorize: function (data, actions) {
	return actions.payment.execute()
	  .then(function () {
	    cargaCompra();
	  });
	}
}, '#paypal-button');


//CARGA LA COMPRA PARA EL USUARIO
function cargaCompra(e) {
	form_data.append("auth", 1);
	//SOLICITAMOS EL CODIGO
	$.ajax({
    method: 'post',
    url: 'php/revisa/revisa-order.php',
    data: form_data,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "html",
    context: document.body,
    success: function(data) {
      modal.close();
      $('#orderDiv').html('').delay(2000);
      $('#orderDiv').addClass('pa-one');
      $('#orderDiv').append('<h3 class="has-text-centered has-text-weight-light is-size-2">' + translateString('_ORDERMOD9') + ': <b class="font-color-1">' + data + '</b></h3>');
      $('#orderDiv').append('<h6 class="has-text-centered has-text-weight-light is-size-6">' + translateString('_ORDERMOD9S1') + '</h6>');
      $('#orderDiv').append('<h4 class="has-text-centered has-text-weight-light is-size-3">' + translateString('_ORDERMOD10') + '</h4>');
      $('#orderDiv').removeClass('is-hidden');
      $('.order-background').removeClass('is-hidden');
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
	});
}
