//INICIALIZAMOS
var src, imei, email, value, price, payOption, dollar, euro, peso;
var coin = "MXN";

// instanciate new modal
var modal = new tingle.modal({
  footer: true,
  stickyFooter: false,
  closeMethods: [],
  closeMethods: [],
  cssClass: ['custom-class-1', 'custom-class-2'],
  onOpen: function() {
    },
  onClose: function() {
    },
  beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn(translateString('_CHECKMOD1'), 'tingle-btn tingle-btn--primary', function() {
  $('.tingle-btn tingle-btn--primary').prop('disabled', true );
  $('.tingle-btn tingle-btn--primary').addClass('button is-loading');
  //PROCESA PAGO
  var form_data = new FormData();
  form_data.append("src", src);
  form_data.append("imei", imei);
  form_data.append("email", email);
  form_data.append("value", value);
  form_data.append("price", price);
  //SOLICITAMOS EL CODIGO
  $.ajax({
    method: 'post',
    url: 'php/revisa/carga-imei.php',
    data: form_data,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "html",
    context: document.body,
    success: function(data) {
      modal.close();
      $('#orderDiv').html('').delay(2000);
      $('#orderDiv').addClass('is-hidden');
      $('#orderDiv').append('<h4 class="has-text-centered has-text-white is-size-3 has-text-weight-light">' + translateString('_ORDERMOD9') + ': <b class="has-text-weight-bold">' + data + '</b></h4>');
      $('#orderDiv').append("<img class='img-center pa-one' src='img/logo_500.png'>");
      $('#orderDiv').append('<h5 class="has-text-centered has-text-white is-size-4">' + translateString('_ORDERMOD10') + '</h5>');
      $('#orderDiv').removeClass('is-hidden');
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
});

$(document).ready(function(){
  cargaServicios($('#source').attr('val'));
  currency();
});

function currency(e) {
  $.ajax({

    method: 'get',
    url: 'admin/apo/carga-currency.php',
    dataType: "json",
    context: document.body,
    success: function(data) {

      $.each(data, function(key, content) {
        dollar = content.USD;
        euro = content.EUR;
        return false
      });

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }

  });
}

//CARGA LA ORDEN PARA PAGO
$('.button-carga-imei').on('click',function(){
	src = $("#srcForm").val();
  imei = $("#imeiForm").val();
	mail = $("#emailImeiForm").html();
  value = $("#result").val();
  price = $('option:selected', '#result').attr('price');
	var text = "<img class='img-center pa-one' src='img/logo_500.png'>";
	text += "<div class='section'></div>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Email: <i class='has-text-white has-text-weight-bold is-marginless'>" + email + "</i></h5>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>IMEI: <i class='has-text-white has-text-weight-bold is-marginless is-size-4'>" + imei + "</i></h5>";
    text += "<p class='has-text-white has-text-weight-light is-marginless'>" + translateString('_CHECKRES1') + "</p>";
	// set content
	modal.setContent(text);
  $('.tingle-modal-box__content').addClass('background-p-1');
  creadorOrdenes(price);
	//modal.open();
})

//CHECADORES DE INPUTS
//IMEI
$('#imeiForm').on('keyup', function() {
  if (this.value.length < 15 || this.value.length > 15) {
    $(this).css('background-color','rgba(255,0,0,0.3)');
    $('.imeiCheck').removeClass('is-hidden');
    $('#emailImeiForm').addClass('is-hidden');
  } else {
    $(this).css('background-color','rgba(255,0,0,0)');
    $(this).css('border-bottom','1px solid #9e9e9e');
    $('.imeiCheck').addClass('is-hidden');
    $('#emailImeiForm').removeClass('is-hidden');
  }

  if (this.value.length == 0) {
    $(this).css('background-color','rgba(255,0,0,0)');
    $(this).css('border-bottom','1px solid #9e9e9e');
    $('.imeiCheck').addClass('is-hidden');
    $('#emailImeiForm').addClass('is-hidden');
  }
});

//EMAIL
$('#emailImeiForm').on('keyup', function() {
  email = $(this).val();
  if (validateEmail(email)) {
    $('.emailCheck').addClass('is-hidden');
    $('#buttonPayOptions').removeClass('is-hidden')
  } else {
    $('.emailCheck').removeClass('is-hidden');
    $('#buttonPayOptions').addClass('is-hidden')
  }

  if (this.value.length == 0) {
    $('.emailCheck').addClass('is-hidden');
    $('#buttonPayOptions').addClass('is-hidden')
  }
});

//inputSelectForm
$('#inputSelectForm').on('change', function() {
  var value = $('option:selected', this).attr('price');
  if(value > 0){
    $('#imeiForm').removeClass('is-hidden');
  } else {
    $('#imeiForm').addClass('is-hidden');
    $('#emailImeiForm').addClass('is-hidden');
    $('#buttonPayOptions').addClass('is-hidden');
    $('#boxPayButton').addClass('is-hidden');
    $('.precio').html('');
  }
});


function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}

function cargaServicios(e) {
	$.ajax({
		type: 'post',
		url: 'php/revisa/orders-service-config.php',
		data: {
		index: e
		},
		dataType: "json",
		context: document.body,
		success: function(data) {
			$.each(data, function (name, value) {
				$('#result').append('<option value="' + value.value + '" price=' + value.price + '>' + value.name + '</option>');
			})

      $('#result').on('click', function(i) {
        $('.precio').html('$' + $('option:selected', this).attr('price') + '.00')
      })
		},
		error: function(xhr, tst, err) {
				console.log(err);
		}
	});
}

//CHECADORES DE PAYOPTION
$('.desbloquea-option').change(function() {
  payOption = $('input[name="payoption"]:checked').val();
  $('.disableButton').removeClass('is-hidden');
  $('.disableButton').removeClass('active-hide');
  $('.disableButton').addClass('active-show');
  //HABILITA EL BOTON DE PAGO
  $('#boxPayButton').removeClass('is-hidden');
});

//LAST STEP
function creadorOrdenes(e) {

  var pay = "";
  var icons = "";
  var price = e;
  //ELIGE ENTRE LAS OPCIONES DE PAGO
  if (payOption == 1) {
    //CARD//PAYPAL BUTTON & MEARCADOPAGO BUTTON
    icons += "<img class='desbloquea-icons' src='img/ico/2.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/3.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/4.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/5.png'>";
    pay += "<div id='paypal-button' class='width-one pago-pay'></div>";
    pay += "</div>";
    pay += "<div class='tile is-parent radius-5'>";
    pay += "<a class='width-one pago-mep' target='_blank'>" + translateString('_ORDERMOD15'); + "</a>";
  } else if (payOption == 2) {
    //PAYPAL//PAYPAL BUTTON
    icons += "<img class='desbloquea-icons' src='img/ico/1.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/2.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/3.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/4.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/5.png'>";
    pay += "<div id='paypal-button' class='width-one pago-pay'></div>";
  } else if (payOption == 3) {
    //CASH//MEARCADOPAGO BUTTON
    icons += "<img class='desbloquea-icons' src='img/ico/6.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/7.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/8.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/9.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/10.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/11.png'>";
    pay += "<a class='width-one pago-mep' target='_blank'>" + translateString('_ORDERMOD15'); + "</a>";
  }

  //DESABILITAMOS EL TIPO DE REVISION
  $('.paymentCheck').addClass('is-hidden');

  //ASIGNAMOS VARIABLES
  imei = $("#imeiForm").val();
  email = $("#emailImeiForm").val();

  //INPUTS REMOVED FOR SEC
  $('#inputFormOrder').addClass('is-hidden');
  $('#buttonPayOptions').addClass('is-hidden');
  $("input").prop('disabled', true);

  var text = "<img class='img-center pa-no' src='img/logo_500.png'>";
  text += "<div id='infoPayContent'>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDEORDFI2') + ": <i class='has-text-white has-text-weight-bold is-marginless'>" + email + "</i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDERMOD0') + ": <i class='has-text-white has-text-weight-bold is-marginless is-size-4'>" + imei + "</i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-right'>" + translateString('_ORDERMOD5') + ": <b class='has-text-weight-semibold is-size-3'>$" + price + "</b>" + coin + "</h5>";

    //AGREGA CHECKS DE PAYPAL, MERCADOPAGO Y EFECTIVO
    text += "<div class='content has-text-centered pa-one orderButtons'>";
      text += "<h5 class='is-size-5 has-text-white has-text-weight-light'>" + translateString('_ORDERMOD6') + "</h5>";
      text += "<h5 class='is-size-7 has-text-white has-text-weight-light is-hidden-tablet'>" + translateString('_ORDERMOD5') + ": <b class='has-text-weight-semibold is-size-6'>$" + price + "</b>" + coin + "</h5>";
      text += "<div class='columns'>";
        text += "<div class='column is-12 has-text-centered'>";
        text += icons;
        text += "</div>";
      text += "</div>";
      text += "<div class='tile is-ancestor'>";
        //PAY BUTTON
        text += "<div class='tile is-parent radius-5'>";
          text += pay;
        text += "</div'>";
      text += "</div>";
    text += "</div>";
  text += "</div>";
  text += "</div>";

  $('#payOptions').html(text);
  $('#payOptions').animate({ scrollTop: 0 }, 'fast');

  //CARGAMOS PAYPAL
  jQuery.getScript("scripts/revisa/checkOutPaypal.js", function(data, status, jqxhr) {
  });

  $('.pago-mep').on('click',function(){

    //PROCESA PAGO EFECTIVO
    var form_data = new FormData();

    form_data.append("imei", imei);
    form_data.append("email", email);
    form_data.append("price", price);
    form_data.append("value", value);
    form_data.append("coin", coin);
    form_data.append("src", src);
    //SOLICITAMOS EL CODIGO
    $.ajax({
      method: 'post',
      url: 'php/revisa/revisa-order.php',
      data: form_data,
      cache: false,
      async: false,
      contentType: false,
      processData: false,
      dataType: "html",
      context: document.body,
      success: function(result) {
        $('#orderDiv').html('').delay(2000);
        $('#orderDiv').addClass('pa-one');
        $('#orderDiv').append('<h3 class="has-text-centered has-text-weight-light is-size-2">' + translateString('_ORDERMOD9') + ': <b class="font-color-1">' + result + '</b></h3>');
        $('#orderDiv').append('<h6 class="has-text-centered has-text-weight-light is-size-6">' + translateString('_ORDERMOD9S1') + '</h6>');
        $('#orderDiv').append('<h4 class="has-text-centered has-text-weight-light is-size-3">' + translateString('_ORDERMOD10') + '</h4>');
        $('#orderDiv').removeClass('is-hidden');
        $('.order-background').removeClass('is-hidden');
        //VARIABLE idVenta
        var randomID = $('.idVenta').html();
        var folioID = $('.idVenta').attr('folio');
        mercadopagoButton(randomID,folioID);
        //CARGAMOS MERCADOPAGOBUTTON
        function mercadopagoButton(e,f) {
          //PROCESA PAGO EFECTIVO
          var form_data = new FormData();
          form_data.append("nombre", name);
          form_data.append("email", email);
          form_data.append("coin", coin);
          //CAMBIO DE CURRENCY A DOLARES SI COIN NO ES DOLAR
          if (coin == "USD") {
            price = price;
          } else if (coin == "MXN") {
            price = (price * dollar);
            price = price;
          } else if (coin == "EUR") {
            price = (price / euro) * dollar;
            price = price;
          }
          form_data.append("precio", price);
          form_data.append("random", e);
          form_data.append("folio", f);
          $.ajax({
            method: 'post',
            url: 'php/revisa/mercadopago-checkout.php',
            data: form_data,
            cache: false,
            async: false,
            contentType: false,
            processData: false,
            dataType: "html",
            context: document.body,
            success: function(data) {
              //$('.pago-mep').attr('href',data);
              $('.orderButtons').delay(1000).remove();
              //window.open(data,'_blank');
              var mpFrame = '<h6 class="background-p-1"><img class="img-center pa-one" src="img/logo_500.png" style="height:100px"></h6>';
              mpFrame += '<h6 class="has-text-weight-light is-size-7 pa-fiv background-p-1">' + translateString('_ORDERMOD7') + '</h6>';
              mpFrame += '<iframe class="iframeMP" src="' + data + '"></iframe>';
              modal.setContent(mpFrame);
              modal.open();
              $('.tingle-modal-box__content').css('background','#eee');
              $('.tingle-modal-box__content').css('height','80vh');
            },
            error: function(xhr, tst, err) {
              console.log(err);
            }
          });
        };
      },
      error: function(xhr, tst, err) {
          console.log(err);
      }
    });
  })
}
