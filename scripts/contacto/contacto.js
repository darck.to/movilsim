$(document).ready(function(){

});

$('.enviarContacto').on('click',function(){

    var nombre = $('#name').val();
    var mail = $('#mail').val();
    var message = $('#messa').val();

    //ENVIA CONTACTO
    $.ajax({
        type: 'POST',
        url: 'php/contacto/envia-contacto.php',
        data: ({
            nombre: nombre,
            mail: mail,
            message: message
        }),
        context: document.body,
        success: function(data) {
            $('#messageForm').addClass('is-hidden');
            $('#messageMsg').html(data);
        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

})

//EMAIL
$('#mail').on('keyup', function() {
    email = $(this).val();
    if (validateEmail(email)) {
        $('.emailCheck').addClass('is-hidden');
        $('.enviarContacto').removeClass('is-hidden');
    } else {
        $('.emailCheck').removeClass('is-hidden');
        $('.enviarContacto').addClass('is-hidden');
    }
    
    if (this.value.length == 0) {
        $('.emailCheck').addClass('is-hidden');
        $('.enviarContacto').addClass('is-hidden');
    }
});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}