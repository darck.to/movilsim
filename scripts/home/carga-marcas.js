//MARCAS
var box = '<div class="column is-2 is-half-mobile marca-encajonada marca-home handed"><div class="marcaName">';
var end = '</div></div>';

var marca;

$(document).ready(function(){

	cargaMarcas();

	$.fn.toggleText = function(t1, t2){
	  if (this.text() == t1) this.text(t2);
	  else                   this.text(t1);
	  return this;
	};

});

function cargaMarcas(e) {

	$.ajax({
		type: "POST",
		url: "php/home/carga-marcas.php",
		dataType: "json",
		context: document.body,

		success: function(data) {

			$.each(data, function(index, home) {
				if (home.portada == 1) {
	            	$('#marcaBox').append(box + '<img class="marca-brand" src="img/' + home.img + '">' + '<div><p val=' + home.id + ' mar=' + home.name + '>' + home.name + '</p></div>' + end);
				}
	        });

			$('#marcaBox').append('<div class="marcasHide columns is-multiline is-hidden pa-eleven"></div>');

        $.each(data, function(index, home) {
					if (home.portada != 1) {
          	$('.marcasHide').append(box + '<img class="marca-brand" src="img/' + home.img + '">' + '<div><p val=' + home.id + ' mar=' + home.name + '>' + home.name + '</p></div>' + end);
					}
        });

        //$('#marcaBox').after("<div class=\"showMarcas has-text-weight-bold handed font-s-2\"><p><?php echo _BRAMORE; ?></p><i class='icon-show fas fa-chevron-down fa-5x'></i></div>");

        //CARGA LA MARCA SELECCIONADA
			$('.marcaName').on('click',function(){

				var val = $(this).find('p').attr('val');
				var marca = $(this).find('p').attr('mar');

				//cargaEquipos(val, marca);
				cargaImei(val, marca);

			});

			//MUESTRA LAS MARCAS OCULTAS
			$('.showMarcas').on( 'click' , function () {

				$('.brandToogle').toggleClass('is-hidden');
				$('.marcasHide').toggleClass('is-hidden');
				$('.icon-show').toggleClass('fa-chevron-down');
				$('.icon-show').toggleClass('fa-chevron-up');
				//$('.showMarcas p').toggleText('<?php echo _BRALESS; ?>', '<?php echo _BRAMORE; ?>');

			});

		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}

function cargaEquipos(val, mar) {

	var idSelect = val;
	var idMarca = mar;

	$.ajax({
		type: "POST",
        url: "php/desbloquea/carga-equipos.php",
        dataType: "json",
		context: document.body,
        success: function(data) {

        	$('#desqloqueaBox').html('');
        	$('#desqloqueaBox').removeClass('hidden');
        	$('#desqloqueaBox').addClass('fadeIn border-r yellow lighten-4 ten-p');

        	$.each(data, function(index, marca) {
        		if (marca.id_m == idSelect) {
	            	$('#desqloqueaBox').append(boxEquipos + '<img class="marca-brand" src="img/' + marca.img + '">' + '<div class="center-align"><p val=' + marca.id + '>' + marca.name + '</p></div>' + end);
        		}
	        });

        	$('#desqloqueaBox').append("<h6 class='right right-align grey-text text-lighten-1'><b>Selecciona tu equipo para continuar el proceso</b></h6>");

	        $('html, body').animate({ scrollTop: 0 }, 'fast');

	        //CARGA LA MARCA SELECCIONADA
			$('.equipoName').on('click',function(){

				var val = $(this).find('p').attr('val');

				cargaImei(val, idMarca);

			})

        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
    })

}


//OMNI Buscador
$("input.omniSearch").on("keyup", function(e) {

    var input, filter, parent, contenedor, p, i;

    input = $(this);

    filter = input.val().toUpperCase();

    parent = $('#marcaBox > .column');

    contenedor = $('#marcaBox > .column > .marcaName > div')

    for (i = 0; i < contenedor.length; i++) {

        p = contenedor[i].getElementsByTagName("p")[0];

        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {

            parent[i].style.display = "";

        } else {

            parent[i].style.display = "none";

        }

    }

    var j;

    parentHid = $('#marcaBox > .marcasHide > .column');

    hideContenedor = $('#marcaBox > .marcasHide > .column > .marcaName > div')

    for (j = 0; j < hideContenedor.length; j++) {

        p = hideContenedor[j].getElementsByTagName("p")[0];

        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {

        	$('.marcasHide').addClass('is-hidden');
        	$('.marcasHide').css('width','auto');
            parentHid[j].style.display = "";

        } else {

        	$('.marcasHide').removeClass('is-hidden');
        	$('.marcasHide').css('width','100%');
            parentHid[j].style.display = "none";

        }

    }


});
