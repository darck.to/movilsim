//INICIALIZAMOS
var cuponNumber;

$(document).ready(function(){

});

$('.button-carga-cupon').on('click',function(){

    cuponNumber = $('#cuponForm').val();

    //REVISAR ORDER
    $.ajax({
        type: 'POST',
        url: 'php/cupones/revisa-cupon.php',
        data: JSON.stringify({
            cupon : cuponNumber
        }),
        contentType: "application/json",
        context: document.body,
        success: function(data) {

            console.log(data);

            $.each(data, function () {
                $.each(this, function (name, value) {
                    if (name == 'Vigencia') {
                        if (value == '0') {
                            $('.statusCupon').html('<b class="has-text-danger">' + translateString('_COUPSTA1') + '</b>');
                        } else if (value == '1') {
                            $('.statusCupon').html('<b class="has-text-success">' + translateString('_COUPSTA2') + '</b>');
                        } else if (value == '2') {
                            $('.statusCupon').html('<b class="has-text-grey-dark">' + translateString('_COUPSTA3') + '</b>');
                        }
                    }

                    if (name == 'Till') {
                        $('.timeCupon').html(value);
                    }

                    if (name == 'Descuento') {
                        $('.discountCupon').html(value + '%');
                    }

                })
            }); 

        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

})