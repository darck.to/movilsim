//PLANTILLAS CAJAS DE MENSAJES
var boxMensajes = '<div class="box is-radiusless single-item"><div class="content">';
var end = '</div></div>';

// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Cerrar', 'tingle-btn tingle-btn--primary', function() {
    
    modal.close();                

});

$(document).ready(function(){

    //CARGA MENSAJES
    cargaMensajes();

});

function cargaMensajes(e) {

    $.ajax({

        method: 'post',
        url: 'php/preguntas/carga-mensajes.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            data.sort(function(a, b){return b.id - a.id});

            $.each(data, function(index, message) {
                $('#preguntasMensajes').append(boxMensajes + '<h6 class="font-s-1 has-text-weight-bold ma-b-zero">' + message.usuario + '&nbsp;<i class="is-size-7 font-color-1">' + message.fecha + ':</i></h6><p class="searchParagraph"><b>' + message.mensaje + '</b></p><h6 class="has-text-right is-size-7 ma-b-zero font-color-1">' + translateString('_ASKANS1') + '</h6><p class="has-text-right p' + index + '"><b class="has-text-weight-light">' + message.answer + '</b></p>' + end);
            });

            //PAJINATOR
            $(".container-pajinator").pagify(5, ".single-item");

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });    

};

//OMNI Buscador
$("input.omniSearch").on("keyup", function(e) {
    
    var input, filter, parent, contenedor, p, i;

    input = $(this);
    
    filter = input.val().toUpperCase();

    parent = $('#preguntasMensajes > .single-item');

    contenedor = $('.content > .searchParagraph')
    
    for (i = 0; i < contenedor.length; i++) {
        
        p = contenedor[i].getElementsByTagName("b")[0];
        
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {

            parent[i].style.display = "";

        } else {

            parent[i].style.display = "none";

        }

    }

});