//INICIALIZAMOS
var orderNumber;
var folio;
var kindOrder;

// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: [],
    closeMethods: ['overlay', 'button', 'escape'],
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Resend status', 'tingle-btn tingle-btn--primary', function() {

    //REVISAR ORDER
    $.ajax({
        type: 'post',
        url: 'php/order/envia-order.php',
        data: {
            folio : folio,
            kind : kindOrder,
            order : orderNumber
        },
        context: document.body,
        success: function(data) {

            modal.close();

        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

        
});

$(document).ready(function(){

    revisaOrder();

});

function revisaOrder(e) {

    orderNumber = $('#orderCheck').val();

    //REVISAR ORDER
    $.ajax({
        type: 'POST',
        url: 'php/order/revisa-order.php',
        data: JSON.stringify({
            order : orderNumber
        }),
        contentType: "application/json",
        context: document.body,
        success: function(data) {

            console.log(data);

            var text = "<img class='responsive-img' src='img/logo.png'>";
            text += "<div class='section'></div>";
            
            $.each(data, function () {
                $.each(this, function (name, value) {
                    if (name == 'Fecha') {
                        title = 'Order Date'
                    } else if (name == 'Order') {
                        title = 'Order #'
                    } else {
                        title = name
                    }

                    if (name == 'Estatus' && value == '0') {
                        title = 'Status';
                        value = '<i class="material-icons fiv-p red-text">thumb_down</i>'
                    } else if (name == 'Estatus' && value == '1') {
                        title = 'Status';
                        value = '<i class="material-icons fiv-p green-text">thumb_up</i>'
                    }

                    if (name == 'Marca') {
                        kindOrder = 'order';
                    } else {
                        kindOrder = 'check';
                    }

                    if (name == 'Folio') {
                        folio = value;
                    }

                    if (name != 'Folio') {
                        text += "<h5 class='grey-text text-darken-1 no-m fiv-p'>" + title + ": <h6 class='grey-text text-darken-2 no-m'>" + value + "</h6></h5>";
                    }

                })
            });

            text += "<div class='section'></div>";
            text += "<p class='grey-text text-darken-1 no-m'>Pressing the button below will send you the full report and status of your order</p>";

            // set content
            modal.setContent(text);

            modal.open();

        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

}