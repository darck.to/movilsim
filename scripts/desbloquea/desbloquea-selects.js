//EQUIPOS
var boxEquipos = '<div class="column is-2 is-half-mobile has-background-white marca-encajonada marca-equipos-box shadow-blue handed"><div class="content is-relative equipoName">';
var end = '</div></div>';
//TIPO DE PAGO
var pago;

//VARIABLES
var imei;
var email;
var name;
var logoUnlock;
var marca;
var equipo;
var equipoID;
var precio;
var serviceID;
var serviceNAME;
var desc;
var pais;
var operator;
var serviceBU;
var bundleArray;
var resultBundle = [];
var serviceArray = [];
var equipmentArray;

//NUMERO DE ORDER
var order;


$(document).ready(function(){
    //CARGA SELECTS
    cargaSelects();
});

//CARGA SELECTS DE PAISES Y OPERADORES
function cargaSelects(e) {

  id = $('#id').html();
  marca = $('#marca').html();
  equipo = $('#equipo').html();
  equipoID = $('#equipoID').html();
  var img = $('#img').html();

  $(".marca-brand").attr('src','' + img);
  $("#marcaBrand").html(marca);
  $("#equipoBrand").html(equipo);

  $('.thirdStepOrder').addClass('is-hidden');

  $('.progress').removeAttr('value');

  //CARGA LOS PAISES DESDE JSON
  $.ajax({

    method: 'post',
    url: 'php/desbloquea/carga-paises.php',
    dataType: "json",
    context: document.body,
    success: function(result) {
      $.each(result, function(index, pais) {
        if (pais.portada == "1") {
          starColor = "amber-text";
          $("#selectCountry").append("<option class='cargaOperador' value=" + pais.id + ">" + pais.pais + "</option>");
        } else {
        }
      });
      $("#selectCountry").append("<option class='dropdown-divider'></option>");
      $.each(result, function(index, pais) {
        $("#selectCountry").append("<option class='cargaOperador' value=" + pais.id + ">" + pais.pais + "</option>");
      });

      $('#selectCountry').on('change',function(){
        $("#selectOperator").html("");
        var paisSelect = $(this).val();
        //CARGA LOS OPERADORES DESDE JSON
        $.ajax({

          method: 'post',
          url: 'php/desbloquea/carga-operators.php',
          dataType: "json",
          context: document.body,
          success: function(data) {
            var duplicados = [];
            var unicos = [];
            //$.each(data, function(name,content) {
            //  if (!duplicados[content.operador]) {
            //    duplicados[content.operador] = true;
            //    unicos.push(content);
            //  }
            //});
            var n = 0;
            $('.paymentCheck').removeClass('is-hidden');
            $("#selectOperator").html("<option value=0>" + translateString('_ORDESESEL3') + "</option>");

            $.each(data, function(key, operator) { //REPLACED DATA WITH UNICOS
              //2 LEÉ EL ARREGLO DE EQUIPOS QUE CUENTAN CON OPERADOR ASIGNADO
              equipmentArray = operator.eqarr;
              equipmentArray = equipmentArray.toString();
              equipmentArray = equipmentArray.split(',');

              //3 SERVICE ARRAY DE CARGA-OPERATORS
              var serviceOperator = operator.serviceid;
              serviceOperator = serviceOperator.toString();
              serviceOperator = serviceOperator.split(',');

              precio = operator.cost;

              $.each(equipmentArray, function(i, equipment) {
                //4 LEÉ SI EL EQUIPO QUE SE ENCUENTRA EN EL OPERADOR ES EL MISMO QUE EL QUE ESTÁ SELECCIONADO
                if (equipment == equipoID) {
                  console.log(equipment, equipoID);
                  //CARGA OPTIONS CON LOS OPERADORES
                  if (operator.id_m == paisSelect) {
                    if (!duplicados[operator.operador]) {
                      duplicados[operator.operador] = true;
                      $("#selectOperator").append("<option value=" + operator.id + ">" + operator.operador + "</option>");
                      n++;
                    }
                  }
                }
              });

              $('#selectOperator').on('change',function(){
                if(this.value != 0 ) {
                  $('.thirdStepOrder').removeClass('is-hidden');
                } else {
                  $('.thirdStepOrder').addClass('is-hidden');
                }
              });

            });

            if (n == 0) {
              box = '<div class="box is-6 has-background-white radius-5 pa-one">';
                box += '<div class="content has-text-centered">';
                  box += '<h4 class="has-text-danger">' + translateString('_ORDERSESEH1') + '</h4>';
                  box += '<p>' + translateString('_ORDERSESEP1') + '</p>';
                  box += '<div class="field">';
                    box += '<div class="control">';
                      box += '<input id="i-mail" class="input is-primary" type="text" placeholder="' + translateString('_ORDERSESEI1') + '">';
                    box += '</div>';
                  box += '</div>';
                  box += '<a class="button sendOperatorNotification ma-b-one pa-one input-format"><b>' + translateString('_ORDERSESEB1') + '</b></a>';
                box += '</div>';
              box += '</div>';
              $('#boxMessage').html(box);

              $('.sendOperatorNotification').on('click', function() {
                var mail = $('#i-mail').val();
              })
            } else {
              $('#boxMessage').html('');
            }

            $('.progress').attr('value','66');
          },
          error: function(xhr, tst, err) {
            console.log(err);
          }

        });
      })

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }

  });

}

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
$('.thirdStepOrder').on('click',function(){

  var id = $('#equipo').html();
  var country = $('#selectCountry').val();
  var operator = $('#selectOperator').val();
  var url = 'desbloquear.php';
  var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
  '<input type="text" name="paso" value=3 />' +
  '<input type="text" name="id" value="' + id + '" />' +
  '<input type="text" name="marca" value="' + marca + '" />' +
  '<input type="text" name="equipo" value="' + id + '" />' +
  '<input type="text" name="equipoID" value="' + equipoID + '" />' +
  '<input type="text" name="pais" value="' + country + '" />' +
  '<input type="text" name="operator" value="' + operator + '" />' +
  '</form>');
  $('body').append(form);
  form.submit();

});
