//EQUIPOS
var boxEquipos = '<div class="column is-2 is-half-mobile has-background-white marca-encajonada marca-equipos-box shadow-blue handed"><div class="content is-relative equipoName">';
var end = '</div></div>';
//TIPO DE PAGO
var pago;

//VARIABLES
var imei;
var email;
var name;
var logoUnlock;
var marca;
var equipo;
var equipoID;
var precio;
var marcaimg;
var serviceID;
var serviceNAME;
var desc;
var pais;
var operator;
var serviceBU;
var bundleArray;
var resultBundle = [];
var serviceArray = [];
var equipmentArray;

//NUMERO DE ORDER
var order;

$(document).ready(function(){

  //CARGA EQUIPOS
  cargaEquipos();


});

function cargaEquipos() {

  id = $('#id').html();
  marca = $('#marca').html();
  var imgmarca;

  //IMAGEN DE LA marca
  $.ajax({
		type: "POST",
		url: "php/home/carga-marcas.php",
		dataType: "json",
		context: document.body,
		success: function(data) {
			$.each(data, function(index, content) {
				if (content.name == marca) {
	        imgmarca = content.img
				}
	    });
		},
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});

  $.ajax({
    type: "POST",
    url: "php/desbloquea/carga-equipos.php",
    dataType: "json",
    context: document.body,
    success: function(data) {

      $('#desqloqueaBox').html('');

      var n = 0;
      var equipotmp;
      var marcaname;

      $.each(data, function(index, value) {
        //SI CUENTA CON EQUIPOS DADOS DE ALTA
        if (value.id_m == id) {
          //PRIMERO SI CUENTA CON UN EQUIPO UNICO LLAMADO IGUAL QUE LA MARCA
          if (value.name == marca) {
            console.log(value.name, marca);
            $('#desqloqueaBox').html();
            marcaimg = value.img;
            marcaname = value.name;
            equipotmp = value.id;
            var text = boxEquipos + '<p class="is-size-6 is-hidden" val=' + equipotmp + '></p>';
              text += '<div class="content si-display">';
                text += '<h6>' + translateString('_ORDEEQS2'); + '&nbsp;';
                text +=  '&nbsp;' + marca + '&nbsp;' + translateString('_ORDEEQS22'); + '</h6>'
                text += '<img class="pa-one" src="img/' + marcaimg + '">';
              text += '</div>';
              text += '<div class="content si-display pa-one is-size-7">';
                text += '<li>' + translateString('_ORDEEQTXT1') + '</li>';
                text += '<li>' + translateString('_ORDEEQTXT2') + '&nbsp;' + marcaname + '&nbsp;' + translateString('_ORDEEQTXT22') + '</li>';
                text += '<li>' + translateString('_ORDEEQTXT3') + '&nbsp;' + marcaname + '&nbsp;' + translateString('_ORDEEQTXT33') + '</li>';
                text += '<li>' + translateString('_ORDEEQTXT4') + '</li>';
              text += '</div>';
              text += '<div class="content background-p-2 si-display radius-5 pa-one">';
                text += '<p class="has-text-centered is-size-5 has-text-white width-one">' + translateString('_ORDEEQBTN1') + '</p>';
              text += '</div>' + end;

            $('#desqloqueaBox').html(text);

            $('.marca-encajonada').addClass('centered-div');
            $('.marca-encajonada').addClass('pa-one margin-cero-auto height-auto');
            $('.marca-encajonada').removeClass('is-2');
            $('.marca-encajonada').addClass('width-one-third');
            return false
          }
          $('#desqloqueaBox').append(boxEquipos + '<img src="img/' + value.img + '">' + '<p class="is-size-6 equipo-name" val=' + value.id + '>' + value.name + '</p>' + end);
          n = n + 1;
        }
      });

      //CARGA LA MARCA SELECCIONADA
      $('.equipoName').on('click',function(){
        equipo = $(this).find('p').attr('val');
        equipoID = $(this).find('p').attr('val');
        marcaimg = $(this).find('img').attr('src');
        secondStep();
      })

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })

}

//CARGA PAISES Y OPERADORES SEGUNDO PASO ANTES DEL PAGO
function secondStep(){

    var url = 'desbloquear.php';
    var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
    '<input type="text" name="paso" value=2 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="equipo" value="' + equipo + '" />' +
    '<input type="text" name="equipoID" value="' + equipoID + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '<input type="text" name="img" value="' + marcaimg + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();

}
