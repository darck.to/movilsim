//EQUIPOS
var boxEquipos = '<div class="column is-2 is-half-mobile has-background-white marca-encajonada marca-equipos-box shadow-blue handed"><div class="content is-relative equipoName">';
var end = '</div></div>';
//TIPO DE PAGO
var pago;

//VARIABLES
var imei;
var email;
var name;
var logoUnlock;
var marca;
var equipo;
var equipoID;
var precio;
var serviceID;
var serviceNAME;
var desc;
var pais;
var operator;
var serviceBU;
var bundleArray;
var serviceTAG;
var tagArray;
var resultBundle = [];
var equipmentArray;
var payOption;

var precioBox;
var precioAttr;
var dollar, euro, peso;
var coin = "MXN";

//NUMERO DE ORDER
var order;

// instanciate new modal
var modal = new tingle.modal({
  footer: true,
  stickyFooter: true,
  closeMethods: [],
  closeLabel: "Close",
  cssClass: ['custom-class-1', 'custom-class-2'],
  onOpen: function() {
  },
  onClose: function() {
  },
  beforeClose: function() {
    return true;
  }
});

$(document).ready(function(){
  //CARGA CAPTURAIMEI
  cargaCapturaImei();
  currency();
});

function currency(e) {
  $.ajax({

    method: 'get',
    url: 'admin/apo/carga-currency.php',
    dataType: "json",
    context: document.body,
    success: function(data) {

      $.each(data, function(key, content) {
        dollar = content.USD;
        euro = content.EUR;
        return false
      });

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }

  });
}

function cargaCapturaImei(e) {

  id = $('#id').html();
  marca = $('#marca').html();
  equipo = $('#equipo').html();
  equipoID = $('#equipoID').html();
  pais = $('#pais').html();
  operator = $('#operator').html();

  //SOLICITAMOS LA MARCA y EQUIPO
  $.ajax({

    method: 'post',
    url: 'php/desbloquea/carga-equipos.php',
    dataType: "json",
    context: document.body,
    success: function(data) {

      $.each(data, function(index, unlock) {
        if (unlock.id == equipo) {
          equipo = unlock.name;
          desc = unlock.desc;
          logoUnlock = unlock.img;
        }
      });

      $(".marca-brand").attr('src','img/' + logoUnlock);
      $("#marcaBrand").html(marca);
      $("#nameEquipment").html(equipo);
      $("#equipoBrand").html(equipo);
      $("#descPhone").html(desc);
      $("#sectionMarcaEquipo").html(marca + ' ' + equipo);
      $("#sectionImgEquipo").attr('src','img/' + logoUnlock);

      leePrecio();

    },
    error: function(xhr, tst, err) {
        console.log(err);
    }

  });

  function leePrecio(e) {

    $('.offerBox').html('');
    $('.progress').removeAttr('value');

    $.ajax({
      type: "POST",
      url: "php/desbloquea/carga-operators.php",
      dataType: "json",
      context: document.body,
      success: function(data) {

        //LEE SERVICIOS Y BUNDLE
        leeBundle();

        $.ajax({
          type: "POST",
          url: "admin/apo/carga-servicios.php",
          data: JSON.stringify({
              src: 1
          }),
          dataType: "json",
          context: document.body,
          success: function(result) {

            //0.5 CARGAMOS LOS SERVICIOS QUE TENGAN EL MISMO NOMBRE DE EXISTIR PARA TOMARLOS TODOS
            var nombreOperator;
            $.each(data, function(i,v){
              if (operator == v.id) {
                nombreOperator = v.operador
              }
            })

            //1 SERVICEARRAY
            var serviceArray = result;

            $('.paymentCheck').removeClass('is-hidden');
            var counter = 0; //PARA CADA clase de price
            $.each(data, function(key, operador) {

              //1.5 SOLO LEE SI EL OPERATOR ES IGUAL A LA VARIABLE GLOBAL operator OPERADOR RECIBIDO Y AL NOMBRE DEL OPERADOR QUE SE SELECCIONO
              if (nombreOperator === operador.operador) {
                //if (operador.id == operator) {

                  //2 LEÉ EL ARREGLO DE EQUIPOS QUE CUENTAN CON OPERADOR ASIGNADO
                  equipmentArray = operador.eqarr;
                  equipmentArray = equipmentArray.toString();
                  equipmentArray = equipmentArray.split(',');
                  //2.5 LEÉ EL ARREGLO DE PRECIOS QUE CUENTAN CON OPERADOR ASIGNADO
                  serviceBU = operador.servicebu;
                  serviceBU = serviceBU.toString();
                  bundleArray = serviceBU.split(',');
                  //2.6 LEÉ EL ARREGLO DE TAGS QUE CUENTAN CON OPERADOR ASIGNADO
                  serviceTAG = operador.servicetag;
                  serviceTAG = serviceTAG.toString();
                  tagArray = serviceTAG.split(',');

                  //3 SERVICE ARRAY DE CARGA-OPERATORS NNNOOO EL DE serviceArray de CARGA-SERVICIOS
                  var serviceOperator = operador.serviceid;
                  serviceOperator = serviceOperator.toString();
                  serviceOperator = serviceOperator.split(',');

                  $.each(equipmentArray, function(i, equipment) {
                    //4 LEÉ SI EL EQUIPO QUE SE ENCUENTRA EN EL OPERADOR ES EL MISMO QUE EL QUE ESTÁ SELECCIONADO
                    if (equipment == equipoID) {

                      $.each(serviceArray, function(j, services) {
                        $.each(serviceOperator, function(k, service) {
                          //5 LEÉ SI EL SERVICIO PROVENIENTE DE SERVICEID ES IGUAL AL QUE OFRECE EL OPERADOR EN serviceid
                          if (services.SERVICEID == service) {
                            //LEE EL PRECIO DEL BUNDLE ACTUAL
                            discountNow = bundleArray[k];
                            //LEE EL TAG QUE CORRESPONDE
                            tag = tagArray[k];
                            var finalPrice = discountNow;
                            var text = '<div class="column is-one-third centered-div divToHide">';
                            text += '<div class="box">';
                            text += '<div class="content has-text-centered">';
                            //CALCULA EL DESCUENTO BASADO EN EL TIMER Y CORRIGE DE SER NECESARIO
                            var prices;
                            var then = localStorage.getItem('time');
                            var prices = "";
                            var now = new Date().getTime();
                            var distance = then - now;
                            text += '<h1 class="is-size-4 ma-no">' + translateString('_ORDETAG') + '</h1>';
                            $.each(resultBundle, function(index,value) {
                              priceBundle = ((finalPrice*value.discount)/100)+parseInt(finalPrice);
                              priceBundle = ((priceBundle*value.discount)/100)+parseInt(finalPrice);
                              priceBundle = priceBundle.toFixed(2)
                            })
                            var x = setInterval(function() {
                              if (distance < 0) {
                                for (var i = 0; i < counter; i++) {
                                  $('.priceD' + i).html($('.beforeD' + i).html());
                                  $('.payC' + i).attr('price',$('.beforeD' + i).html());
                                  clearInterval(x);
                                }
                              }
                            }, 500);
                            text += '<small class="has-text-centered has-text-grey width-one ma-no"><i>' + translateString('_ORDEPRE') + '&nbsp;$<span class="te-d-lt beforeD' + counter + '">' + priceBundle + '</span></i></small>';
                            text += '<h2 class="has-text-weight-bold has-text-centered has-text-danger width-one ma-t-half">$<i id="afterPrice" class="precioDesbloqueo priceD' + counter + '">' + finalPrice + '.00</i></h2>';
                            text += '</div>';
                            text += '<div class="content">';
                            text += '<p class="is-marginless is-uppercase is-size-7 has-text-weight-bold has-text-justified font-s-2">';
                            text += '' + tag + '&nbsp;';
                            if (operador.day != "") { time =  operador.day + " d&iacute;as"}
                            if (operador.hour != "") { time = operador.hour + " horas"}
                            if (operador.min != "") { time = operador.min + " minutos"}
                            text += '<br><i class="font-color-1">Tiempo estimado de entrega: ' + time + '</i>';
                            text += '<br><i class="font-color-1">Tasa de exito: ' + operador.rate + '%</i>';
                            text += '<br>DESBLOQUEO POR IMEI';
                            text += '</p>';
                            text += '</div>';
                            text += '<div class="badge-undo is-relative is-hidden"></div>';
                            text += '<div class="content background-p-2 handed payConfirm payC' + counter + '" price="' + finalPrice + '" serid=' + services.SERVICEID + ' serna=' + tag + '>';
                            text += '<div class="content">';
                            text += '<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white width-one">COMPRAR</p>';
                            text += '</div>';
                            text += '</div>';
                            text += '</div>';
                            text += '</div>';

                            $('.offerBox').append(text);
                            counter++;
                          }

                        });

                      });

                    }

                  });

                  //GUARDAMOS LOS PRECIOS EN UN ARRAY
                  //RECORREMOS LOS PRECIOS Y LOS GUARDAMOS EN UN ARRAY EN ORDEN DE APARICION
                  precioBox = $('.precioDesbloqueo').map(function() {//PRECIOS EN BOX
                    return $(this).html();
                  }).get().join( "," );
                  precioBox = precioBox.toString();
                  precioBox = precioBox.split(',');
                  precioAttr = $('.payConfirm').map(function() {
                    return $(this).attr('price');
                  }).get().join( "," );
                  precioAttr = precioAttr.toString();
                  precioAttr = precioAttr.split(',');
                //}

              } //FINAL DE IF NOMBRE DE OPERATOR

              $('.progress').attr('value','99');

            });

            $('.payConfirm').on('click',function(){
              precio = $(this).attr('price');
              serviceID = $(this).attr('serid');
              serviceNAME = $(this).attr('serna');
              $('.divToHide').addClass('no-display');
              $(this).parentsUntil('divToHide').removeClass('no-display');
              $('.badge-undo').removeClass('is-hidden');
              $('.progress').removeAttr('value');
              confirmPayment();
            });

            $('.badge-undo').on('click',function(){
              $('#inputOrderForm').addClass('is-hidden');
              $('.divToHide').removeClass('no-display');
              $('.badge-undo').addClass('is-hidden');
            });

          },
          error: function(xhr, tst, err) {
            console.log(err);
          }
        })

      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })

  }


};

//CARGA LA MARCA SELECCIONADA
function confirmPayment(e){
  $('#inputOrderForm').removeClass('is-hidden');
  $('#inputOrderForm').addClass('active-show');
  $('#name').focus();
};

//CHECADORES DE INPUTS
$('.desbloquea-option').change(function() {
  payOption = $('input[name="payoption"]:checked').val();
  $('.disableButton').removeClass('is-hidden');
  $('.disableButton').removeClass('active-hide');
  $('.disableButton').addClass('active-show');
});

//IMEI
$('#imei').on('keyup', function() {
  if (this.value.length < 15 || this.value.length > 15) {
    $('.imeiCheck').removeClass('is-hidden');
    $(this).siblings('.desbloquea-input-check').addClass('is-hidden');
    $('.disableButton').attr('onclick',' ');
    $('.disableButton').unbind('click');
  } else {
    $('.imeiCheck').addClass('is-hidden');
    $(this).siblings('.desbloquea-input-check').removeClass('is-hidden');
    $('.disableButton').click(creadorOrdenes);
  }
  if (this.value.length == 0) {
    $('.imeiCheck').addClass('is-hidden');
    $(this).siblings('.desbloquea-input-check').addClass('is-hidden');
    $('.disableButton').attr('onclick',' ');
    $('.disableButton').unbind('click');
  }
});

//EMAIL
$('#email').on('keyup', function() {
    email = $(this).val();
    if (validateEmail(email)) {
        $('.emailCheck').addClass('is-hidden');
        $(this).siblings('.desbloquea-input-check').removeClass('is-hidden');
    } else {
        $('.emailCheck').removeClass('is-hidden');
        $(this).siblings('.desbloquea-input-check').addClass('is-hidden');
    }

    if (this.value.length == 0) {
        $('.emailCheck').addClass('is-hidden');
        $(this).siblings('.desbloquea-input-check').addClass('is-hidden');
        $("#imei").prop("disabled",true);
    }
});

//CONFIMATION
$('#confirm').on('keyup', function() {
    email = $('#email').val();
    confirm = $(this).val();
    if (confirm != email) {
        $('.confirmCheck').removeClass('is-hidden')
        $(this).siblings('.desbloquea-input-check').addClass('is-hidden');
        $("#imei").prop("disabled",true);
    } else {
        $('.confirmCheck').addClass('is-hidden');
        $(this).siblings('.desbloquea-input-check').removeClass('is-hidden');
        $("#imei").prop("disabled",false);
    }

    if (this.value.length == 0) {
        $('.confirmCheck').addClass('is-hidden');
        $(this).siblings('.desbloquea-input-check').addClass('is-hidden');
        $("#imei").prop("disabled",true);
    }
});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function leeNombre(name, type) {
    var cadena = name;
    var tipo = type;
    $.ajax({
      type: "POST",
      url: "php/desbloquea/carga-nombres.php",
      async: false,
      data: ({
        cadena: cadena,
        tipo: tipo
      }),
      dataType: 'text',
      context: document.body,
      success: function(result) {
        $('.tagString'+tipo+'').html(result);
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
}

function leeBundle() {

  $.ajax({
    method: 'post',
    url: 'php/desbloquea/lee-bundle.php',
    dataType: "json",
    context: document.body,
    success: function(data) {
      resultBundle = data;
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });

}

//CAMBIA TIPO DE CAMBIO DE ACUERDO A LA API https://api.exchangeratesapi.io/latest?base=MXN&symbols=USD,EUR
$('.changeCurrency').on('change',function(e) {

  var type = this.value;
  var mon, bubble;

  $.ajax({

    method: 'get',
    url: 'admin/apo/carga-currency.php',
    dataType: "json",
    context: document.body,
    success: function(data) {

      $.each(data, function(key, content) {

        if (type == 1) {
          mon = 1;
          coin = "MXN"
        } else if (type == 2) {
          mon = content.USD;
          dollar = content.USD;
          coin = "USD"
        } else if (type == 3) {
          mon = content.EUR;
          euro = content.EUR;
          coin = "EUR"
        }

        return false

      });

      //CAMBIAMOS LOS VALORES EN CADA ARRAY Y LOS MANDAMOS A SU LUGAR DE ORIGEN
      $.each(precioBox, function(key, content) {
        bubble = content * mon;
        $('.priceD'+key).html(bubble.toFixed(2));
      })
      $.each(precioAttr, function(key, content) {
        bubble = content * mon;
        $('.payC'+key).attr('price',bubble.toFixed(2));
      })

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }

  });

});

//LAST STEP
function creadorOrdenes(e) {

  var pay = "";
  var icons = "";
  //ELIGE ENTRE LAS OPCIONES DE PAGO
  if (payOption == 1) {
    //CARD//PAYPAL BUTTON & MEARCADOPAGO BUTTON
    icons += "<img class='desbloquea-icons' src='img/ico/2.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/3.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/4.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/5.png'>";
    pay += "<div id='paypal-button' class='width-one pago-pay'></div>";
    pay += "</div>";
    pay += "<div class='tile is-parent radius-5'>";
    pay += "<a class='width-one pago-mep' target='_blank'>" + translateString('_ORDERMOD15'); + "</a>";
  } else if (payOption == 2) {
    //PAYPAL//PAYPAL BUTTON
    icons += "<img class='desbloquea-icons' src='img/ico/1.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/2.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/3.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/4.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/5.png'>";
    pay += "<div id='paypal-button' class='width-one pago-pay'></div>";
  } else if (payOption == 3) {
    //CASH//MEARCADOPAGO BUTTON
    icons += "<img class='desbloquea-icons' src='img/ico/6.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/7.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/8.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/9.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/10.png'>";
    icons += "<img class='desbloquea-icons' src='img/ico/11.png'>";
    pay += "<a class='width-one pago-mep' target='_blank'>" + translateString('_ORDERMOD15'); + "</a>";
  }

  //DESABILITAMOS EL TIPO DE DESLOQUEO SELECCIONADO Y LA CURRENCY
  $('.selectCurrency').addClass('is-hidden');
  $('.paymentCheck').addClass('is-hidden');

  //ASIGNAMOS VARIABLES
  marca = $('#marcaBrand').html();
  equipo = $('#equipoBrand').html();
  pais = $("#pais").html();
  operator = $("#operator").html();
  name = $("#name").val();
  email = $("#email").val();
  imei = $("#imei").val();

  //INPUTS REMOVED FOR SEC
  $('#inputTextForm').addClass('no-display');
  //$("#inputTextForm input").prop('disabled', true);
  $('.finalStep').remove();
  $('.badge-undo').remove();
  $('.payConfirm').remove();

  var text = "<img class='img-center pa-one' src='img/logo_500.png'>";
  text += "<div id='infoPayContent'>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDEORDFI1') + ": <i class='has-text-white has-text-weight-bold is-marginless'>" + name + "</i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDEORDFI2') + ": <i class='has-text-white has-text-weight-bold is-marginless'>" + email + "</i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDERMOD0') + ": <i class='has-text-white has-text-weight-bold is-marginless is-size-4'>" + imei + "</i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDERMOD1') + ": <i class='has-text-white has-text-weight-bold is-marginless tagString1'></i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDERMOD2') + ": <i class='has-text-white has-text-weight-bold is-marginless tagString2'></i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-weight-light is-marginless'>" + translateString('_ORDERMOD4') + ": <i class='has-text-white has-text-weight-bold is-marginless'>" + marca + ' - ' + equipo + "</i></h5>";
    text += "<h5 class='has-text-white ma-b-one pa-fiv radius-5 border-1p-solid border-w has-text-right'>" + translateString('_ORDERMOD5') + ": <b  class='has-text-weight-semibold is-size-3'>$<span id='priceTagOrder'>" + precio + "</span></b>" + coin + "</h5>";
    text += '<div class="control is-relative"><small class="is-size-7 te-d-ul handed showInputCoupon">' + translateString('_CUPONTXT') + '</small><i class="is-hidden desbloquea-input-check"></i><i class="is-hidden desbloquea-input-error"></i><input class="input input-format is-hidden checkInputCoupon" placeholder="' + translateString('_CUPONINP') + '"></div>';

    //AGREGA CHECKS DE PAYPAL, MERCADOPAGO Y EFECTIVO
    text += "<div class='content has-text-centered pa-one orderButtons'>";
      text += "<h5 class='is-size-5 has-text-white has-text-weight-light'>" + translateString('_ORDERMOD6') + "</h5>";
      text += "<div class='columns'>";
        text += "<div class='column is-12 has-text-centered'>";
        text += icons;
        text += "</div>";
      text += "</div>";
      text += "<div class='tile is-ancestor'>";
        //PAY BUTTON
        text += "<div class='tile is-parent radius-5'>";
          text += pay;
        text += "</div'>";
      text += "</div>";
    text += "</div>";
  text += "</div>";
  text += "</div>";

  //CONFIRMAMOS ORDER
  $('#confirmOrder').html(text);
  leeNombre(pais,1);
  leeNombre(operator,2);
  $('#confirmOrder').animate({ scrollTop: 0 }, 'fast');

  //MUESTRA COUPON INPUT
  $('.showInputCoupon').on('click', function(e){
    $(this).remove();
    $('.checkInputCoupon').removeClass('is-hidden');
  })

  //REVISA COUPON INPUT
  $('.checkInputCoupon').on('keyup', function(e){
    var value = $(this).val();
    if(value.length == 16) {
      //REVISAR ORDER
      $.ajax({
        type: 'POST',
        url: 'php/cupones/revisa-cupon.php',
        data: JSON.stringify({
          cupon : value
        }),
        contentType: "application/json",
        context: document.body,
        success: function(data) {
          $.each(data, function (name, content) {
            if (content.Vigencia == 1) {
              $('.checkInputCoupon').siblings('.desbloquea-input-check').removeClass('is-hidden');
              $('.checkInputCoupon').siblings('.desbloquea-input-error').addClass('is-hidden');
              //CAMBIA PRECIO
              $('#priceTagOrder').html((precio-(precio*content.Descuento)/100).toFixed(2));
              $('.checkInputCoupon').prop('disabled',true);
              precio = (precio-(precio*content.Descuento)/100).toFixed(2);
            } else {
              $('.checkInputCoupon').siblings('.desbloquea-input-check').addClass('is-hidden');
              $('.checkInputCoupon').siblings('.desbloquea-input-error').removeClass('is-hidden');
            }
          });
        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
      })
    }
  })

  //CARGAMOS PAYPAL
  jQuery.getScript("scripts/desbloquea/checkOutPaypal.js", function(data, status, jqxhr) {
  });

  $('.progress').attr('value','100');

  $('.pago-mep').on('click',function(){

    //var operation = $('#idServicio').val();
    //PROCESA PAGO EFECTIVO
    var form_data = new FormData();

    form_data.append("imei", imei);
    form_data.append("email", email);
    form_data.append("nombre", name);
    form_data.append("marca", marca);
    form_data.append("equipo", equipo);
    form_data.append("precio", precio);
    form_data.append("coin", coin);
    form_data.append("pais", pais);
    form_data.append("operator", operator);
    form_data.append("serviceid", serviceID);
    form_data.append("servicename", serviceNAME);
    //SOLICITAMOS EL CODIGO
    $.ajax({
      method: 'post',
      url: 'php/desbloquea/desbloquea-order.php',
      data: form_data,
      cache: false,
      async: false,
      contentType: false,
      processData: false,
      dataType: "html",
      context: document.body,
      success: function(result) {
        $('#orderDiv').html('').delay(2000);
        $('#orderDiv').addClass('pa-one');
        $('#orderDiv').append('<h3 class="has-text-centered has-text-weight-light is-size-2">' + translateString('_ORDERMOD9') + ': <b class="font-color-1">' + result + '</b></h3>');
        $('#orderDiv').append('<h6 class="has-text-centered has-text-weight-light is-size-6">' + translateString('_ORDERMOD9S1') + '</h6>');
        $('#orderDiv').append('<h4 class="has-text-centered has-text-weight-light is-size-3">' + translateString('_ORDERMOD10') + '</h4>');
        $('#orderDiv').removeClass('is-hidden');
        $('.order-background').removeClass('is-hidden');
        //VARIABLE idVenta
        var randomID = $('.idVenta').html();
        var folioID = $('.idVenta').attr('folio');
        mercadopagoButton(randomID,folioID);
        //CARGAMOS MERCADOPAGOBUTTON
        function mercadopagoButton(e,f) {
          //PROCESA PAGO EFECTIVO
          var form_data = new FormData();
          form_data.append("email", email);
          form_data.append("nombre", name);
          //CAMBIO DE CURRENCY A DOLARES SI COIN NO ES DOLAR
          //if (coin == "USD") {
          //  precio = precio;
          //} else if (coin == "MXN") {
          //  precio = (precio * dollar);
          //  precio = precio;
          //} else if (coin == "EUR") {
          //  precio = (precio / euro) * dollar;
          //  precio = precio;
          //}
          form_data.append("precio", precio);
          form_data.append("coin", coin);
          form_data.append("random", e);
          form_data.append("folio", f);
          $.ajax({
            method: 'post',
            url: 'php/desbloquea/mercadopago-checkout.php',
            data: form_data,
            cache: false,
            async: false,
            contentType: false,
            processData: false,
            dataType: "html",
            context: document.body,
            success: function(data) {
              //$('.pago-mep').attr('href',data);
              $('.orderButtons').delay(1000).remove();
              //window.open(data,'_blank');
              var mpFrame = '<h6 class="background-p-1"><img class="img-center pa-one" src="img/logo_500.png" style="height:100px"></h6>';
              mpFrame += '<h6 class="has-text-weight-light is-size-7 pa-fiv background-p-1">' + translateString('_ORDERMOD7') + '</h6>';
              mpFrame += '<iframe class="iframeMP" src="' + data + '"></iframe>';
              modal.setContent(mpFrame);
              modal.open();
              $('.tingle-modal-box__content').css('background','#eee');
              $('.tingle-modal-box__content').css('height','80vh');
            },
            error: function(xhr, tst, err) {
              console.log(err);
            }
          });
        };
      },
      error: function(xhr, tst, err) {
          console.log(err);
      }
    });
  })
}
