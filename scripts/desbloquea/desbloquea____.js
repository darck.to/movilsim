//EQUIPOS
var boxEquipos = '<div class="column is-2 is-half-mobile has-background-white marca-encajonada marca-equipos-box shadow-blue handed"><div class="content is-relative equipoName">';
var end = '</div></div>';
//TIPO DE PAGO
var pago;

//VARIABLES
var imei;
var email;
var name;
var logoUnlock;
var marca;
var equipo;
var precio;
var desc;
var pais;
var operator;
var serviceID;
var serviceArray;
var serviceBU;
var bundleArray;
var discountNow;
var resultBundle = [];
var servicename;
var sid;

//NUMERO DE ORDER 
var order;

// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay','button','escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--primary', function() {
    
    modal.close();

});

$(document).ready(function(){

    //CARGA EQUIPOS
    cargaEquipos();

    //CARGA SELECTS
    cargaSelects();

    //CARGA CAPTURAIMEI y BUNDLE
    leeBundle();
    cargaCapturaImei();

});

function cargaCapturaImei(e) {

	id = $('#id').html();
    marca = $('#marca').html();
    equipo = $('#equipo').html();

    //SOLICITAMOS LA MARCA
    $.ajax({

        method: 'post',
        url: 'php/desbloquea/carga-equipos.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            $.each(data, function(index, unlock) {
                if (unlock.id == equipo) {
                    equipo = unlock.name;
                    precio = unlock.cost;
                    desc = unlock.desc;
                    logoUnlock = unlock.img;
                    serviceID = unlock.serviceid;
                    serviceID = serviceID.toString();
                    serviceArray = serviceID.split(',');
                    serviceBU = unlock.servicebu;
                    serviceBU = serviceBU.toString();
                    bundleArray = serviceBU.split(',');
                }
            });

            leePrecio();

        	$(".marca-brand").attr('src','img/' + logoUnlock);
            $("#marcaBrand").html(marca);
            $("#nameEquipment").html(equipo);
        	$("#equipoBrand").html(equipo);
            $("#descPhone").html(desc);
            $("#sectionMarcaEquipo").html(marca + ' ' + equipo);
            $("#sectionImgEquipo").attr('src','img/' + logoUnlock);

            $(".sectionInstrucciones").removeClass('is-hidden');

            function leePrecio(e) {
                
                $('.offerBox').html('');

                $.each(serviceArray, function(index, value) {
                    var id = value;
                     $.ajax({
                        type: "POST",
                        url: "admin/apo/carga-servicios.php",
                        data: JSON.stringify({
                            src: 1
                        }),
                        dataType: "json",
                        context: document.body,
                        success: function(result) {
                            
                            $.each(result, function(j, apo) {
                                if (apo.SERVICEID == id) {
                                    console.log(bundleArray[index]);
                                    $.each(resultBundle, function(i, val) {
                                        if (val['id'] == bundleArray[index]) {
                                            discountNow = val['discount'];
                                            console.log(val['discount']);
                                        }
                                    })
                                    servicename = apo.SERVICENAME;
                                    var finalPrice = precio*discountNow;
                                    finalPrice = finalPrice.toFixed(2);
                                    var text = '<div class="column is-12 has-background-white ma-one width-one__tworem">';
                                    text += '<div class="tile is-ancestor">';
                                    text += '<div class="tile is-parent is-3 has-text-centered">';
                                    text += '<div class="content is-relative width-one">';
                                    text += '<h2 class="has-text-weight-bold has-text-centered vertical-align-abs width-one">$<i class="precioDesbloqueo">' + finalPrice + '</i></h2>';
                                    text += '</div>';
                                    text += '</div>';
                                    text += '<div class="tile is-parent is-4">';
                                    text += '<div class="content is-relative">';
                                    text += '<p class="is-marginless is-capitalized is-size-7 has-text-weight-bold has-text-justified font-s-2">';
                                    text += '&nbsp;' + servicename + '&nbsp;';
                                    text += '<br><i class="font-color-1">12 A 24 horas</i>';
                                    text += '<br>DESBLOQUEO POR IMEI';
                                    text += '</p>';
                                    text += '</div>';
                                    text += '</div>';
                                    text += '<div class="tile is-parent is-5 background-p-2 handed payConfirm " price="' + finalPrice + '" sid=' + apo.SERVICEID + '>';
                                    text += '<div class="content is-relative width-one">';
                                    text += '<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white vertical-align-abs width-one">COMPRAR</p>';
                                    text += '</div>';
                                    text += '</div>';
                                    text += '</div>';
                                    text += '</div>';
                                    $('.offerBox').append(text);
                                }
                            });

                            $('.payConfirm').on('click',function(){
                                precio = $(this).attr('price');
                                sid = $(this).attr('sid');
                                confirmPayment();
                            });
                        },
                        error: function(xhr, tst, err) {
                          console.log(err);
                        }
                    })

                });

            }

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

};

//CARGA SELECTS DE PAISES Y OPERADORES
function cargaSelects(e) {

    $('.thirdStepOrder').addClass('is-hidden');


    //CARGA LOS PAISES DESDE JSON
    $.ajax({

        method: 'post',
        url: 'php/desbloquea/carga-paises.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            $("#selectCountry").append("<option class='cargaOperador' value='114'>M&eacute;xico</option>");
            $("#selectCountry").append("<option class='cargaOperador' value='187'>United States USA</option>");
            $("#selectCountry").append("<option class='dropdown-divider'></option>");
            $.each(data, function(index, pais) {
                $("#selectCountry").append("<option class='cargaOperador' value=" + pais.id + ">" + pais.pais + "</option>");
            });
            
            $('#selectCountry').on('change',function(){
                $("#selectOperator").html("");
                var paisSelect = $(this).val();
                //CARGA LOS OPERADORES DESDE JSON
                $.ajax({

                    method: 'post',
                    url: 'php/desbloquea/carga-operators.php',
                    dataType: "json",
                    context: document.body,
                    success: function(data) {

                        $.each(data, function(index, operator) {
                            if (operator.id_m == paisSelect) {
                                $("#selectOperator").append("<option value=" + operator.id + ">" + operator.operador + "</option>");
                            }
                        });

                        $('.thirdStepOrder').removeClass('is-hidden');

                    },
                    error: function(xhr, tst, err) {
                        console.log(err);
                    }

                });
            })

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

}

function cargaEquipos() {

    id = $('#id').html();
    marca = $('#marca').html();

    $.ajax({
        type: "POST",
        url: "php/desbloquea/carga-equipos.php",
        dataType: "json",
        context: document.body,
        success: function(data) {
            
            $('#desqloqueaBox').html('');
            
            $.each(data, function(index, marca) {
                if (marca.id_m == id) {
                    $('#desqloqueaBox').append(boxEquipos + '<img src="img/' + marca.img + '">' + '<p class="is-size-6 equipo-name" val=' + marca.id + '>' + marca.name + '</p>' + end);
                }
            });

            //CARGA LA MARCA SELECCIONADA
            $('.equipoName').on('click',function(){

                equipo = $(this).find('p').attr('val');

                secondStep();

            })

        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
    })

}

//CARGA PAISES Y OPERADORES SEGUNDO PASO ANTES DEL PAGO
function secondStep(){

    var url = 'desbloquear';
    var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
    '<input type="text" name="paso" value=2 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="equipo" value="' + equipo + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();

};

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
$('.thirdStepOrder').on('click',function(){

    var id = $('#equipo').html();
    var country = $('#selectCountry').val();
    var operator = $('#selectOperator').val();

    var url = 'desbloquear';
    var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
    '<input type="text" name="paso" value=3 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '<input type="text" name="equipo" value="' + id + '" />' +
    '<input type="text" name="pais" value="' + country + '" />' +
    '<input type="text" name="operator" value="' + operator + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();

});

//CARGA LA MARCA SELECCIONADA
function confirmPayment(e){

	marca = $('#marcaBrand').html();
	equipo = $('#equipoBrand').html();
    pais = $("#pais").html();
    operator = $("#operator").html();
    name = $("#name").val();
    imei = $("#imei").val();
	//precio = $(this).attr('price');

	var text = "<img class='img-center pa-one' src='img/logo_500.png'>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Your Name: <i class='has-text-white has-text-weight-bold is-marginless'>" + name + "</i></h5>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Email: <i class='has-text-white has-text-weight-bold is-marginless'>" + email + "</i></h5>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>IMEI: <i class='has-text-white has-text-weight-bold is-marginless is-size-4'>" + imei + "</i></h5>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Country: <i class='has-text-white has-text-weight-bold is-marginless'>" + pais + "</i></h5>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Operator: <i class='has-text-white has-text-weight-bold is-marginless'>" + operator + "</i></h5>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Unlock Service: <i class='has-text-white has-text-weight-bold is-marginless'>" + servicename + "</i></h5>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Equipment: <i class='has-text-white has-text-weight-bold is-marginless'>" + marca + ' - ' + equipo + "</i></h5>";
	text += "<h5 class='has-text-white has-text-right'>Cost: <b class='has-text-weight-semibold is-size-3'>$" + precio + "</b></h5>";
    text += "<input id='idServicio' type='hidden' value='"+ sid +"'>";

    //AGREGA CHECKS DE PAYPAL, MERCADOPAGO Y EFECTIVO
    text += "<div class='content has-text-centered pa-one'>";
    text += "<h5 class='is-size-5 has-text-white has-text-weight-light'>Select your payment method</h5>";
    text += "<div class='tile is-ancestor'>";
    text += "<div class='tile is-parent'>";
    text += "<div id='paypal-button' class='width-one pago-pay'></div>";
    text += "</div>";
    text += "<div class='tile is-parent'>";
    text += "<a class='pa-one width-one pago-mep'></a>";
    text += "</div>";
    text += "<div class='tile is-parent'>";
    text += "<a class='pa-one width-one pago-efe'></a>";
    text += "</div'>";
    text += "</div>";
    text += "</div>";
    
    text += "<small class='has-text-white'>Press 'ESC' to cancel your order</small>";

	// set content
	modal.setContent(text);
    $('.tingle-modal-box__content').addClass('background-p-1');
	modal.open();

    //CARGAMOS PAYPAL
    jQuery.getScript("scripts/desbloquea/checkOutPaypal.js", function(data, status, jqxhr) {
    });
    
    $('.pago-efe').on('click',function(){
        var operation = $('#idServicio').val();
        //PROCESA PAGO EFECTIVO
        var form_data = new FormData();

        form_data.append("imei", imei);
        form_data.append("email", email);
        form_data.append("nombre", name);
        form_data.append("marca", marca);
        form_data.append("equipo", equipo);
        form_data.append("precio", precio);
        form_data.append("pais", pais);
        form_data.append("operator", operator);
        form_data.append("operation", operation);

        //SOLICITAMOS EL CODIGO
        $.ajax({

            method: 'post',
            url: 'php/desbloquea/desbloquea-order.php',
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "html",
            context: document.body,
            success: function(data) {

                modal.close();
                
                $('#orderDiv').html('').delay(2000);
                $('#orderDiv').addClass('pa-one');
                $('#orderDiv').append('<h4 class="has-text-centered is-size-4">Tu n&uacute;mero de orden es: <b class="font-color-1">' + data + '</b></h4>');
                $('#orderDiv').append('<h5 class="has-text-centered is-size-5">Guardalo para futuras referencias</h5>');
                $('#orderDiv').removeClass('is-hidden');
                $('.has-background-white-ter').removeClass('is-hidden');

                $('#orderDiv').animate({ scrollTop: 0 }, 'fast');

            },
            error: function(xhr, tst, err) {
                console.log(err);
            }

        });
    })

};       

//CHECADORES DE INPUTS
//IMEI
$('#imei').on('keyup', function() {
    if (this.value.length < 15 || this.value.length > 15) {
    	$('.imeiCheck').removeClass('is-hidden');
    	$('.paymentCheck').addClass('is-hidden');
        $('.paymentCheck').removeClass('active-show');
        $('.paymentCheck').addClass('active-hide');
    } else {
    	$('.imeiCheck').addClass('is-hidden');
    	$('.paymentCheck').removeClass('is-hidden');
        $('.paymentCheck').removeClass('active-hide');
        $('.paymentCheck').addClass('active-show');
    }
    if (this.value.length == 0) {
    	$('.imeiCheck').addClass('is-hidden');
    	$('.paymentCheck').addClass('is-hidden');
        $('.paymentCheck').removeClass('active-show');
        $('.paymentCheck').addClass('active-hide');
    }
});

//EMAIL
$('#email').on('keyup', function() {
	email = $(this).val();
	if (validateEmail(email)) {
    	$('.emailCheck').addClass('is-hidden');
	} else {
    	$('.emailCheck').removeClass('is-hidden');
	}
    
    if (this.value.length == 0) {
    	$('.emailCheck').addClass('is-hidden');
    }
});

//CONFIMATION
$('#confirm').on('keyup', function() {
	email = $('#email').val();
	confirm = $(this).val();
	if (confirm != email) {
    	$('.confirmCheck').removeClass('is-hidden');
	} else {
    	$('.confirmCheck').addClass('is-hidden');
	}
    
    if (this.value.length == 0) {
    	$('.confirmCheck').addClass('is-hidden');
    }
});


function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}

function leeBundle() {

    $.ajax({

        method: 'post',
        url: 'php/desbloquea/lee-bundle.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            resultBundle = data;

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });


}