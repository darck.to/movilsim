<!--CINTILLO DE PAGOS-->
<div class="pa-one has-background-white is-mobile-blo-lef-wid">
  <div class="container pa-one">
    <div class="columns is-multiline is-vcentered">
      <div class="column is-two-third-mobile is-four-fifths pa-no has-text-centered">
        <div class="columns is-multiline is-mobile is-vcentered">
          <div class="column is-one-fifths">
            <div class="footer-text">
              <span class="has-text-weight-bold font-color-1 "><?php echo _PAYT1; ?></span>
            </div>
          </div>
          <div class="column width-mobile is-four-fifths has-text-centered">
            <div class="footer-payment">
              <img class="" src="img/ico/12.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/1.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/2.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/3.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/4.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/5.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/9.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/10.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/11.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/6.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/7.png" alt="payment-option">
            </div>
            <div class="footer-payment">
              <img class="" src="img/ico/8.png" alt="payment-option">
            </div>
          </div>
        </div>
      </div>
      <div class="column width-mobile is-one-fifth pa-no has-text-centered">
         <div class="columns is-multiline is-mobile is-vcentered">
           <div class="column width-mobile is-three-quarters">
             <div class="footer-text">
               <span class="has-text-weight-bold font-color-1"><?php echo _PAYT2; ?></span>
             </div>
           </div>
           <div class="column width-mobile is-one-quarter">
             <div class="footer-payment">
               <img src="img/footer-ssl.png">
             </div>
           </div>
         </div>
      </div>
    </div>
  </div>
</div>

<!--FOOTER-->
<footer class="footer has-background-black">
  <div class="container content">
    <div class="columns">
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _FMENU1; ?></h5>
    		<ul id="menuFooter">
    		</ul>
    	</div>
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _FMENU2; ?></h5>
    		<ul id="menuPop">
    		</ul>
    	</div>
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _FMENU3; ?></h5>
    		<ul>
    			<li><a class="has-text-grey-light" href="revisar"><?php echo _MENU2; ?></a></li>
          <li><a class="has-text-grey-light" href="cupones"><?php echo _MENU3; ?></a></li>
          <li><a class="has-text-grey-light" href="preguntas"><?php echo _MENU4; ?></a></li>
          <li><a class="has-text-grey-light" href="contacto"><?php echo _MENU5; ?></a></li>
    	    <li><a class="has-text-grey-light" href="pagos"><?php echo _MENU6; ?></a></li>
          <li><a class="has-text-grey-light" href="como"><?php echo _MENU7; ?></a></li>
          <li><a class="has-text-grey-light" href="nosotros"><?php echo _MENU8; ?></a></li>
    		</ul>
    	</div>
    	<div class="column">
        <h5 class="has-text-white"><?php echo _FMENU4; ?></h5>
        <ul class="ul-no">
          <li class="pa-b-one"><a class="has-text-grey-light is-relative" target="_blank" href="https://wa.me/5214941055849?text=I'm%20interested%20in%20your%20%20unlock%20services"><i class="social-icons social-whats fab fa-2x fa-whatsapp has-text-primary"></i><span class="po-a-t-9-l50">&nbsp;<?php echo _("Whatsapp"); ?></span></a></li>
          <li class="pa-b-one"><a class="has-text-grey-light is-relative" target="_blank" href="https://facebook.com"><i class="social-icons social-face fab fa-2x fa-facebook has-text-link"></i><span class="po-a-t-9-l50">&nbsp;<?php echo _("Facebook"); ?></span></a></li>
          <li class="pa-b-one"><a class="has-text-grey-light is-relative" target="_blank" href="https://twitter.com"><i class="social-icons social-twit fab fa-2x fa-twitter has-text-info"></i><span class="po-a-t-9-l50">&nbsp;<?php echo _("Twitter"); ?></span></a></li>
        </ul>
    		<img class="image" src="img/logo_500.png">
    	</div>
    </div>
  </div>
</footer>
