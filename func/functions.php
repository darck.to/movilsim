<?php

	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length = 8) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//FOLIOS ORDERS
	function leeFolio() {
		//NOMBRE DE ARCHIVO
		$filename = '../../admin/assets/folio.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio'];
		}
		return $folio;
	}
	function masFolio() {
		$filename = file_get_contents('../../admin/assets/folio.json');
		$data = json_decode($filename, true);
		$viejo = $data[0]['folio'];
		$data[0]['folio'] = $viejo + 1;
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
		file_put_contents('../../admin/assets/folio.json', $newJsonString);
	}

	//LIMPIAMOS LAS VARIABLES
	function html_escape($html_escape) {
        $html_escape =  htmlspecialchars($html_escape, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        return $html_escape;
    }

  function getPreferredLanguage() {
		$langs = array();
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			// break up string into pieces (languages and q factors)
			preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i',
			        $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
			if (count($lang_parse[1])) {
			    // create a list like "en" => 0.8
			    $langs = array_combine($lang_parse[1], $lang_parse[4]);
			    // set default to 1 for any without q factor
			    foreach ($langs as $lang => $val) {
			        if ($val === '') $langs[$lang] = 1;
			    }
			    // sort list based on value
			    arsort($langs, SORT_NUMERIC);
			}
		}
		//extract most important (first)
		foreach ($langs as $lang => $val) { break; }
		//if complex language simplify it
		if (stristr($lang,"-")) {$tmp = explode("-",$lang); $lang = $tmp[0]; }
		return $lang;
	}

	 //LEE EL PAIS O EL OPERADOR
    function leePais($id) {
		//NOMBRE DE ARCHIVO
		$str = $id;
		$filename = '../../php/desbloquea/files/paises.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			foreach ($data as $content) {
				if ($content['id'] == $id) {
					$str = $content['pais'];
				}
			}
		}
		return $str;
	}
	function leeOperador($id) {
		//NOMBRE DE ARCHIVO
		$str = $id;
		$filename = '../../php/desbloquea/files/operadores.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			foreach ($data as $content) {
				if ($content['id'] == $id) {
					$str = $content['operador'];
				}
			}
		}
		return $str;
	}

	function cargaIdMarca($id) {
		$str = $id;
		$filename = "php/home/files/marcas.json";
		if (file_exists($filename)) {
			$filename = file_get_contents($filename, true);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				if (strcasecmp($content['name'],$str) == 0) {
					$value = $content['id'];
				}
			}
		}
		return $value;
	}

	function cargaNombreMarca($id) {
		$str = $id;
		$filename = "php/home/files/marcas.json";
		if (file_exists($filename)) {
			$filename = file_get_contents($filename, true);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				if ($content['id'] == $str) {
					$value = $content['name'];
				}
			}
		}
		return $value;
	}

	function cargaIdEquipo($marca, $equipo) {
		$filename = "php/desbloquea/files/marcas.json";
		if (file_exists($filename)) {
			$filename = file_get_contents($filename, true);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				if ($content['id_m'] == $marca && strcasecmp($content['name'],$equipo) == 0) {
					$value = $content['id'];
				}
			}
		}
		return $value;
	}

	function cargaNombreEquipo($id) {
		$filename = "php/desbloquea/files/marcas.json";
		if (file_exists($filename)) {
			$filename = file_get_contents($filename, true);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				if ($content['id'] == $id) {
					$value = $content['name'];
				}
			}
		}
		return $value;
	}

	function cargaImgEquipo($id) {
		$filename = "php/desbloquea/files/marcas.json";
		if (file_exists($filename)) {
			$filename = file_get_contents($filename, true);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				if ($content['id'] == $id) {
					$value = $content['img'];
				}
			}
		}
		return $value;
	}

?>
