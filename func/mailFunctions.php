<?php
	include_once("functions.php");
  include_once "../../locale/".getPreferredLanguage().".php";

	//BUSCAMOS LA INFORMACION DE PERFIL EN BASE AL LINKU
	function enviaCorreo($asunto,$cuerpo,$mail) {
		// Enviamos por email la nueva contraseña
		$remite_nombre = "Movilsim";
		$remite_email = "no-reply@movilsim.com.mx";
		$mensaje = cuerpoMail($cuerpo);
		$cabeceras = "From: ".$remite_nombre." <".$remite_email.">\r\n";
		$cabeceras = $cabeceras."Mime-Version: 1.0\n";
		$cabeceras = $cabeceras."Content-Type: text/html";
		//CORREO DEL REMITENTE
		$usuario_email = $mail;
		$enviar_email = mail($usuario_email,$asunto,$mensaje,$cabeceras);

		if ($enviar_email) {
			echo "<h3 class=\"has-text-weight-light is-size-4\">"._MAIL01."</h3>";
		} else {
			echo "<h3 class=\"has-text-weight-light is-size-4\">"._MAIL02."</h3>";
		}

		$enviar_email = mail("hectorhackhd@gmail.com",$asunto,$mensaje,$cabeceras);
	}

	function cuerpoMail($cuerpo) {
		$logo = "
			<a style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent;' href=''><img style='display: block; margin: 0 auto; height: 160px;' src='http://movilsim.com/img/logo_500.png'/></a>
		";
		$bgInit = "
			<div style='position: relative; width: 100%; height: 900px; padding-top: 10px; background-color: #00b7dd;'>
		";
		$body = "
				<div style='position: relative; margin: 0 auto; width: 420px; height: 700px; z-index: 2;'>
		";
		$header = "
					<div style='position: relative; width: calc(100% - 30px); height: 187px; padding: 15px; background-color: #00b7dd; color: white; text-align: center; font-size: 1.5rem; font-weight: lighter;'>".$logo."</div>
		";
		$content = "
					<div style='position: relative; width: calc(100% - 30px); height: auto; padding: 15px; background-color: #f5f5f5; color: #4A4A4A; text-align: left; font-weight: lighter; font-size: 1.1rem;'>".$cuerpo."</div>
		";
		$footer = "
					<div style='position: relative; width: calc(100% - 30px); height: 50px; padding: 15px; color: #4A4A4A; background-color: #0a0a0a;'>
						<a href='http://movilsim.com' style='float: left; position: relative; color: #ffffff; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'>movilSim</a>
						<i style='float: right; position: relative'>Todos los Derechos Reservados 2020</i>
					</div>
		";
		$bodyClose = "
				</div>
		";
		$bgClose = "
			</div>
		";
		return $bgInit.$body.$header.$content.$footer.$bodyClose.$bgClose;
	}
?>
