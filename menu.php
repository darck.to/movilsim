<nav class="navbar background-p-1" role="navigation" aria-label="main navigation">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item has-text-white has-text-weight-bold logo-menu" href="."></a>

      <a role="button" class="navbar-burger burger has-text-weight-bold has-text-white" aria-label="menu" aria-expanded="false" data-target="navbarBasic">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBasic" class="navbar-menu background-p-1">
      <div class="navbar-end">

        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link navbar-item has-text-white has-text-weight-bold">
            <?php echo _MENU1; ?>
          </a>
          <div id="desbloqueaMarcasMenu" class="navbar-dropdown">
          </div>
        </div>

        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-item has-text-white has-text-weight-bold">
            <a class="navbar-link navbar-item has-text-white has-text-weight-bold">
              <?php echo _MENU2; ?>
            </a>
            <div class="navbar-dropdown">
              <a class="navbar-item hoverBlue" href="revisar.php"><?php echo _MENU2S1; ?></a>
              <a class="navbar-item hoverBlue" href="operator.php"><?php echo _MENU2S2; ?></a>
              <a class="navbar-item hoverBlue" href="model.php"><?php echo _MENU2S3; ?></a>
            </div>
          </a>
        </div>

        <a class="navbar-item has-text-white has-text-weight-bold" href="cupones.php">
          <?php echo _MENU3; ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold" href="preguntas.php">
          <?php echo _MENU4; ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold" href="contacto.php">
          <?php echo _MENU5; ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold" href="perfil.php">
          <i class="fas fa-user"></i>
        </a>
      </div>
    </div>
  </div>
</nav>
