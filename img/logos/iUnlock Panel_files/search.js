// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        $('#unlockCode').remove();
		$('.sendUnlockCode').remove();
        return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {
    
    modal.close();

});

$(document).ready(function() {

	$.expr[":"].contains = $.expr.createPseudo(function(arg) {
	    return function( elem ) {
	        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	    };
	});

	var jsonSearch;

	$.ajax({
		url: "php/search/search.php",
		dataType: "json",
		success: function(html){
			jsonSearch = html;
		}
	});

	//GENERAL SEARCH
	$("input.omniSearch").on("keyup", function(e) {
		// Set Search String
		var search_string = $(this).val();
		
		if (e.which == 13) {
		    buscando(search_string);
			$('#results').html('');
			return false;    //<---- Add this line
		}

		// Do Search
		if (search_string == '') {
			$('#results').html('');
			$("#results").fadeOut();
			$('#results-container').addClass('hide');
		}else{
			$("#results").fadeIn();
			$('#results-container').removeClass('hide');
            $.each(jsonSearch, function (id, key) {
            	$.each(this, function (name, value) {
            		if (key.Name == 'IMEI Request') {var kind = 'check'} else {var kind = 'order'}
            		str = JSON.stringify(value);
            		if (str.toLowerCase().indexOf(search_string) >= 0) {
            			$('#results').append('<h6 class="searchItem" val=' + key.Folio + '_' + kind + '_' + key.Order + '><b>' + name + ': ' + value + '</b></h6>');
            		}
            	});
            });
		}

		//ELIMINADOR CUALQUIERA QUE YA NO COINCIDA
		$('#results h6:not(:contains(' + search_string + '))').remove();
		var seen = {};
		$('#results h6').each(function() {
		    var txt = $(this).text();
		    if (seen[txt])
		        $(this).remove();
		    else
		        seen[txt] = true;
		});

		//BUSQUEDA DE ITEMS
		$(".searchItem").on("click", function(e) {

			var find = $(this).attr('val');

			$.ajax({
				type: "POST",
				url: "php/search/search-data.php",
				data: { find: find },
				cache: false,
				success: function(data){

					var modalContent;

					$.each(data, function(index, marca) {
						modalContent = '<div class="row">';
						modalContent += '<div class="col s12 m4 offset-m2">';
						modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + marca['Folio'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + marca['Date'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Client: </em></i><i class="right"><b>' + marca['Name'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Email: </em></i><i class="right grey-text text-darken-3"><b>' + marca['Email'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Order No.: </em></i><i class="right"><b id="orderNo">' + marca['Order'] + '</b></i></p>';
						modalContent += '</div>';
						modalContent += '<div class="col s12 m4">';
						modalContent += '<p class="fiv-p"><i><em>IMEI: </em></i><i class="right grey-text text-darken-3"><b>' + marca['IMEI'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Company: </em></i><i class="right"><b>' + marca['Brand'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Model: </em></i><i class="right"><b>' + marca['Equipment'] + '</b></i></p>';
						modalContent += '<p class="fiv-p"><i><em>Cost: </em></i><i class="right"><b>$&nbsp;' + marca['Cost'] + '</b></i></p>';
						//STATUS Y COLOR DEL ESTATUS
						if (marca['Status'] == 0) {var color = 'red-text text-lighten-2';} else if (marca['Status'] == 1) { var color = 'green-text text-darken-1';}
						modalContent += '<p class="fiv-p"><i><em>Status: </em></i><i class="right"><i class="material-icons '+color+'">payment</i><b></b></i></p>';
						modalContent += '<input id="codeUnlock" type="hidden" value="' + marca['Code'] + '">';
						modalContent += '</div>';
						modalContent += '</div>';
					});

					//SET MODAL
					modal.setContent(modalContent);

					modal.open();

				}
			});


		});

	});

});



function buscando(cadena) {

	var cadenaBuscada = cadena;

	if (cadenaBuscada == "") {

		M.toast({html: "Busqueda Vacia!"});
		return false;

	}

	$.ajax({
		type: "POST",
		url: "buscador.php",
		data: { busca: cadenaBuscada },
		cache: false,
		success: function(data){
			$('.lds-ripple').css('visibility','visible');
			$("#navres").fadeOut();
			$('#navres-container').addClass('hide');
			$("#bodyContent").html(data);
		}
	});
}