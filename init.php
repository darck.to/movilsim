<!--FontAwesome-->
<link type="text/css" rel="stylesheet" href="css/all.min.css"  media="screen,projection"/>
<!--Import tingle.css-->
<link type="text/css" rel="stylesheet" href="css/tingle.css"  media="screen,projection"/>
<!-- jQuery -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<!--Tingle-->
<script type="text/javascript" src="js/tingle.js"></script>
<!--Pajinator-->
<script type="text/javaScript" src="js/pajinator.js"></script>

<script type="text/javaScript" src="scripts/onLoad.js"></script>
