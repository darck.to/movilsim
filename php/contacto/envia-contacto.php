<?php

	include_once('../../func/functions.php');

	if (empty($_POST['mail'])) {die("Error, contact support"); }
	if (empty($_POST['message'])) {die("Error, contact support"); }
	if (empty($_POST['nombre'])) {die("Error, contact support"); }

	$nombre = html_escape($_POST['nombre']);
	$mail = html_escape($_POST['mail']);
	$message = html_escape($_POST['message']);

	date_default_timezone_set("America/Mexico_City");
	$fecha = Date('Y-m-d H:i:s');
	$ndate = Date('YmdHis');

	//GUARDAMOS EL ARCHIVO DE LA NOTA
    $cabecera[] = array('fecha'=> $fecha, 'email'=> $mail, 'nombre'=> $nombre, 'message'=>$message, 'answer'=>'', 'status'=>0);

	//NOMBRE DE ARCHIVO
	$fileName = '../../admin/assets/message/message'.$ndate.'.json';
	//INCREMENTAMOS EL FOLIO
	masFolio();
	if (file_exists($fileName)) {
	} else {
		$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
		fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
		fclose($fileFinal);
	}

	//ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Mensaje en proceso";
	$mensaje = "<h1 style='font-weight: lighter;'>Gracias por contactarte con nosotros!</h1>
				<h2>".$nombre.",</h2>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Te avisaremos en cuanto tu mensaje sea respondido</p>
				<div style='display: block; padding: 5px 0;'></div>
				<div style='display: block; padding: 10px 0;'></div>
				<div style='display: block; padding: 10px 0;'></div>
				<p style='font-size: .8rem'>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de movilSim para poder auxiliarte: <a href='mailto:ordenes@movilsim.com' style='color: #00b7dd; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$mail);

	echo "<div class=\"column has-text-weight-light has-text-centered\"><h4>". _CONTRES1 ."</h4><small class=\"font-color-2\">&nbsp;". _CONTRES2 ."</small></div>";

?>
