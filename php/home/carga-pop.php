<?php
	
	header('Content-type: application/json');
	
   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists('../../admin/assets/popular.json')) {
		
		$filename = file_get_contents('../../admin/assets/popular.json');
		$json = json_decode($filename, true);

	}

	echo json_encode($json);

?>