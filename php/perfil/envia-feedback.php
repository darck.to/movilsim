<?php
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	include('../../func/abre_conexion.php');

	$fol = mysqli_real_escape_string($mysqli, $_POST['folio']);
	$order = mysqli_real_escape_string($mysqli, $_POST['order']);
	$message = mysqli_real_escape_string($mysqli, $_POST['message']);

	$file = '../../admin/assets/'.$fol.'_order_'.$order.'.json';
	$filename = file_get_contents($file);
	$data = json_decode($filename, true);

	$data[0]['comentario'] = $message;
	
	//LO VOLVEMOS A GUARDAR
	$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
	if ( file_put_contents($file, $newJsonString) ) {
		echo "Feedback submited";
	} else {
		echo "Error, contact support";
	}

    include('../../func/cierra_conexion.php');

?>