<?php

	if (empty($_POST['mail'])) {die("Error, contact support"); }
	if (empty($_POST['order'])) {die("Error, contact support"); }

	//RECIBIMOS LA INFORMACION
	include_once('../../func/functions.php');
	$mail = html_escape($_POST['mail']);
	$order = html_escape($_POST['order']);


	//INICIALIZACIÓN DE VARIABLE
	$aprobacion = 0;
	$fileList = glob('../../admin/assets/*.json');

   	//ORDERS
	foreach($fileList as $filename){

		if (file_exists($filename)) {

			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				//SI ENCUENTRA UNA COINCIDENCA DE MAIL Y ORDER SALVA LOS QUE COINCIDAN CON ESE MAIL
				if ($content['email'] == $mail && $content['random'] == $order) {

					$aprobacion = 1;

				}

			}

		}

	}

	if ($aprobacion == 1) {
		//SESSION VALUES
		session_start();
		$_SESSION['user'] = $mail;
	}

	echo ($aprobacion);

?>
