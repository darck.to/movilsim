<?php
	
	session_start();
	include_once('../../func/functions.php');

	if (empty($_POST['message'])) {die("Error, contact support"); }
	if (empty($_POST['nombre'])) {die("Error, contact support"); }
	
	$nombre = html_escape($_POST['nombre']);
	$mail = $_SESSION['user'];
	$message = html_escape($_POST['message']);

	//FECHA
	date_default_timezone_set("America/Mexico_City");
	$fecha = Date('Y-m-d H:i:s');
	$ndate = Date('YmdHis');

	//REVISAMOS SI EL USUARIO TIENE ALGUNA PREGUNTA ABIERTA
	$comprobacion = 1;

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../admin/assets/questions/question*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename) {
	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				if ($content['email'] == $mail) {
					if ($content['status'] == 0) {
						$comprobacion = 0;
						exit("<div class=\"column has-text-weight-light has-text-centered\"><h4>You can't make another question, please wait for the answer to your previous one</h4><small class=\"font-color-2\">&nbsp;We'll send you a message via email when that happens</small></div>");
					} elseif ($content['status'] == 1) {
						$comprobacion = 1;
					}
				} 
			}
		} else {
			$comprobacion = 1;
		}
	}

	if ($comprobacion == 1 ) {

		//GUARDAMOS EL ARCHIVO DE LA NOTA
	    $cabecera[] = array('email'=> $mail, 'usuario'=> $nombre, 'mensaje'=>$message, 'fecha'=> $fecha, 'answer'=>'', 'status'=>0);
		
		//NOMBRE DE ARCHIVO
		$fileName = '../../admin/assets/questions/question'.$ndate.'.json';
		if (file_exists($fileName)) {
		} else {
			$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
			fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
			fclose($fileFinal);
		}

		//ENVIAMOS EL MAIL DE CONFIRMACION//
		////////////////////////////////////
		$asunto = "Mensaje en proceso";
		$mensaje = "<h1 style='text-align: center; font-weight: lighter;'>movilSIM</h1>
					<p><h2>".$nombre.", gracias por tu mensaje</h2></p>
					<div style='display: block; padding: 5px 0;'></div>
					<p>Te avisaremos en cuanto tu mensaje sea respondido</p>
					<div style='display: block; padding: 5px 0;'></div>
					<div style='display: block; padding: 10px 0;'></div>
		";
		include_once('../../func/mailFunctions.php');
		enviaCorreo($asunto,$mensaje,$mail);

		echo "<div class=\"column has-text-weight-light has-text-centered\"><h4>Message sent!</h4><small class=\"font-color-2\">&nbsp;We will send you a message via email when your response its available</small></div>";

	}

?>