<?php

	session_start();
	
	header('Content-type: application/json');

	if (isset($_SESSION['user'])) {

		$mail = $_SESSION['user'];
		
		//INICIALIZACIÓN DE VARIABLE
		$resultados = [];
		$fileList = glob('../../admin/assets/*_order_*.json');

		foreach($fileList as $filename){

			if (file_exists($filename)) {
				
				$filename = file_get_contents($filename);
				$json = json_decode($filename, true);

				foreach ($json as $content) {
				
					if ($content['email'] == $mail) {

						$resultados[] = array('folio' => $content['folio'],'fecha' => $content['fecha'],'order' => $content['random'],'imei' => $content['imei'],'marca' => $content['marca'],'equipo' => $content['equipo'],'precio' => $content['precio'],'estatus' => $content['estatus'],'code' => $content['code'], 'serviceinfo'=>$content['serviceinfo'], 'mporder'=>revisaOrderMP($content['mporder']), 'comentario'=>$content['comentario']);
						
					}

				}

			}

		}

		$fileList = glob('../../admin/assets/*_check_*.json');

	   	//ORDERS
		foreach($fileList as $filename){

			if (file_exists($filename)) {
				
				$filename = file_get_contents($filename);
				$json = json_decode($filename, true);

				foreach ($json as $content) {
				
					if ($content['email'] == $mail) {
						
						$resultados[] = array('folio' => $content['folio'],'fecha' => $content['fecha'],'order' => $content['random'],'imei' => $content['imei'],'marca' => "&nbsp;",'equipo' => "&nbsp;",'precio' => 0,'estatus' => $content['estatus'],'code' => $content['code']);

					}

				}

			}

		}

		$resultados = array_map('array_values', $resultados);
		$resultados = array_values($resultados);
		print json_encode($resultados);

	}

	function revisaOrderMP($order) {
		if($order <> 0) {
		    require '../../vendor/autoload.php';
		    // Agrega credenciales
		    MercadoPago\SDK::setAccessToken('APP_USR-2084779251716512-082715-c191cac961cc369255952085dacabe72-456348368');
		    $merchant_order = null;
			$payment = MercadoPago\Payment::find_by_id($order);
		    //COMPRABAMOS EL TICKET
		    $urlTicket = $payment->transaction_details->external_resource_url;
		} else {
			$urlTicket = '#';
		}
	    return $urlTicket;
	}

?>