<?php
  include_once('../../func/functions.php');
  if (empty($_POST['email'])) {die("Falta el Email"); }
  //RECIBIMOS POR POST LOS DATOS DE LA TRANSACCION
  $email = html_escape($_POST['email']);
  $nombre = html_escape($_POST['nombre']);
  $precio = $_POST['precio'];
  $precio = (float)$precio;
  $coin = html_escape($_POST['coin']);
  $random = html_escape($_POST['random']);
  $folio = html_escape($_POST['folio']);
  date_default_timezone_set("America/Mexico_City");
  $fecha = Date('Y-m-d H:i');
  // Requerimos el archivo autoload dentro de la carpeta 'vendor' para poder usar el SDK
  require '../../vendor/autoload.php';
  // Agrega credenciales
  MercadoPago\SDK::setAccessToken('APP_USR-2084779251716512-082715-c191cac961cc369255952085dacabe72-456348368');
  // Crea un objeto de preferencia
  $preference = new MercadoPago\Preference();
  // Crea un ítem en la preferencia
  $item = new MercadoPago\Item();
  $item->title = 'MovilSim Unlock Service: '.$random;
  $item->quantity = 1;
  $item->unit_price = $precio;
  $item->currency_id = 'USD';
  $preference->items = array($item);
  $payer = new MercadoPago\Payer();
  $payer->name = $nombre;
  $payer->email = $email;
  $payer->date_created = $fecha;
  $payer->phone = array(
    "area_code" => "",
    "number" => ""
  );
  $preference->back_urls = array(
    "success" => "php/revisa/mp/success.php?order=".$random."&folio=".$folio,
    "failure" => "php/revisa/mp/failure.php?order=".$random."&folio=".$folio,
    "pending" => "php/revisa/mp/pending.php?order=".$random."&folio=".$folio
  );
  $preference->auto_return = "approved";
  $preference->save();
  echo $preference->init_point;

?>
