<?php

	include_once('../../func/functions.php');

	if (empty($_POST['imei'])) {die("Falta el IMEI"); }
	if (empty($_POST['email'])) {die("Falta el Email"); }
	if (empty($_POST['value'])) {die("Falta el Servicio"); }

	//LEEMOS EL ULTIMO FOLIO
	$folio = leeFolio();

	//RECIBIMOS LA INFORMACION
	$src = html_escape($_POST['src']);
	$imei = html_escape($_POST['imei']);
	$email = html_escape($_POST['email']);
	$value = html_escape($_POST['value']);

  date_default_timezone_set("America/Mexico_City");
	$fecha = Date('Y-m-d H:i');
	$mes = Date('m');

	//ID ALEATORIO
	$random = generateRandomString();

	//EJECUTAMOS LA LLAMADA A LA API
	$service =  $value; //"3" ej APi Service iD
  $format = "json"; // $format = "html"; display result in JSON or HTML format
  $imei = $imei; // IMEI or SERIAL Number
  $api = "J6V-6X5-KPT-KVA-9LI-MNK-3QX-LL9"; // APi Key

  $curl = curl_init ("https://sickw.com/api.php?format=$format&key=$api&imei=$imei&service=$service");
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
  curl_setopt($curl, CURLOPT_TIMEOUT, 60);
  $result = curl_exec($curl);
  curl_close($curl);

  //echo $result; // Here the result is printed


	//GUARDAMOS EL ARCHIVO DE LA NOTA
  $cabecera[] = array('folio'=> $folio,'fecha'=> $fecha, 'mes'=> $mes, 'random'=> $random, 'imei'=> $imei, 'email'=> $email, 'nombre'=> 'IMEI Request', 'precio'=> 0, 'fuente'=>$src, 'comentario'=> '', 'estatus' => 0, 'serviceinfo'=>$result, 'code' => '');

	//NOMBRE DE ARCHIVO
	$fileName = '../../admin/assets/'.$folio.'_check_'.$random.'.json';
	//INCREMENTAMOS EL FOLIO
	masFolio();
	if (file_exists($fileName)) {
	} else {
		$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
		fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
		fclose($fileFinal);
		echo $random."\n";
	}

	//ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Solicitud en Proceso";
	$mensaje = "<h1 style='font-weight: lighter;'>Gracias por confiar en nosotros!</h1>
				<h2 style='font-weight: lighter;'>Guarda tu n&uacute;mero de orden:</h2>
				<h3 style='text-align: center;'><b>".$random."</b></h3>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Te avisaremos en cuanto tengamos respuesta de tu Solicitud</p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
				<div style='display: block; padding: 10px 0;'></div>
				<p style='font-size: .8rem'>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de movilSim para poder auxiliarte: <a href='mailto:ordenes@movilsim.com' style='color: #00b7dd; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$email);

?>
