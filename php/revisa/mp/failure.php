<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  class="background-p-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<head>
	<base href="." />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<link href="../../../estilos/reset.css" type="text/css" rel="stylesheet">

	<!--Import bulma.css-->
	<link type="text/css" rel="stylesheet" href="../../../css/bulma.css"  media="screen,projection"/>
	<!--FontAwesome-->
	<link type="text/css" rel="stylesheet" href="../../../css/all.min.css"  media="screen,projection"/>
	<!--Import tingle.css-->
	<link type="text/css" rel="stylesheet" href="../../../css/tingle.css"  media="screen,projection"/>

	<link href="../../../estilos/estilos.css" type="text/css" rel="stylesheet">
	<link rel="icon" type="image/png" href="../../../favicon.png">

	<!--[if lt IE 9]>
	    <script src="scripts/html5shiv.js"></script>
	<![endif]-->
	<title>MovilSIM</title>
</head>

<!-- seccion de lenguajes-->
<?php include_once('../../../lang.php'); ?>

<body>

	<?php

		error_reporting(E_ALL);
		ini_set('display_errors', 1);

		include('../../../func/abre_conexion.php');
		//mine
		$order = mysqli_real_escape_string($mysqli, $_GET['order']);
		$folio = mysqli_real_escape_string($mysqli, $_GET['folio']);
		$mporder = mysqli_real_escape_string($mysqli, $_GET['collection_id']);

		$filename = file_get_contents('../../../admin/assets/'.$folio.'_check_'.$order.'.json');
		$finaldestination = '../../../admin/assets/'.$folio.'_order_'.$order.'.json';
		$data = json_decode($filename, true);

		$data[0]['estatus'] = 0;
		$data[0]['mporder'] = $mporder;

		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
		if ( file_put_contents($finaldestination, $newJsonString) ) {
			//echo "Order changed";
			$result = _ORDERMOD11;
		} else {
			//echo "Error, contact support";
			$result = _ORDERMOD14;
		}

	    include('../../../func/cierra_conexion.php');

	?>

	<div class="section">
		<div class="container">
			<div class="content">
				<img class='img-center pa-one' src='../../../img/logo_500.png'>
			</div>
			<div class="columns is-multiline has-background-white radius-5 shadow-blue-darker pa-one">
				<div class="columns">
					<div class="column is-10 is-offset-1">
						<h3 class='title has-text-centered has-text-weight-light is-size-2'><?php echo _ORDERMOD9; ?>&nbsp;<b class='font-color-2'><?php echo $order; ?></b></h3>
				        <h6 class='has-text-centered has-text-weight-light is-size-6'><?php echo _ORDERMOD9S1; ?></h6>
				        <h4 class='has-text-centered has-text-weight-light is-size-3'><?php echo _ORDERMOD10; ?></h4>
					</div>
				</div>
				<div class="columns">
					<div class="column is-10 is-offset-1">
						<h4 class="title has-text-centered has-text-weight-light is-size-3"><?php echo $result; ?></h4>
					</div>
				</div>
			</div>
			<div class="content has-text-centered ma-one pa-one">
				<a class="button is-large has-text-link" href="https://movilsim.com"><?php echo _ORDERBRET1; ?></a>
			</div>
		</div>
	</div>

</body>

</html>
