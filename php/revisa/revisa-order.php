<?php

	include_once('../../func/functions.php');
	include_once('../../func/abre_conexion.php');

	if (empty($_POST['imei'])) {die("Falta el IMEI"); }
	if (empty($_POST['email'])) {die("Falta el Email"); }
	if (empty($_POST['price'])) {die("Falta el Servicio"); }

	//LEEMOS EL ULTIMO FOLIO
	$folio = leeFolio();

	//RECIBIMOS LA INFORMACION
	$src = mysqli_real_escape_string($mysqli, $_POST['src']);
	$imei = mysqli_real_escape_string($mysqli, $_POST['imei']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$price = mysqli_real_escape_string($mysqli, $_POST['price']);
	$value = mysqli_real_escape_string($mysqli, $_POST['value']);

	//PAYPAL AUTH
	if (empty($_POST['auth'])) {} else {$msg = _ORDERMOD13;}

  date_default_timezone_set("America/Mexico_City");
	$fecha = Date('Y-m-d H:i');
	$mes = Date('m');

	//ID ALEATORIO
	$random = generateRandomString();
	$result = "";


  //echo $result; // Here the result is printed

	//GUARDAMOS EL ARCHIVO DE LA NOTA
	$sqlPro = $mysqli->query("INSERT INTO chek_movi (folio, fecha, mes, random, imei, email, nombre, precio, fuente, comentario, estatus, serviceinfo, code) VALUES ($folio, '".$fecha."', $mes, '".$random."', '".$imei."', '".$email."', 'IMEI Request', 0, $src, '', 0, '".$result."', '')");
	if($sqlPro) {
		masFolio();
		echo "<i class='idVenta' folio='" . $folio . "'>" . $random . "</i>&nbsp;\n&nbsp " . $msg;
		echo $random."\n";
	} else {
		echo mysqli_error($mysqli);
	}

  //$cabecera[] = array('folio'=> $folio,'fecha'=> $fecha, 'mes'=> $mes, 'random'=> $random, 'imei'=> $imei, 'email'=> $email, 'nombre'=> 'IMEI Request', 'precio'=> 0, 'fuente'=>$src, 'comentario'=> '', 'estatus' => 0, 'serviceinfo'=>$result, 'code' => '');

	//NOMBRE DE ARCHIVO
	//$fileName = '../../admin/assets/'.$folio.'_check_'.$random.'.json';
	//INCREMENTAMOS EL FOLIO
	//masFolio();
	//if (file_exists($fileName)) {
	//} else {
	//	$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
	//	fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
	//	fclose($fileFinal);
	//	echo "<i class='idVenta' folio='" . $folio . "'>" . $random . "</i>&nbsp;\n&nbsp " . $msg;
	//	echo $random."\n";

	include('../../func/cierra_conexion.php');

	//ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Solicitud en Proceso";
	$mensaje = "<h1 style='font-weight: lighter;'>Gracias por confiar en nosotros!</h1>
				<h2 style='font-weight: lighter;'>Guarda tu n&uacute;mero de orden:</h2>
				<h3 style='text-align: center;'><b>".$random."</b></h3>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Te avisaremos en cuanto tengamos respuesta de tu Solicitud</p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
				<div style='display: block; padding: 10px 0;'></div>
				<p style='font-size: .8rem'>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de movilSim para poder auxiliarte: <a href='mailto:ordenes@movilsim.com' style='color: #00b7dd; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$email);

?>
