<?php

	include_once('../../func/functions.php');
	include_once('../../func/abre_conexion.php');

	if (empty($_POST['imei'])) {die("Falta el IMEI"); }
	if (empty($_POST['email'])) {die("Falta el Email"); }

	//LEEMOS EL ULTIMO FOLIO
	$folio = leeFolio();

	//RECIBIMOS LA INFORMACION
	$imei = mysqli_real_escape_string($mysqli, $_POST['imei']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$nombre = mysqli_real_escape_string($mysqli, $_POST['nombre']);
	$marca = mysqli_real_escape_string($mysqli, $_POST['marca']);
	$equipo = mysqli_real_escape_string($mysqli, $_POST['equipo']);
	$precio = mysqli_real_escape_string($mysqli, $_POST['precio']);
	$coin = mysqli_real_escape_string($mysqli, $_POST['coin']);
	$pais = mysqli_real_escape_string($mysqli, $_POST['pais']);
	$operator = mysqli_real_escape_string($mysqli, $_POST['operator']);
	$serviceid = mysqli_real_escape_string($mysqli, $_POST['serviceid']);
	$servicename = mysqli_real_escape_string($mysqli, $_POST['servicename']);

	//PAYPAL AUTH
	if (empty($_POST['auth'])) {} else {$msg = _ORDERMOD13;}

  date_default_timezone_set("America/Mexico_City");
	$fecha = Date('Y-m-d H:i');
	$mes = Date('m');

	//ID ALEATORIO
	$random = generateRandomString();

	//GUARDAMOS EL ARCHIVO DE LA NOTA
	$sqlPro = $mysqli->query("INSERT INTO ord_movi (folio, fecha, mes, random, imei, email, nombre, marca, equipo, pais, operator, precio, coin, serviceid, servicename, comentario, estatus, serviceanswer, serviceinfo, mporder, code) VALUES ( $folio, '".$fecha."', $mes, '".$random."', '".$imei."', '".$email."', '".$nombre."', '".$marca."', '".$equipo."', $pais, $operator, $precio, '".$coin."', $serviceid, '".$servicename."', '', 0, 0, 0, 0, '')");
	if($sqlPro) {
		masFolio();
		echo "<i class='idVenta' folio='" . $folio . "'>" . $random . "</i>&nbsp;\n&nbsp " . $msg;
	} else {
		echo mysqli_error($mysqli);
	}

  //$cabecera[] = array('folio'=> $folio,'fecha'=> $fecha, 'mes'=> $mes, 'random'=> $random, 'imei'=> $imei, 'email'=> $email, 'nombre'=> $nombre, 'marca'=> $marca, 'equipo'=> $equipo, 'pais'=> $pais, 'operator'=> $operator, 'precio'=> $precio, 'coin'=> $coin, 'serviceid' => $serviceid, 'servicename' => $servicename, 'comentario'=> '', 'estatus' => 0, 'serviceanswer'=> 0, "serviceinfo"=> 0, 'mporder' => 0, 'code' => 0);

	//NOMBRE DE ARCHIVO
	//$fileName = '../../admin/assets/'.$folio.'_order_'.$random.'.json';
	//INCREMENTAMOS EL FOLIO
	//if (file_exists($fileName)) {
	//} else {
	//	$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
	//	fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
	//	fclose($fileFinal);
	//	echo "<i class='idVenta' folio='" . $folio . "'>" . $random . "</i>&nbsp;\n&nbsp " . $msg;

		//CUENTA LOS POPULARES PARA EL FOOTER
		$filename = '../../admin/assets/popular.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			foreach ($data as $content) {
				if ($data[0]['marca'] == $marca) {
					//NO HACE NADA AL ESTAR DUPLICADA LA ENTRADA
				} else {
					$change = array("marca"=>$marca);
					$data[] = $change;
					//AL SER UNA ENTRADA NUEVA ELIMINAMOS EL PRIMER REGISTRO
					$data = array_slice($data, 1);
					//LO VOLVEMOS A GUARDAR CON EL REGISTRO NUEVO
					$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
					file_put_contents('../../admin/assets/popular.json', $newJsonString);
				}
			}
		}
	//}

	//ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Orden en Proceso";
	$mensaje = "<h1 style='font-weight: lighter;'>Gracias por confiar en nosotros!</h1>
				<h2 style='font-weight: lighter;'>".$nombre." Guarda tu n&uacute;mero de orden:</h2>
				<h3 class='text-align: center;'><b>".$random."</b></h3>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Inicia sesión en <a href='http://movilsim.com' target:'_blank'>MOVILSIM.com</a> con tu correo electr&oacute;nico y este n&uacute;mero que generamos para ti exclusivamente, para que puedas revisar el estado de tu pedido</p>
				<p>Tambien estaremos al pendiente de tu orden y te avisaremos en cuanto este lista</p>
				<div style='display: block; padding: 10px 0;'></div>
				<p style='font-size: .8rem'>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de movilSim para poder auxiliarte: <a href='mailto:ordenes@movilsim.com' style='color: #00b7dd; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$email);

	include('../../func/cierra_conexion.php');
?>
