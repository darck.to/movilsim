<?php

	include_once('../../func/functions.php');

	if (empty($_POST['folio'])) {die("Error, contact support"); }
	if (empty($_POST['kind'])) {die("Error, contact support"); }
	if (empty($_POST['order'])) {die("Error, contact support"); }

	$folio = html_escape($_POST['folio']);
	$kind = html_escape($_POST['kind']);
	$order = html_escape($_POST['order']);

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$filename = '../../admin/assets/'.$folio.'_'.$kind.'_'.$order.'.json';

	if (file_exists($filename)) {

		$filename = file_get_contents($filename);
		$json = json_decode($filename, true);

		$email = $json[0]['email'];
		if ($json[0]['estatus'] == 1) {
			$status = "Finalizada";
		} else {
			$status = "en Proceso";
		}
		$code = $json[0]['code'];

		if ($kind == 'order') {
			$title = "Desbloqueo";
		} else {
			$title = "Revisión de IMEI";
		}

		//ENVIAMOS EL MAIL DE BIENVENIDA//
		//////////////////////////////////
		$asunto = "Tu Orden de ".$title." se encuentra ".$status."";
		$mensaje = "<h1 style='font-weight: lighter;'>Estatus de tu servicio</h1>
					<h3><b>Tu ".$title.": ".$code."</b></h3>
					<div style='display: block; padding: 5px 0;'></div>
					<p>Sigue las instrucciones que puedes encontrar en <b><a href='http://movilsim.com' target:'_blank'>nuestra p&aacute;gina</a></b> para seguir con tu proceso de ".$title."</p>
					<div style='display: block; padding: 5px 0;'></div>
					<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://movilsim.com' target:'_blank'>AQUI</a></b></p>
					<div style='display: block; padding: 10px 0;'></div>
					<p style='font-size: .8rem'>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de movilSim para poder auxiliarte: <a href='mailto:ordenes@movilsim.com' style='color: #00b7dd; text-decoration: none; -webkit-tap-highlight-color: transparent; font-weight: bolder;'><b>AQUI</b></a></p>
		";
		include_once('../../func/mailFunctions.php');
		enviaCorreo($asunto,$mensaje,$email);

	}

?>
