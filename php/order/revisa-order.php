<?php
	
	header('Content-type: application/json');

	$_POST = json_decode(file_get_contents('php://input'), true);

	$order = $_POST['order'];

	$fileList = glob('../../admin/assets/*_order_'.$order.'.json');

   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	foreach($fileList as $filename){

		$resultados = [];

		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$resultados[] = array("Folio"=>$content['folio'],"Fecha"=>$content['fecha'],"Order"=>$content['random'],"Marca"=>$content['marca'],"Equipo"=>$content['equipo'],"Estatus"=>$content['estatus']);

			}

		}

	}

	if (empty($resultados)) {

		$resultados = [];

		$fileList = glob('../../admin/assets/*_check_'.$order.'.json');

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		foreach($fileList as $filename){

			if (file_exists($filename)) {
				
				$filename = file_get_contents($filename);
				$json = json_decode($filename, true);

				foreach ($json as $content) {

					$resultados[] = array("Folio"=>$content['folio'],"Fecha"=>$content['fecha'],"Order"=>$content['random'],"Estatus"=>$content['estatus']);

				}

			}

		}

	}

	print json_encode($resultados);

?>