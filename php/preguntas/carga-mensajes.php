<?php
	
	header('Content-type: application/json');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../admin/assets/questions/question*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$resultados[] = array("usuario"=>$content['usuario'],"fecha"=>$content['fecha'],"mensaje"=>$content['mensaje'],"answer"=>$content['answer']);

			}

		}

	}

	print json_encode($resultados);

?>