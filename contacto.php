<!-- seccion de footer-->
<?php include_once('headers.php'); ?>

<body>

	<!-- seccion de logo-->
	<?php include_once('menu.php'); ?>

	<!-- seccion de cuerpo-->
	<?php include_once('templates/contacto/contacto.php'); ?>
	
	<!-- seccion de footer-->
	<?php include_once('footer.php'); ?>

</body>

</html>

<?php include_once('init.php'); ?>
<!--CARGA SCRIPT LOCAL-->
<script type="text/javascript" src="scripts/contacto/contacto.js"></script>