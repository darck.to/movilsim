<!-- seccion de lenguajes-->
<?php include_once('lang.php'); ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo _TAGLANG; ?>" prefix="og: http://movilsim.com">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<head>
	<base href="." />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="google-site-verification" content="ps-wzM9il4z6MLkp6nY2bxRM6GV9Jd9hBcTFyzF2jr4" />
	<meta name="description" content="Desbloqueo de equipos celulares bloqueados, Revisi&oacute;n de blacklist, reporte de robo, estatus y ficha t&eacute;cnica. Desbloquea tu equipo Iphone, Samsung, LG, HTC, Huawei, Xiaomi, Sony con nosotros. Desbloqueo de celulares, att, telcel, unefon, movistar.">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<link rel="canonical" href="https://movilsim.com" />
	<link href="estilos/reset.css" type="text/css" rel="stylesheet">

	<!--Import bulma.css-->
	<link type="text/css" rel="stylesheet" href="css/bulma.css"  media="screen,projection"/>

	<link href="estilos/estilos.css" type="text/css" rel="stylesheet">

	<link rel="icon" type="image/png" href="favicon.png">

	<!--[if lt IE 9]>
	    <script src="scripts/html5shiv.js"></script>
	<![endif]-->
	<!-- Start of  Zendesk Widget script -->
	<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=2ab10431-9a8f-44f8-bdc7-a4fad767c363"> </script>
	<!-- End of  Zendesk Widget script -->
	<title>MovilSIM</title>
	<meta property="og:site_name" content="MovilSIM" />
	<meta property="og:title" content="MovilSIM Desbloqueos de Celular" />
	<meta property="og:description" content="Desbloqueo de equipos celulares bloqueados. Desbloqueo de celulares, att, telcel, unefon, movistar." />
	<meta property="og:image" content="https://movilsim.com/img/portada.png" />
	<meta property="og:image:url" content="https://movilsim.com/img/portada.png" />
	<meta property="og:image:type" content="image/jpeg" />
</head>
